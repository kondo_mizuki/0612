#import "CustomWebViewController.h"
#import "Util.h"
#import "UtilWebView.h"

@interface CustomWebViewController ()

@property(nonatomic,retain)UIImageView *header_bg;

@end

@implementation CustomWebViewController

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void) setbackButton:(id)target action:(NSString*)selector {
    _header_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_bg.png"]];
    [self addSubview:_header_bg];

//    UIImageView *header_logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_logo.png"]];
//    header_logo.center = CGPointMake([Util getCenterWidth], _header_bg.image.size.height/2);
//    [self addSubview:header_logo];

    UIImage *imageBack = [UIImage imageNamed:@"web_btn_close.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(0, _header_bg.image.size.height, imageBack.size.width, imageBack.size.height);
    backButton.center = CGPointMake([Util getWinSize].size.width - imageBack.size.width, _header_bg.image.size.height/2);
    [backButton setBackgroundImage:imageBack forState:UIControlStateNormal];
    [backButton setBackgroundImage:imageBack forState:UIControlStateHighlighted];
    [backButton addTarget:target action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchDown];

    [self addSubview:backButton];
}

- (void) loadUrl:(NSString *)targetUrl view:(UIView *)view {
    UtilWebView *wv = [[UtilWebView alloc] initWithFrame:CGRectMake(0, _header_bg.image.size.height, [Util getWinSize].size.width, [Util getWinSize].size.height)];
    [wv loadUrl:targetUrl view:view];
    [self addSubview:wv];
}

@end
