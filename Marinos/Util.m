//
//  Util.m
//  TimeCapsuleSampleWithARC
//
//  Created by kyo on 2013/01/29.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "Util.h"
#import "UIApplication+UIID.h"
#import "FMDatabase.h"
#import "Define.h"
#import "DatabaseUtility.h"
#import "TopVC.h"
#import "MapVC.h"
#import "CardVC.h"
#import "AlbumVC.h"
@interface Util()

@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, strong) FMDatabase *db;

@end


@implementation UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
{
    UIGraphicsBeginImageContext(size);
    // draw scaled image into thumbnail context
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();
    // pop the context
    UIGraphicsEndImageContext();
    if(newThumbnail == nil)
        NSLog(@"could not scale image");
    return newThumbnail;
}

@end

@implementation Util

static Util* sharedUtil = nil;

+ (Util*)sharedManager {
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [[self alloc] init];
        }
    }
    return sharedUtil;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [super allocWithZone:zone];
            return sharedUtil;
        }
    }
    return nil;
}


+ (void) setDefaultsValues {
    //バージョンなど初期値の設定
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@"" forKey:KEY_USER_ID];
    [dic setObject:[NSNumber numberWithBool:NO] forKey:KEY_SEND_FLAG];
    [dic setObject:[NSNumber numberWithBool:NO] forKey:KEY_FIRST_PLAY_DEVICE];
    [dic setObject:[NSNumber numberWithBool:NO] forKey:KEY_MIGRATION_FLAG];
    [dic setObject:[NSNumber numberWithBool:YES] forKey:KEY_RECEIVE_PUSH_FLAG];
    [dic setObject:@"1.0.0" forKey:KEY_CURRENT_VERSION];
    [dic setObject:@"0" forKey:KEY_VERSION_ID];
    [dic setObject:@"" forKey:KEY_UNIVERSAL_UNIQUE_ID_DEVICE];
    [ud registerDefaults:dic];
    [ud synchronize];
}

-(void)prepareToPlaySounds
{/*
    // ファイルのパスを作成します。
    NSString *path = [[NSBundle mainBundle] pathForResource:@"choice" ofType:@"mp3"];
    // ファイルのパスを NSURL へ変換します。
    NSURL* url = [NSURL fileURLWithPath:path];
    // ファイルを読み込んで、プレイヤーを作成します。
    _buttonSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_buttonSE prepareToPlay];
    _buttonSE.volume = 0.3;
    
    path = [[NSBundle mainBundle] pathForResource:@"cardGetButtonSound" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardButtonSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardButtonSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"cardAppearSound" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardAppearSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardAppearSE prepareToPlay];
    _cardAppearSE.volume = 0.8;

    path = [[NSBundle mainBundle] pathForResource:@"cardZoomUpSoundNormal" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardZoomUpSE_Normal = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardZoomUpSE_Normal prepareToPlay];
    _cardZoomUpSE_Normal.volume = 0.5;

    path = [[NSBundle mainBundle] pathForResource:@"cardZoomUpSoundRare" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _cardZoomUpSE_Rare = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_cardZoomUpSE_Rare prepareToPlay];
    _cardZoomUpSE_Rare.volume = 0.5;
    
    path = [[NSBundle mainBundle] pathForResource:@"giiiin" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _getTextSE_Rare = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_getTextSE_Rare prepareToPlay];
    _getTextSE_Rare.volume = 0.6;
    
    path = [[NSBundle mainBundle] pathForResource:@"shu" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _albumSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_albumSE prepareToPlay];
    _albumSE.volume = 0.5;
    
    path = [[NSBundle mainBundle] pathForResource:@"kiiiin" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _kiiinSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_kiiinSE prepareToPlay];

    path = [[NSBundle mainBundle] pathForResource:@"cursor25" ofType:@"wav"];
    url = [NSURL fileURLWithPath:path];
    _chuiuSE= [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_chuiuSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"pyoro52" ofType:@"wav"];
    url = [NSURL fileURLWithPath:path];
    _pyoroSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_pyoroSE prepareToPlay];
    _pyoroSE.volume = 0.3;
    
    path = [[NSBundle mainBundle] pathForResource:@"Sys03-pop up" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _pironSE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_pironSE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"v_appearSE" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _vic_appear_SE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_vic_appear_SE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"v_buttonPushSE" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _vic_Btn_SE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_vic_Btn_SE prepareToPlay];
    
    path = [[NSBundle mainBundle] pathForResource:@"v_finishSE" ofType:@"mp3"];
    url = [NSURL fileURLWithPath:path];
    _vic_finish_SE = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [_vic_finish_SE prepareToPlay];*/
}



-(void)playSound:(int)soundID
{/*
    if (soundID == SOUND_TAB)
    {
        if (_buttonSE.isPlaying)
        {
            [_buttonSE stop];
            _buttonSE.currentTime = 0.0;
        }
        [_buttonSE play];
    }
    else if (soundID == SOUND_GET_BUTTON)
    {
        //_cardButtonSE.currentTime = 0.0;
        [_cardButtonSE play];
    }
    else if (soundID == SOUND_CARD_APPEAR)
    {
        [_cardAppearSE play];
    }
    else if (soundID == SOUND_CARD_ZOOM_NORMAL)
    {
        [_cardZoomUpSE_Normal play];
    }
    else if (soundID == SOUND_CARD_ZOOM_RARE)
    {
        [_cardZoomUpSE_Rare play];
    }
    else if (soundID == SOUND_GET_TEXT)
    {
        [_getTextSE_Rare play];
    }
    else if (soundID == SOUND_ALBUM)
    {
        [_albumSE play];
    }
    else if (soundID == SOUND_KIIIN)
    {
        [_kiiinSE play];
    }
    else if (soundID == SOUND_CHUIU)
    {
        [_chuiuSE play];
    }
    else if (soundID == SOUND_PYORO)
    {
        [_pyoroSE play];
    }
    else if (soundID == SOUND_PIRON)
    {
        [_pironSE play];
    }
    else if (soundID == SOUND_VICTORY_APPER)
    {
        [_vic_appear_SE play];
    }
    else if (soundID == SOUND_VICTORY_BUTTON)
    {
        [_vic_Btn_SE play];
    }
    else if (soundID == SOUND_VICTORY_FINISH)
    {
        [_vic_finish_SE play];
    }*/
}



- (id)copyWithZone:(NSZone*)zone {
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

#pragma mark - データベース操作のためのopen,closeメソッド
- (BOOL)openDatabase {
    //DBファイルへのパスを取得
    //パスは~/Documents/配下に格納される。
    NSString *dbPath = nil;
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得データ数を確認
    if ([documentsPath count] >= 1) {
        //固定で0番目を取得でOK
        dbPath = [documentsPath objectAtIndex:0];
        //パスの最後にファイル名をアペンドし、DBファイルへのフルパスを生成。
        dbPath = [dbPath stringByAppendingPathComponent:DB_FILE];
    } else {
        //error
        NSLog(@"search Document path error. database file open error.");
        return false;
    }
    
    //DBファイルがDocument配下に存在するか判定
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dbPath]) {
        //存在しない
        //デフォルトのDBファイルをコピー(初回のみ)
        //ファイルはアプリケーションディレクトリ配下に格納されている。
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *orgPath = [bundle bundlePath];
        //初期ファイルのパス。(~/XXX.app/sample.db)
        orgPath = [orgPath stringByAppendingPathComponent:DB_FILE];
        
        //デフォルトのDBファイルをDocument配下へコピー
        if (![fileManager copyItemAtPath:orgPath toPath:dbPath error:nil]) {
            //error
            NSLog(@"db file copy error. : %@ to %@.", orgPath, dbPath);
            return false;
        }
    }

    //open database with FMDB.
    self.db = [FMDatabase databaseWithPath:dbPath];
    return [self.db open];    
}

- (void)closeDatabase {
    if (self.db) {
        [self.db close];
    }
}

#pragma mark - データベース操作 add2.0.0
/**===========  add ver2.0.0   ==========================================================**/
//- (NSMutableArray *)getPhotosFromNewHistorysTable{
//    if ([self openDatabase]) {
//        NSMutableArray *array = [NSMutableArray array];
//        FMResultSet *rs = [self.db executeQuery:@"SELECT id, title, thumbnail, record_date, url, latitude, longitude, place_id, show_flag FROM historys ORDER BY id DESC"];
//        while ([rs next]) {
//            // Get the column data for this record and put it into a custom Record object
//            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//            
//            NSString *identification = [rs stringForColumn:@"id"];
//            if (identification != nil) {
//                [dic setObject:identification forKey:@"identification"];
//            }
//            
//            NSString *title = [rs stringForColumn:@"title"];
//            if (title != nil) {
//                [dic setObject:title forKey:@"title"];
//            }
//            
//            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
//            if (thumbnail != nil) {
//                [dic setObject:thumbnail forKey:@"thumbnail"];
//            }
//            
//            NSString *url = [rs stringForColumn:@"url"];
//            if (url != nil) {
//                [dic setObject:url forKey:@"url"];
//            }
//            
//            NSString *record_date = [rs stringForColumn:@"record_date"];
//            if (record_date != nil) {
//                [dic setObject:record_date forKey:@"record_date"];
//            }
//            
//            NSString *latitude = [rs stringForColumn:@"latitude"];
//            if (latitude != nil) {
//                [dic setObject:latitude forKey:@"latitude"];
//            }
//            
//            NSString *longtitude = [rs stringForColumn:@"longitude"];
//            if (longtitude != nil) {
//                [dic setObject:longtitude forKey:@"longitude"];
//            }
//            
//            NSNumber *place_id = [NSNumber numberWithInt:[rs intForColumn:@"place_id"]];
//            if (place_id != nil) {
//                [dic setObject:place_id forKey:@"place_id"];
//            }
//            
//            NSNumber *show_flag = [NSNumber numberWithInt:[rs intForColumn:@"show_flag"]];
//            if (show_flag != nil) {
//                [dic setObject:show_flag forKey:@"show_flag"];
//            }
//            [array addObject:dic];
//        }
//        [rs close];
//        [self closeDatabase];
//        return array;
//    } else {
//        NSLog(@"DBが開けなかった");
//    }
//    return nil;
//
//}


-(float)getSatusbarHeight
{
    float sbh = 20.0f;
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f)
    {  //iOS7未満の場合
        sbh = 0.0f;
    }
    return sbh;
}



- (BOOL) createHistorysDeltaTable {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        
        [self.db executeUpdate:@"CREATE TABLE historys_delta(id INTEGER PRIMARY KEY, latitude TEXT, longitude TEXT, delta INTEGER, UNIQUE(latitude, longitude))"];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (int)getHistory_delta:(NSString*)latitude longitude:(NSString*)longitude {
    if ([self openDatabase]) {
        int delta = -1;
        FMResultSet *rs = [self.db executeQuery:@"SELECT delta FROM historys_delta WHERE latitude=? AND longitude=?", latitude, longitude];
        while ([rs next]) {
            delta = [rs intForColumn:@"delta"];
        }
        [rs close];
        [self closeDatabase];
        return delta;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return -10;
}

- (BOOL)saveHistory_delta:(NSString*)latitude with:(NSString*)longitude delta:(int)delta {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        
        [self.db executeUpdate:@"INSERT OR REPLACE INTO historys_delta(latitude, longtitude, delta) VALUES(?, ?, ?)", latitude, longitude, [NSNumber numberWithInt:delta]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)updateHistorys_delta:(int)delta latitude:(NSString*)latitude longitude:(NSString*)longitude {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        
        [self.db executeUpdate:@"UPDATE historys_delta SET delta=? WHERE latitude=? AND longitude=?",[NSNumber numberWithInt:delta], latitude, longitude];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}
- (void) callGetAreaNew:(NSDictionary*)coordinates {
/*
    // 送信したいURLを作成する
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat: @"%@/%@", self.heroku, @"/areanew"]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSError *error = nil;
    NSData *data = nil;
    if([NSJSONSerialization isValidJSONObject:coordinates]){
        data = [NSJSONSerialization dataWithJSONObject:coordinates options:NSJSONWritingPrettyPrinted error:&error];
        NSLog(@"data: %@",data);
        NSLog(@"data to string: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    }
    
    request.HTTPBody = data;
    
    ConnectionHandler *connectionHandler = [[ConnectionHandler alloc] init];
    connectionHandler.api = getAreaNew;
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:connectionHandler];
    
    if (connection) {
        NSLog(@"Success!!");
    } else {
        NSLog(@"Error!!");
    }
*/
}

- (BOOL) createCardImageWithMD5Table{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        [self.db executeUpdate:@"CREATE TABLE card_image_withMD5_table(id INTEGER PRIMARY KEY, file_name TEXT, MD5 TEXT)"];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;

}
- (BOOL) createNewHistorysTable {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        [self.db executeUpdate:@"CREATE TABLE new_historys_table(id INTEGER PRIMARY KEY, thumbnail BLOB, url TEXT, record_date TEXT, latitude REAL, longitude REAL, title TEXT, body TEXT, datetime TEXT, place_id INTEGER, show_flag INTEGER, like_count INTEGER, tweet_count INTEGER)"];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL) insertCardImageWithMD5Table:(NSString *)file_name withMD5:(NSString *)MD5_str{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        
        [self.db executeUpdate:@"INSERT INTO card_image_withMD5_table(file_name , MD5) VALUES(?, ?)", file_name, MD5_str];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL) updateCardImageWithMD5Table:(NSString *)file_name withMD5:(NSString *)MD5_str{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        
        [self.db executeUpdate:@"UPDATE card_image_withMD5_table SET MD5 = ? WHERE file_name = ?",  MD5_str, file_name];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)saveNewHistoryTable:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longitude:(double)longitude title:(NSString*)title detail:(NSString *)detail place_id:(int)place_ID showFlag:(int)show_flag{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        
        NSData *imagedata = [[NSData alloc] initWithData:UIImageJPEGRepresentation(thumbnail, 0.2f)];
        
        NSDate *now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *nowStr = [formatter stringFromDate:now];
        
        [self.db executeUpdate:@"INSERT INTO new_historys_table(thumbnail, url, record_date, latitude, longitude, title, body, datetime, place_id, show_flag, like_count, tweet_count) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", imagedata, url, recordDate, [NSNumber numberWithDouble:latitude], [NSNumber numberWithDouble:longitude], title, detail, nowStr, [NSNumber numberWithInt:place_ID], [NSNumber numberWithInt:show_flag], [NSNumber numberWithInt:INITIAL_LIKE_COUNT], [NSNumber numberWithInt:INITIAL_TWEET_COUNT]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)saveNewHistoryTableForShowFlag:(BOOL)show_flag keyURLForDataBase:(NSString *)key_url{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        [self.db beginTransaction];
        [self.db setShouldCacheStatements:YES];
        NSString *sql = @"UPDATE new_historys_table SET show_flag=? WHERE url=?";
        [self.db executeUpdate:sql, [NSNumber numberWithBool:show_flag], key_url];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (NSMutableArray *)getNewHistoryTable{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *result = [self.db executeQuery:@"SELECT * FROM new_historys_table WHERE show_flag ='1';"];
        while ([result next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            double latitude = [result doubleForColumn:@"latitude"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longitude = [result doubleForColumn:@"longitude"];
            [dic setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            
            NSString *url = [result stringForColumn:@"url"];
            [dic setObject:url forKey:@"url"];
            
            NSNumber *place_id = [NSNumber numberWithInt:[result intForColumn:@"place_id"]];
            if (place_id != nil) {
                [dic setObject:place_id forKey:@"place_id"];
            }
            
            NSNumber *show_flag = [NSNumber numberWithInt:[result intForColumn:@"show_flag"]];
            if (show_flag != nil) {
                [dic setObject:show_flag forKey:@"show_flag"];
            }
            
            [array addObject:dic];
        }
        [result close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray *)getNewHistoryTableMyPhotoData{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *result = [self.db executeQuery:@"SELECT * FROM new_historys_table WHERE show_flag ='1';"];
        while ([result next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            double latitude = [result doubleForColumn:@"latitude"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longitude = [result doubleForColumn:@"longitude"];
            [dic setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[result dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }

            NSString *url = [result stringForColumn:@"url"];
            [dic setObject:url forKey:@"url"];
            
            NSNumber *place_id = [NSNumber numberWithInt:[result intForColumn:@"place_id"]];
            if (place_id != nil) {
                [dic setObject:place_id forKey:@"place_id"];
            }
            
            NSNumber *show_flag = [NSNumber numberWithInt:[result intForColumn:@"show_flag"]];
            if (show_flag != nil) {
                [dic setObject:show_flag forKey:@"show_flag"];
            }
            
            NSString *title = [result stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            NSString *body = [result stringForColumn:@"body"];
            if (body != nil) {
                [dic setObject:body forKey:@"body"];
            }
            
            NSString *record_date = [result stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            [array addObject:dic];
        }
        [result close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

/**==================================================================================**/
- (void) alterColumnToTable:(NSString *)tableName columnName:(NSString *)columnName withType:(NSString *)type {
    /* [db setShouldCacheStatements:YES];
     [db setCrashOnErrors:YES];
     
     if(![db columnExists:@"（テーブル名）" columnName:@"（カラム名）"])
     {
     NSString* sql = [NSString stringWithFormat:@"ALTER TABLE （テーブル名） ADD COLUMN （カラム名） INTEGER DEFAULT 0"];
     [db executeUpdate:sql];
     }*/
    if ([self openDatabase]) {
        [self.db setShouldCacheStatements:YES];
        [self.db setCrashOnErrors:YES];
        NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN '%@' %@", tableName, columnName, type];
        [self.db executeUpdate:sql];
        [self closeDatabase];
        
    } else {
        NSLog(@"DBが開けなかった");
    }

}

- (NSMutableArray *) getColumnFromTable:(NSString *)tableName {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        NSString *sql = [NSString stringWithFormat:@"PRAGMA table_info(%@)", tableName];
        FMResultSet *rs = [self.db executeQuery:sql];
        while ([rs next]) {
            [array addObject:[rs stringForColumn:@"name"]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
- (NSMutableArray*) showTables {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM sqlite_master WHERE type='table'"];
        while ([rs next]) {
            [array addObject:[rs stringForColumn:@"name"]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (int)getPhotosSum {
    if ([self openDatabase]) {
        [self.db setShouldCacheStatements:YES];
        
        int photosCount = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM historys"];
        while ([rs next]) {
//            //ここでデータを展開
            photosCount = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return photosCount;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

#pragma mark - サーバー通信系
- (NSDictionary *)methodGetHTTP:(NSString *)php_program{
    NSDictionary *jsonObj = nil;
    NSString *user_ID = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_ID];
    // サブキューを作成して、処理をサブスレッドで実行する。
    //dispatch_queue_t sub_queue = dispatch_queue_create("sub_queue", 0);
    //dispatch_async(sub_queue, ^{
    NSString *sendStr = [NSString stringWithFormat:@"%@%@%@", self.myServer, php_program ,[NSString stringWithFormat:@"?uid=%d",[user_ID intValue]]];
    NSURL *url = [NSURL URLWithString:sendStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // リクエストを送信する。
    NSError *error = nil;
    NSURLResponse *response = nil;
    // URLからJSONデータを取得(NSData)
    NSData *jsonData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    jsonObj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    //        NSLog(@"%@",jsonObj);
    //        NSArray *array = [jsonObj objectForKey:@"shots"];
    //        NSLog(@"%@",array);
    
    // リクエスト結果を表示する。
    // エラー処理は、上の非同期リクエストと同じ感じで。
    NSLog(@"request finished!!");
    NSLog(@"error = %@", error);
    if (error) {
        // エラー処理を行う。
        if (error.code == -1003) {
            NSLog(@"not found hostname. targetURL=%@", url);
        } else if (-1019) {
            NSLog(@"auth error. reason=%@", error);
        } else {
            NSLog(@"unknown error occurred. reason = %@", error);
        }
        
    } else {
        int httpStatusCode = ((NSHTTPURLResponse *)response).statusCode;
        if (httpStatusCode == 404) {
            NSLog(@"404 NOT FOUND ERROR. targetURL=%@", url);
            // } else if (・・・) {
            // 他にも処理したいHTTPステータスがあれば書く。
            
        } else {
            NSLog(@"success request!!");
            NSLog(@"statusCode = %d", ((NSHTTPURLResponse *)response).statusCode);
            //NSLog(@"responseText = %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            //ここに結果を書く
            NSLog(@"%@",jsonObj);
            /*
             NSString *code = [jsonObj objectForKey:@"code"];
             int uid = [[jsonObj objectForKey:@"uid"] intValue];
             NSString *userID = [NSString stringWithFormat:@"%d",uid];
             NSLog(@"code:%@とuserID:%@",code,userID);
             if ([code isEqualToString:@"R0002"]) {
             [[NSUserDefaults standardUserDefaults] setObject:userID forKey:KEY_USER_ID];
             [[NSUserDefaults standardUserDefaults] synchronize];
             }else if ([code isEqualToString:@"R0000"]){
             NSLog(@"userID is exist");
             }
             */
        }
    }
    return jsonObj;
    /*});*/

}

- (NSMutableArray*)getAlbumCells {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longtitude = [rs stringForColumn:@"longtitude"];
            if (longtitude != nil) {
                [dic setObject:longtitude forKey:@"longtitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getPhotos {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *identification = [rs stringForColumn:@"id"];
            if (identification != nil) {
                [dic setObject:identification forKey:@"identification"];
            }
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longtitude = [rs stringForColumn:@"longtitude"];
            if (longtitude != nil) {
                [dic setObject:longtitude forKey:@"longtitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (UIImage*)getPhotoFromIdentification:(NSString*)identification {
    if ([self openDatabase]) {
        FMResultSet *rs = [self.db executeQuery:@"SELECT thumbnail FROM historys WHERE id=?", identification];
        UIImage *thumbnail = nil;
        while ([rs next]) {
            thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
        }
        [rs close];
        [self closeDatabase];
        return thumbnail;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (BOOL)saveHistory:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longtitude:(double)longtitude title:(NSString*)title detail:(NSString*)detail {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        
        //ステートメントの再利用フラグ
        //おそらくループ内で同一クエリの更新処理を行う場合バインドクエリの準備を何回
        //も実行してしまうのためこのフラグを設定する。
        //このフラグが設定されているとステートメントが再利用される。
        [self.db setShouldCacheStatements:YES];
        
        //insertクエリ実行(プリミティブ型は使えない)
        //    [_db executeUpdate:@"insert into example values (?, ?, ?, ?)",
        //                                1, 2, @"test", 4.1];
        // executeUpdateWithFormatメソッドで可能。
        
        NSData *imagedata = [[NSData alloc] initWithData:UIImageJPEGRepresentation(thumbnail, 0.2f)];
        
        NSDate *now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *nowStr = [formatter stringFromDate:now];
        
        [self.db executeUpdate:@"INSERT INTO historys(thumbnail, url, record_date, latitude, longtitude, title, body, datetime, like_count, tweet_count) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", imagedata, url, recordDate, [NSNumber numberWithDouble:latitude], [NSNumber numberWithDouble:longtitude], title, detail, nowStr, [NSNumber numberWithInt:INITIAL_LIKE_COUNT], [NSNumber numberWithInt:INITIAL_TWEET_COUNT]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)saveCheckin:(double)latitude with:(double)longtitude {
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        
        //ステートメントの再利用フラグ
        //おそらくループ内で同一クエリの更新処理を行う場合バインドクエリの準備を何回
        //も実行してしまうのためこのフラグを設定する。
        //このフラグが設定されているとステートメントが再利用される。
        [self.db setShouldCacheStatements:YES];
        
        //insertクエリ実行(プリミティブ型は使えない)
        //    [_db executeUpdate:@"insert into example values (?, ?, ?, ?)",
        //                                1, 2, @"test", 4.1];
        // executeUpdateWithFormatメソッドで可能。
        
        [self.db executeUpdate:@"INSERT INTO checkins(latitude, longtitude) VALUES(?, ?)",[NSNumber numberWithDouble:[self changeDigitTo3:latitude]], [NSNumber numberWithDouble:[self changeDigitTo3:longtitude]]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

// テスト用メソッド
- (BOOL)showAllRows {
    if ([self openDatabase]) {
        //        [self.db setShouldCacheStatements:YES];
        
        int count = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM checkins"];
        while ([rs next]) {
            //            //ここでデータを展開
            NSLog(@"%d, %f, %f", [rs intForColumnIndex:0], [rs doubleForColumnIndex:1], [rs doubleForColumnIndex:2]);
        }
        [rs close];
        [self closeDatabase];
        return count;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (NSMutableArray*)getCheckIns {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT latitude, longtitude FROM checkins"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longtitude = [rs stringForColumn:@"longtitude"];
            if (longtitude != nil) {
                [dic setObject:longtitude forKey:@"longtitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (int)hasCheckin:(double)latitude with:(double)longtitude {
    if ([self openDatabase]) {
//        [self.db setShouldCacheStatements:YES];
        
        int count = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM checkins WHERE latitude=? AND longtitude=?", [NSNumber numberWithDouble:[self changeDigitTo3:latitude]], [NSNumber numberWithDouble:[self changeDigitTo3:longtitude]]];
        while ([rs next]) {
            //            //ここでデータを展開
            count = [rs intForColumn:@"cnt"];
        }
        
        [rs close];
        [self closeDatabase];
        return count;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getCheckinSum {
    if ([self openDatabase]) {
        [self.db setShouldCacheStatements:YES];
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM checkins"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (void)checkinCheck:(double)latitude with:(double)longtitude {
    if ([self hasCheckin:latitude with:longtitude] == 0) {
        [self saveCheckin:latitude with:longtitude];
    }
}

- (double)changeDigitTo3:(double)value {
    return floor(value*10*10*10)/1000;
}



-(CGFloat)getStatusbarHeight
{
    CGFloat iOS_adjust;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            iOS_adjust = 0.0f;
            break;
        case NSOrderedSame:
            iOS_adjust = 20.0f;
            break;
        case NSOrderedDescending:
            iOS_adjust = 20.0f;
            break;
        default:
            break;
    }
    return iOS_adjust;
    /*
    float sbh = 20.0f;
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f)
    {  //iOS7未満の場合
        sbh = 0.0f;
    }
    return sbh;
     */
}


-(int)getRandom_min:(int)min max:(int)max{
    
    static int initFlag;
    if (initFlag == 0) {
        srand((unsigned int)time(NULL));
        initFlag = 1;
    }
    return min + (int)(rand()*(max-min+1.0)/(1.0+RAND_MAX));
    
}

/**
 比較しやすいように数値にする
 ver 1.2.3.4 => 1020304
 */
- (NSUInteger)integerVersion:(NSString *)version {
	NSUInteger iVersion = 0;
	NSArray *verArray = [version componentsSeparatedByString:@"."];
	for (int i=0; i<verArray.count; i++) {
		NSString *ver = [verArray objectAtIndex:i];
		iVersion += [ver longLongValue] * ((NSUInteger)pow(100, (3-i)));
	}
    NSLog(@"ver:%d",iVersion);
	return iVersion;
}

-(BOOL)checkCurrentVersion:(NSString *)version{
    NSLog(@"casted.ver:%@",version);
    //返り値を定義
    BOOL check = NO;
    NSLog(@"%@",version);
    //現在のバージョン調べる
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    //比較の結果を保持
    NSComparisonResult compare =[currentVersion compare:version options:NSNumericSearch];
    //比較する
    switch (compare) {
        case NSOrderedAscending:
            NSLog(@"前のバージョン");
            check = NO;
            break;
        case NSOrderedSame:
            NSLog(@"同じバージョン");
            check = NO;
            break;
        case NSOrderedDescending:
            NSLog(@"新しいバージョン");
            check = YES;
            break;
            
        default:
            break;
    }
    return check;
}

- (CGFloat)getFooterAdjust{
    CGFloat footerAdjust = 0.0f;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            footerAdjust = 20.0f;
            break;
        case NSOrderedSame:
            footerAdjust = 0.0f;
            break;
        case NSOrderedDescending:
            footerAdjust = 0.0f;
            break;
        default:
            break;
    }
    return footerAdjust;
}

- (CGFloat)getAdjustedHeaderPartsHeight{
    static CGFloat headerHeight = 0.0f;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            headerHeight = TOP_BAR_HEIGHT;//44
            break;
        case NSOrderedSame:
            headerHeight = TOP_BAR_HEIGHT + (CGFloat)[self getStatusbarHeight];//64
            break;
        case NSOrderedDescending:
            headerHeight = TOP_BAR_HEIGHT + (CGFloat)[self getStatusbarHeight];
            break;
        default:
            break;
    }
    return headerHeight;
}

- (NSString*) createUIID {
    return [[UIApplication sharedApplication] uniqueInstallationIdentifier];
}

+ (CGRect) getWinSize {
    return [[UIScreen mainScreen] bounds];
}

+ (CGPoint) getCenterPoint {
    return CGPointMake([Util getWinSize].size.width/2, [Util getWinSize].size.height/2);
}

+ (float) getCenterWidth {
    return [[UIScreen mainScreen] bounds].size.width/2;
}

+ (BOOL)isIOS7
{
    static BOOL sFlag = NO;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            sFlag = NO;
            break;
        case NSOrderedSame:
            sFlag = YES;
            break;
        case NSOrderedDescending:
            sFlag = YES;
            break;
        default:
            break;
    }
    return sFlag;
}

+ (void) setUserData:(NSString*) name prefecture:(NSString*)prefecture team:(NSString*)team {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:name forKey:UD_USER_NAME];
    [ud setObject:prefecture forKey:UD_USER_PREFECTURE];
    [ud setObject:team forKey:UD_USER_TEAM];
    [ud synchronize];
}

+ (void) setUserDataFromUd {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [User sharedManager].name = [ud objectForKey:UD_USER_NAME];
    [User sharedManager].prefecture = [ud objectForKey:UD_USER_PREFECTURE];
    [User sharedManager].team = [ud integerForKey:UD_USER_TEAM];
}

+ (void) goSafari:(NSString*) urlString {
    NSURL *url = [NSURL URLWithString:urlString];
    [[UIApplication sharedApplication] openURL:url];
}

+ (NSArray*)prefectures
{
    NSArray* prefectures = @[
                            @"北海道",@"青森県",@"岩手県",@"宮城県",@"秋田県",@"山形県",@"福島県",@"茨城県",@"栃木県",@"群馬県",
                            @"埼玉県",@"千葉県",@"東京都",@"神奈川県",@"新潟県",@"富山県",@"石川県",@"福井県",@"山梨県",@"長野県",
                            @"岐阜県",@"静岡県",@"愛知県",@"三重県",@"滋賀県",@"京都府",@"大阪府",@"兵庫県",@"奈良県",@"和歌山県",
                            @"鳥取県",@"島根県",@"岡山県",@"広島県",@"山口県",@"徳島県",@"香川県",@"愛媛県",@"高知県",@"福岡県",
                            @"佐賀県",@"長崎県",@"熊本県",@"大分県",@"宮崎県",@"鹿児島県",@"沖縄県"];
    return prefectures;
}

@end
