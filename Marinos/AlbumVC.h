#import <UIKit/UIKit.h>
#import "AlbumDetailViewController.h"
#import "CustomTabBarController.h"
#import <ADG/ADGManagerViewController.h>

typedef enum{
    leftBtn,centerBtn,rightBtn
}buttonType;

@interface AlbumVC : GAITrackedViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIActionSheetDelegate, UIScrollViewDelegate, ADGManagerViewControllerDelegate>
{
    ADGManagerViewController *ADGViewController;
}
@property (nonatomic, retain) ADGManagerViewController *adGManagerViewController;
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic)IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) AlbumDetailViewController *detailViewController;
@property (strong, nonatomic) NSMutableArray *possessionArray;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

- (void) setStage;

@end
