//
//  WeatherCell.h
//  TigersApp
//
//  Created by timecapsule02 on 2015/06/11.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "weatherAPIManager.h"

@interface WeatherCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UIImageView *IconImg;
@property (weak, nonatomic) IBOutlet UILabel *maxLbl;
@property (weak, nonatomic) IBOutlet UILabel *minLbl;

@property (nonatomic, assign) int adjustNum;

- (void)setWeather;
@end
