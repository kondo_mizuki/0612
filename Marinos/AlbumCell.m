#import "AlbumCell.h"
#import "DatabaseUtility.h"
#import "FileManager.h"

@implementation AlbumCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)displayCard:(int)cardID possession:(int)cardPossession albumName:(NSString *)album_name cardCategory:(int)card_category cardText:(NSString *)card_text
{
    NSString *selectedCardIDString = [NSString stringWithFormat:@"No.%03d",cardID];
//    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:cardID];
//    if (selectedCardDic == nil)
//    {
//        NSLog(@"カードDICが空っぽ　%d番",cardID);
//    } else if (selectedCardDic != nil) {
//        NSString *file_name = [selectedCardDic objectForKey:@"file_name"];
//        NSRange searchResult = [file_name rangeOfString:@"t_"];
//        if(searchResult.location == NSNotFound){
//            // みつからない場合の処理
//            //サムネイル用にファイル名変更(あたまにt_を追加)
//            file_name = [@"t_" stringByAppendingString:file_name];
//            
//        }else{
//            // みつかった場合の処理
//            //　何もしない
//        }
//        NSData *cardData = [[FileManager sharedManager] loadDirectory:DIR_IS_NOT_APPLICABLE_BACK_UP fileName:file_name];
//        UIImage *cardDataImage = [[UIImage alloc] initWithData:cardData];

        CGPoint cardImagePoint = CGPointMake(4.0f, 4.0f);
        CGSize cardImageSize = CGSizeMake(72.0f, 111.0f);
        UIImageView *cardImage = [[UIImageView alloc] initWithFrame:CGRectMake(cardImagePoint.x, cardImagePoint.y, cardImageSize.width, cardImageSize.height)];
        [self.contentView addSubview:cardImage];
    
       // if (cardPossession == 0)
      //  {
            //カードを保有していないときの処理
            NSLog(@"card_id%d_category%d:", cardID, card_category);
            UILabel *textLabel, *subLabel;
            CGFloat fontSize = 0.0f, subFontSize = 0.0f;
            UIImage *cardDataImage = [UIImage imageNamed:@"blank"];
            UILabel *cardIDLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, cardImage.frame.size.height-30, cardImage.frame.size.width, 30)];
            cardIDLabel.text = selectedCardIDString;
            cardIDLabel.textColor = [UIColor whiteColor];
            cardIDLabel.backgroundColor = [UIColor clearColor];
            cardIDLabel.textAlignment = NSTextAlignmentCenter;
            cardIDLabel.font = [UIFont fontWithName:MY_FONT size:14];
            [cardImage addSubview:cardIDLabel];
            
            [textLabel setFrame:CGRectMake(0, cardImage.frame.size.height-60, cardImage.frame.size.width, 30)];
            [textLabel setTextColor:[UIColor whiteColor]];
            [textLabel setBackgroundColor:[UIColor clearColor]];
            [textLabel setTextAlignment:NSTextAlignmentCenter];
            [textLabel setLineBreakMode:2];
            [textLabel setFont:[UIFont fontWithName:MY_FONT size:fontSize]];
            [cardImage addSubview:textLabel];
            
            [subLabel setFrame:CGRectMake(0, cardImage.frame.size.height-50, cardImage.frame.size.width, 30)];
            [subLabel setTextColor:[UIColor whiteColor]];
            [subLabel setBackgroundColor:[UIColor clearColor]];
            [subLabel setTextAlignment:NSTextAlignmentCenter];
            [subLabel setLineBreakMode:2];
            [subLabel setFont:[UIFont fontWithName:MY_FONT size:subFontSize]];
            [cardImage addSubview:subLabel];
            
     //   }
        cardImage.image = cardDataImage;
        //複数枚カードを所持している場合の処理
//        if (cardPossession > 1)
//        {
//            UIImage *overLapImg = [UIImage imageNamed:[NSString stringWithFormat:@"overlap.png"]];
//            UIImageView *overLapIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 119)];
//            overLapIV.image = overLapImg;
//            [self.contentView addSubview:overLapIV];
//
//            [self.contentView bringSubviewToFront:cardImage];
//            UIImage *batchImg = [UIImage imageNamed:[NSString stringWithFormat:@"batch.png"]];
//            UIImageView *batchIV = [[UIImageView alloc] initWithFrame:CGRectMake(58, 0, 27, 27)];
//            batchIV.image = batchImg;
//            [self.contentView addSubview:batchIV];
//
//            int cardCount = cardPossession;
//            UILabel *countLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 1, 24, 20)];
//            countLabel.text = [NSString stringWithFormat:@"%d",cardCount];
//            countLabel.textColor = [UIColor whiteColor];
//            countLabel.backgroundColor = [UIColor clearColor];
//            countLabel.textAlignment = NSTextAlignmentCenter;
//            countLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:16];
//            countLabel.adjustsFontSizeToFitWidth = YES;
//            [batchIV addSubview:countLabel];
//        }
}

@end
