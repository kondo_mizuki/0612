//
//  CheckinViewController.h
//  BigbrotherMap
//
//  Created by kyo on 2013/06/07.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "FrameSetVC.h"
//#import "FramePostFinishVC.h"
#import "CameraViewController.h"


@interface CheckinViewController : GAITrackedViewController<UIWebViewDelegate,UIImagePickerControllerDelegate>

//@property(nonatomic, assign)int anikicardID;
//@property(nonatomic, assign)NSString* spotName;
@property(nonatomic, assign)double latitude;
@property(nonatomic, assign)double longitude;
@property(nonatomic, assign)int place_id;
@property (nonatomic, assign) int myCardPoint;
@property (weak, nonatomic) IBOutlet UILabel *pointAddLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTextLabel;
@property (weak, nonatomic) IBOutlet UIWebView *trainWebView;
@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;

@property (nonatomic, readonly) int type;
@end
