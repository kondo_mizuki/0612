//
//  CustomScrollView.m
//  Marinos
//
//  Created by eagle014 on 2014/04/14.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import "CustomScrollView.h"

@interface CustomScrollView(){
    
}
@property (nonatomic, strong) UIView *blindView;

@end

@implementation CustomScrollView
#pragma mark - delegate
-(void)closeButtonPushed:(UIButton *)sender{
    [self.customDelegate tutorialView:self blindView:self.blindView];
}
#pragma mark - method
- (void) showTutorialPage{
    
    self.blindView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, WIN_SIZE.width * self.pageNumber, WIN_SIZE.height)];
    [self.blindView setBackgroundColor:[UIColor grayColor]];
    [self.blindView setAlpha:0.5f];
    [self addSubview:self.blindView];
    
    [self setContentSize:CGSizeMake(WIN_SIZE.width * self.pageNumber, 400.0f)];
    [self setPagingEnabled:YES];
    [self setBounces:NO];
    [self setDecelerationRate:UIScrollViewDecelerationRateFast];
    [self flashScrollIndicators];
    [self setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    
    CGSize imageViewSize = CGSizeMake(300.0f, 388.0f);
        
    for (int i = 1; i <= self.pageNumber; i++)
    {
        CGFloat adjustHeight;
        if (WIN_SIZE.height == 568) {
            adjustHeight = 50.0f;
        } else {
            adjustHeight = 20.0f;
        }
         UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + (WIN_SIZE.width * (i - 1)), adjustHeight, imageViewSize.width, imageViewSize.height)];
         NSString *imageName = [NSString stringWithFormat:@"tutorial_%@_%02d.png", self.pageName, i];
         NSLog(@"%@", imageName);
        
         UIImage *img = [UIImage imageNamed:imageName];
         [imageView setImage:img];
         [imageView setUserInteractionEnabled:YES];
         [self addSubview:imageView];
        
        //×ボタンつける
        CGSize btnSize = CGSizeMake(60.0f, 60.0f);
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(imageView.frame.size.width - 40, -20, btnSize.width, btnSize.height);
        [closeButton setImage:[UIImage imageNamed:@"close-btn.png"] forState:UIControlStateNormal];
        [closeButton setImage:[UIImage imageNamed:@"close-btn-tap.png"] forState:UIControlStateHighlighted];
        [closeButton addTarget:self action:@selector(closeButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
        [imageView addSubview:closeButton];
    }
}

#pragma mark - setUp
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
