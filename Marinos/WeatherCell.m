//
//  WeatherCell.m
//  TigersApp
//
//  Created by timecapsule02 on 2015/06/11.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import "WeatherCell.h"

@implementation WeatherCell{
    NSCalendar *cal;
    NSArray *weatherArray;
}

- (void)awakeFromNib {
    // Initialization code
    
    cal = [NSCalendar currentCalendar];
    cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setWeather{
    [self.dateLbl setText:[self makeDay]];
    
    [self setWeatherParts];
}

-(NSString*)makeDay{
    // NSLog(@"adjustNum:%d",adjustNum);
    NSDate *date=[NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps.day =self.adjustNum;
    
    date = [cal dateByAddingComponents:comps toDate:date options:0];
    //NSLog(@"date:%@",date);
    
    // NsDate => NSString変換用のフォーマッタを作成
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]]; // Localeの指定
    [df setDateFormat:@"MM月dd日"];
    
    return [df stringFromDate:date];
}

-(void)setWeatherParts{
    weatherArray=[[weatherAPIManager sharedManager]weatherAPI:self.adjustNum];
    
    NSString *maxStr = [NSString stringWithFormat:@"%@",[weatherArray valueForKeyPath:@"temp.max"]];
    [self.maxLbl setText:maxStr];
    
    NSString *minStr = [NSString stringWithFormat:@"%@",[weatherArray valueForKeyPath:@"temp.min"]];
    [self.minLbl setText:minStr];
    
    //NSString *imgStr = [NSString stringWithFormat:@"%@",[weatherArray valueForKeyPath:@"weather.icon"]];
    NSString *imgStr =[[weatherArray valueForKeyPath:@"weather.icon"]objectAtIndex:0];
    //NSLog(@"%@",imgStr);
                       
    NSString *imageURLStr =[NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",imgStr];
    NSURL *imageURL = [NSURL URLWithString:imageURLStr];
    NSData *dataURL = [NSData dataWithContentsOfURL:imageURL];
    self.IconImg.image=[[UIImage alloc] initWithData:dataURL];

}

@end
