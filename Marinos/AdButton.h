//
//  AdButton.h
//  Marinos
//
//  Created by eagle014 on 2014/07/23.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdButton : UIButton

@property (nonatomic, copy) NSString *adText;

@end
