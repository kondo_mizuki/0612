//
//  LocationUtility.h
//  BigbrotherMap
//
//  Created by kyo on 2013/06/06.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationUtility : NSObject <CLLocationManagerDelegate>

@property (nonatomic, assign, readonly) float latitude;
@property (nonatomic, assign, readonly) float longitude;
@property (nonatomic, assign, readonly) float altitude;
@property (nonatomic, assign, readonly) BOOL isLocationEnabled;

+ (LocationUtility*) sharedManager;
- (void) getLocation;
- (void) onPauseLocation;
- (void) getSignificantLocation;
- (void) onPauseSignificantLocation;

//- (void) startLocationService:(CLLocationAccuracy)locationAccuracy withDistance:(CLLocationDistance)meter withDegree:(CLLocationDegrees)degree;

@end
