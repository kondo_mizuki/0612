//
//  CheckinViewController.m
//  BigbrotherMap
//
//  Created by kyo on 2013/06/07.
//  Copyright (c) 2013年 kyo. All rights reserved.


#import "CheckinViewController.h"
#import "Util.h"
#import "AlbumDetailViewController.h"
#import "LocationUtility.h"
#import "DatabaseUtility.h"
#import "FileManager.h"
#import "OHAttributedLabel.h"

@interface CheckinViewController ()
@property (nonatomic, strong) UIImageView *myImageView;
@property (weak, nonatomic) UIImage *original;
@property (weak, nonatomic) IBOutlet UIImageView *ssIV;
@property (weak, nonatomic) IBOutlet UIImageView *checkInPanel;

@end

@implementation CheckinViewController
{
    UILabel *titleLabel;
    UILabel *pointLabel;
    UILabel *cardLabel;
    int currentYear;
}
#pragma mark - iOS7対応
- (UIStatusBarStyle)preferredStatusBarStyle {
    //文字を白くする
    return UIStatusBarStyleLightContent;
}

#pragma mark - 画面遷移系
-(void)moveToAlbumDetailViewController:(id)sender
{
    //アルバム詳細に遷移。
    AlbumDetailViewController *detailViewController = [[AlbumDetailViewController alloc] init];
    detailViewController.cardID = [sender tag];
    detailViewController.card_year = currentYear;
    [self.navigationController pushViewController:detailViewController animated:NO];
    [[Util sharedManager] playSound:SOUND_ALBUM];
}

- (void)moveToCameraViewController:(id)sender
{
    if ([LocationUtility sharedManager].isLocationEnabled)
    {
        
//        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//        {
//            MSCameraViewController *camVC = [[MSCameraViewController alloc] init];
//            [camVC setPostLatitude:[[LocationUtility sharedManager] latitude]];
//            [camVC setPostLongitude:[[LocationUtility sharedManager] longitude]];
//            [camVC setPostLatitudeStr:[NSString stringWithFormat:@"%.3f", [[LocationUtility sharedManager] latitude]]];
//            [camVC setPostLongitudeStr:[NSString stringWithFormat:@"%.3f",[[LocationUtility sharedManager] longitude]]];
//            [camVC setPlace_ID:self.place_id];
//            // アニメーション作成
//            CATransition *transition = [CATransition animation];
//            [transition setDuration:0.5f];
//            [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
//            [transition setType:kCATransitionMoveIn];
//            [transition setSubtype:kCATransitionFromBottom];
//            [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
//            [[self navigationController] pushViewController:camVC animated:NO];
//        }
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)backToPreviousView:(UIButton *)sender{
    // アニメーション作成
    CATransition *transition = [CATransition animation];
    [transition setDuration:0.1f];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [transition setType:kCATransitionReveal];
    [transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void) makePartsFromServerObj {
    [[Util sharedManager] playSound:SOUND_PYORO];
    //_place_idをもとにチェックイン場所の情報を取得
    NSMutableDictionary *pDic = [[DatabaseUtility sharedManager] getLocation:self.place_id];
    
    //ヘッダーとトップのチェックイン場所テキストにチェックイン場所を代入。
    NSString *checkInPlaceName = [pDic objectForKey:@"place"];
    titleLabel.text = checkInPlaceName;
    [_mainTextLabel setText:[NSString stringWithFormat:@"「%@」\nにチェックインしました。",checkInPlaceName]];
    //保有ポイント数とか読み込み
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.myCardPoint = [defaults integerForKey:KEY_MY_CARD_POINT];
    NSString *year = [defaults objectForKey:KEY_DATE_YEAR];
    NSString *month = [defaults objectForKey:KEY_DATE_MONTH];
    NSString *day = [defaults objectForKey:KEY_DATE_DAY];
    NSString *dateStr = [NSString stringWithFormat:@"%@年%@月%@日", year ,month, day];
    NSLog(@"currentDate:%@",dateStr);
//    NSLog(@"chechInJSON:%@",sender.jsonDic);
//    //文字列の比較
//    NSString *code = [sender.jsonDic objectForKey:@"code"];
//    if ([code isEqualToString:CHECKIN_CODE_FINISHED]) {
//        //チェックイン済み
//        [_pointAddLabel setText:@"チェックインボーナスは一日一回！"];
//    }else if ([code isEqualToString:CHECKIN_CODE_NORMAL]){
//        //ノーマルチェックイン
//        [self normalCheckInProcess:sender];
//    }else if ([code isEqualToString:CHECKIN_CODE_SPECIAL]){
//        //SPチェックイン
//        [self specialCheckInProcess:sender];
//    }else if ([code isEqualToString:CHECKIN_CODE_ERROR]){
//        //端末時刻不正
//    }
    //ポイント表示
    [_pointLabel setText:[NSString stringWithFormat:@"%d",self.myCardPoint]];
    //カード数表示
    //保有カード数カウントを最新の値にする。
    int cardPossession = [[DatabaseUtility sharedManager] getGettedAllCardCount];
    [_cardLabel setText:[NSString stringWithFormat:@"%d",cardPossession]];
}


#pragma mark - checkinProcess
- (void)normalCheckInProcess{
    [[Util sharedManager] playSound:SOUND_PIRON];
    
    //日付など読み込み
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.myCardPoint = [defaults integerForKey:KEY_MY_CARD_POINT];
    NSString *year = [defaults objectForKey:KEY_DATE_YEAR];
    NSString *month = [defaults objectForKey:KEY_DATE_MONTH];
    NSString *day = [defaults objectForKey:KEY_DATE_DAY];
    NSString *dateStr = [NSString stringWithFormat:@"%@年%@月%@日", year ,month, day];
    NSLog(@"currentDate:%@",dateStr);
     
    CGFloat statusbarHeight = [[Util sharedManager] getStatusbarHeight];
    /*
    UIImageView *checkInPanel = [[UIImageView alloc] initWithFrame:CGRectMake(30, 70+statusbarHeight, 250, 55)];
    [self.view addSubview:checkInPanel];
    */
    CGSize labelSize = CGSizeMake(250.0f, 55.0f);
    //NSString *pointStr = [NSString stringWithFormat:@"%dptゲットしました!", [[sender.jsonDic objectForKey:@"point"] intValue]];
  //  [_pointAddLabel setText:pointStr];
    /*
    if ([[sender.jsonDic objectForKey:@"point"] intValue] == 30)
    {
        checkInPanel.image = [UIImage imageNamed:@"gettext-30point.png"];
    }
    else
    {
        checkInPanel.image = [UIImage imageNamed:@"gettext-10point.png"];
    }
     */
    
    //ポイントの保存
    int addPoint = 10;//仮
    self.myCardPoint = self.myCardPoint + addPoint;
    [defaults setInteger:self.myCardPoint forKey:KEY_MY_CARD_POINT];
    [defaults synchronize];
    //[self.httpObj setCumulationPoint:self.myCardPoint];
    //pointLabel.text = [NSString stringWithFormat:@"%d", self.myCardPoint];
    
    //ノーマルチェックイン情報を保存
    [[DatabaseUtility sharedManager] updateLastCheckIn:dateStr withID:self.place_id];

    //_place_idをもとに場所を取ってくる
    NSMutableDictionary *dicLocation = [[DatabaseUtility sharedManager] getLocation:self.place_id];
    //GoogleAnalytics用のテキスト用意
    NSString *placeName = [dicLocation objectForKey:@"place"];
    NSLog(@"%@",placeName);
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // TODO:GoogleAnalytics
    // This event will also be sent with &cd=Home%20Screen.
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_CHECKIN
                                                          action:@"Normal"
                                                           label:placeName
                                                           value:nil] build]];
}

- (void)specialCheckInProcess{
    _pointAddLabel.hidden = false;
    _ssIV.hidden = false;
    _checkInPanel.hidden = false;
    //日付など読み込み
    // TODO: ver2.0.0
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.myCardPoint = [defaults integerForKey:KEY_MY_CARD_POINT];
    NSString *year = [defaults objectForKey:KEY_DATE_YEAR];
    NSString *month = [defaults objectForKey:KEY_DATE_MONTH];
    NSString *day = [defaults objectForKey:KEY_DATE_DAY];
    NSString *dateStr = [NSString stringWithFormat:@"%@年%@月%@日", year ,month, day];
    NSLog(@"currentDate:%@",dateStr);

    CGFloat statusbarHeight = [[Util sharedManager] getStatusbarHeight];
//    [self.view addSubview:checkInPanel];
    

    //カード保有情報を＋１
    int getCardID =10;//仮
    NSMutableDictionary *myDic = [[DatabaseUtility sharedManager] getSelectCardPrimaryID:getCardID];
    int card_id = [[myDic objectForKey:@"card_id"] intValue];
    currentYear = [[myDic objectForKey:@"card_year"] intValue];
    [[DatabaseUtility sharedManager] updateGetCard:card_id];
    //保有カード数カウントを再度最新の値にする。
    int cardPossession = [[DatabaseUtility sharedManager] getGettedAllCardCount];
    UILabel *gettedcardCountLabel = (UILabel*)[self.view viewWithTag:GETTED_CARD_COUNT_LABEL_TAG];
    gettedcardCountLabel.text = [NSString stringWithFormat:@"%d",cardPossession];
    
    
    CGFloat adjust = [[Util sharedManager] getStatusbarHeight];
    UILabel *spCheckInLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 200 + adjust, 300, 80)];
    spCheckInLabel.numberOfLines = 2;
    spCheckInLabel.font = [UIFont fontWithName:MY_FONT size:24];
    spCheckInLabel.textAlignment = NSTextAlignmentCenter;
    spCheckInLabel.textColor = [UIColor blackColor];
    spCheckInLabel.backgroundColor = [UIColor clearColor];
    spCheckInLabel.hidden = YES;
    if (WIN_SIZE.height == 568) {
        //スペシャルチェックインテキスト表示
//        [self.view addSubview:spCheckInLabel];
        //card_idを元にrare_levelの情報を取得する
        int rareLevelIdentifier = [[DatabaseUtility sharedManager] getCardRareLevelByCardIdentifer:getCardID];
        NSString *rareLevelName = [[DatabaseUtility sharedManager] getRareLevelTextByRareLevelIdentifier:rareLevelIdentifier];
        NSString *spStr = [NSString stringWithFormat:@"本日は%@年%@月%@日\n%@", year, month, day, rareLevelName];
        NSAttributedString *atStr = [NSAttributedString attributedStringWithString:spStr];
        [spCheckInLabel setAttributedText:atStr];
        //spCheckInLabel.text = spStr;
    }
    //カード情報を取得
    //NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:getCardID];
    //NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:card_id year:currentYear];
    NSString *fileName =[myDic objectForKey:@"file_name"];//selectedCardDic[@"file_name"];
    NSData *cardData = [[FileManager sharedManager] loadDirectory:DIR_IS_NOT_APPLICABLE_BACK_UP fileName:fileName];
    UIImage *cardDataImage = [[UIImage alloc] initWithData:cardData];
    UIButton *cardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cardBtn setImage:cardDataImage forState:UIControlStateNormal];
    [cardBtn setImage:cardDataImage forState:UIControlStateHighlighted];
    cardBtn.frame = CGRectMake(0, 0, 58, 90);
    cardBtn.tag = card_id;
    cardBtn.hidden = YES;
    [cardBtn addTarget:self action:@selector(moveToAlbumDetailViewController:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:cardBtn];
    //アニメーション用カード
    _ssIV.image = cardDataImage;
    _ssIV.transform = CGAffineTransformMakeScale(10.0, 10.0);
//    [self.view addSubview:ssIV];
    
    //カード出現アニメーション
    [UIView animateWithDuration:0.8f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
//                         ssIV.center = CGPointMake(cardBtn.center.x, cardBtn.center.y);
                         _ssIV.alpha = 1.0;
                         _ssIV.transform = CGAffineTransformMakeScale(0.0, 0.0);
                     }
                     completion:^(BOOL finished) {
                         [[Util sharedManager] playSound:SOUND_CHUIU];
                     }];
    //SPチェックイン情報を保存
    [[DatabaseUtility sharedManager] updateLastSPCheckIn:dateStr withID:_place_id];
    //checkInPlaceLabel.center = CGPointMake(checkInPlaceLabel.center.x, checkInPlaceLabel.center.y+9);
    
    //_place_idをもとに場所を取ってくる
    NSMutableDictionary *dicLocation = [[DatabaseUtility sharedManager] getLocation:_place_id];
    //GoogleAnalytics用のテキスト用意
    NSString *placeName = dicLocation[@"place"];
    NSLog(@"%@",placeName);
    //GoogleAnalytics用のインスタンス用意。⬇で使うため
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    // TODO:GoogleAnalytics
    // This event will also be sent with &cd=Home%20Screen.
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_CHECKIN
                                                          action:@"Special"
                                                           label:placeName
                                                           value:nil] build]];
}


//-(void)checkInCheck
//{
//    [[Util sharedManager] playSound:SOUND_PYORO];
//    //_place_idをもとにチェックイン場所の情報を取得
//    NSMutableDictionary *pDic = [[DatabaseUtility sharedManager] getLocation:_place_id];
//
//    //ヘッダーとトップのチェックイン場所テキストにチェックイン場所を代入。
//    NSString *checkInPlaceName = pDic[@"place"];
//    titleLabel.text = checkInPlaceName;
//    checkInPlaceLabel.text = [NSString stringWithFormat:@"「%@」\nにチェックインしました！",checkInPlaceName];
//    //保有ポイント数とか読み込み
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    self.myCardPoint = [defaults integerForKey:KEY_MY_CARD_POINT];
//    NSString *year = [defaults objectForKey:KEY_DATE_YEAR];
//    NSString *month = [defaults objectForKey:KEY_DATE_MONTH];
//    NSString *day = [defaults objectForKey:KEY_DATE_DAY];
//    NSString *dateStr = [NSString stringWithFormat:@"%@年%@月%@日", year ,month, day];
//    NSLog(@"currentDate:%@",dateStr);
//    /*
//    int myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
//    int year = (int)[ud integerForKey:@"KEY_YEAR"];
//    int month = (int)[ud integerForKey:@"KEY_MONTH"];
//    int day = (int)[ud integerForKey:@"KEY_DAY"];
//    NSString *dateStr = [NSString stringWithFormat:@"%d年%d月%d日",year,month,day];
//    NSLog(@"currentDate:%@",dateStr);
//     */
//    //チェックインずみかどうかをチェック。
//    NSString *last_check_in_date = pDic[@"last_check_in_date"];
//    NSLog(@"lasetCheckInDate:%@",last_check_in_date);
//    if (![dateStr isEqualToString:last_check_in_date])
//    {
//        //当日チェックインは初めて
//        [self performSelector:@selector(checkIn) withObject:nil afterDelay:0.7];
//    }
//    else
//    {//当日チェックインずみ
//        bottomLabel.text = @"チェックインボーナスは\n１日１回！";
//    }
//    //ポイント表示
//    pointLabel.text = [NSString stringWithFormat:@"%d", self.myCardPoint];
//    //カード数表示
//    //保有カード数カウントを最新の値にする。
//    int cardPossession = [[DatabaseUtility sharedManager] getGettedAllCardCount];
//    UILabel *gettedcardCountLabel = (UILabel*)[self.view viewWithTag:GETTED_CARD_COUNT_LABEL_TAG];
//    gettedcardCountLabel.text = [NSString stringWithFormat:@"%d",cardPossession];
//}



//-(void)checkIn
//{
//    //日付など読み込み
//    //保有ポイント数読み込み
//    // TODO: ver2.0.0
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int myPoint = [defaults integerForKey:KEY_MY_CARD_POINT];
//    NSString *year = [defaults objectForKey:KEY_DATE_YEAR];
//    NSString *month = [defaults objectForKey:KEY_DATE_MONTH];
//    NSString *day = [defaults objectForKey:KEY_DATE_DAY];
//    NSString *dateStr = [NSString stringWithFormat:@"%@年%@月%@日", year ,month, day];
//    NSLog(@"currentDate:%@",dateStr);
//    
//    
//    /*
//    //日付とか読み込み
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    //保有ポイント数読み込み
//    int myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
//    int year = (int)[ud integerForKey:@"KEY_YEAR"];
//    int month = (int)[ud integerForKey:@"KEY_MONTH"];
//    int day = (int)[ud integerForKey:@"KEY_DAY"];
//    NSString *dateStr = [NSString stringWithFormat:@"%d年%d月%d日",year,month,day];
//     */
//    //NSString *moreDateStr = [NSString stringWithFormat:@"%d年%d月%d日",year,month,day+1];
//#warning テスト用強制日付変換です！！！
//    //dateStr = [NSString stringWithFormat:@"%d年%d月%d日",2014,3,15];
//    float statusbarHeight = [[Util sharedManager] getSatusbarHeight];
//    UIImageView *checkInPanel = [[UIImageView alloc] initWithFrame:CGRectMake(30, 70+statusbarHeight, 250, 55)];
//    [self.view addSubview:checkInPanel];
//    
//    //_place_idをもとにスペシャルチェックインをチェック
//    NSMutableArray *spArr = [[DatabaseUtility sharedManager] getSPCheckIn:_place_id];
//    //_place_idをもとに場所を取ってくる
//    NSMutableDictionary *dicLocation = [[DatabaseUtility sharedManager] getLocation:_place_id];
//    //GoogleAnalytics用のテキスト用意
//    NSString *placeName = dicLocation[@"place"];
//    NSLog(@"%@",placeName);
//    //GoogleAnalytics用のインスタンス用意。⬇で使うため
//    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    
//    //保守用の記述
//    NSMutableArray *allCardArray = [[DatabaseUtility sharedManager] getAllCard];
//    //int currentYear = 0;
//    NSMutableDictionary *damyDic = allCardArray[0];
//    currentYear = [damyDic[@"card_year"] intValue];
//    
//    BOOL spFlag = NO;
//    if (spArr != nil &&  [spArr count] >= 1)
//    {
//        for (int i = 0; i<[spArr count]; i++)
//        {
//            //カードDBから１つずつレコード（dic）を取り出して、選択されたカテゴリのカードのみ別アレイに格納。
//            NSMutableDictionary *spDic = spArr[i];
//            //日付チェック
//            NSString *spDateStr = spDic[@"sp_check_in_date"];
//            if ([dateStr isEqualToString:spDateStr] && spFlag == NO)
//            {
//                //スペシャルチェックインフラグを立てる
//                spFlag = YES;
//                
//                //カード保有情報を＋１
//                int getCardID = [spDic[@"card_id"] integerValue];
//                //[[DatabaseUtility sharedManager] updateGetCard:getCardID];
//                [[DatabaseUtility sharedManager] updateGetCard:getCardID year:currentYear];
//                //保有カード数カウントを再度最新の値にする。
//                int cardPossession = [[DatabaseUtility sharedManager] getGettedAllCardCount];
//                UILabel *gettedcardCountLabel = (UILabel*)[self.view viewWithTag:GETTED_CARD_COUNT_LABEL_TAG];
//                gettedcardCountLabel.text = [NSString stringWithFormat:@"%d",cardPossession];
//                
//                checkInPanel.image = [UIImage imageNamed:@"gettext-special.png"];
//                checkInPanel.center = CGPointMake(checkInPanel.center.x-15, checkInPanel.center.y-15+statusbarHeight);
//                checkInPanel.transform = CGAffineTransformMakeRotation(M_PI * -13 / 180.0);
//                checkInPanel.hidden = YES;
//
//                //スペシャルチェックインテキスト表示
//                UILabel *spCheckInLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,220,300,80)];
//                spCheckInLabel.numberOfLines = 2;
//                spCheckInLabel.font = [UIFont fontWithName:MY_FONT size:24];
//                spCheckInLabel.textAlignment = NSTextAlignmentCenter;
//                spCheckInLabel.textColor = [UIColor blackColor];
//                spCheckInLabel.backgroundColor = [UIColor clearColor];
//                spCheckInLabel.hidden = YES;
//                [self.view addSubview:spCheckInLabel];
//                NSString *spStr = [NSString stringWithFormat:@"本日は%@年%@月%@日\n%@開催日です。",year,month,day,@"ホームゲーム"];
//                spCheckInLabel.text = spStr;
//                //カード情報を取得
//                //NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:getCardID];
//                NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:getCardID year:currentYear];
//                NSString *fileName = selectedCardDic[@"file_name"];
//                UIImage *ss = [UIImage imageNamed:fileName];
//                UIButton *cardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//                [cardBtn setImage:ss forState:UIControlStateNormal];
//                [cardBtn setImage:ss forState:UIControlStateHighlighted];
//                cardBtn.frame = CGRectMake(0, 0, 58, 90);
//                cardBtn.center = CGPointMake(checkInPanel.center.x+140, checkInPanel.center.y+10);//y+40
//                cardBtn.tag = getCardID;
//                cardBtn.hidden = YES;
//                [cardBtn addTarget:self action:@selector(moveToAlbumDetailViewController:) forControlEvents:UIControlEventTouchUpInside];
//                [self.view addSubview:cardBtn];
//                //アニメーション用カード
//                UIImageView *ssIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 58, 90)];
//                ssIV.image = ss;
//                ssIV.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
//                ssIV.alpha = 0.2;
//                ssIV.transform = CGAffineTransformMakeScale(10.0, 10.0);
//                [self.view addSubview:ssIV];
//                
//                //カード出現アニメーション
//                [UIView animateWithDuration:0.8f
//                                      delay:0.0f
//                                    options:UIViewAnimationOptionCurveLinear
//                                 animations:^{
//                                     ssIV.center = CGPointMake(cardBtn.center.x, cardBtn.center.y);
//                                     ssIV.alpha = 1.0;
//                                     ssIV.transform = CGAffineTransformMakeScale(1.0, 1.0);
//                                 }
//                                 completion:^(BOOL finished) {
//                                     [[Util sharedManager] playSound:SOUND_CHUIU];
//                                     cardBtn.hidden = NO;
//                                     ssIV.hidden = YES;
//                                     checkInPanel.hidden = NO;
//                                     spCheckInLabel.hidden = NO;
//                                 }];
//                //SPチェックイン情報を保存
//                [[DatabaseUtility sharedManager] updateLastSPCheckIn:dateStr withID:_place_id];
//                checkInPlaceLabel.center = CGPointMake(checkInPlaceLabel.center.x, checkInPlaceLabel.center.y+9);
//                
//                // TODO:GoogleAnalytics
//                // This event will also be sent with &cd=Home%20Screen.
//                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_CHECKIN
//                                                                      action:@"Special"
//                                                                       label:placeName
//                                                                       value:nil] build]];
//
//                //return;
//            }
//        }
//    }
//    if (spFlag == NO)
//    {
//        [[Util sharedManager] playSound:SOUND_PIRON];
//
//        NSMutableDictionary *pDic = [[DatabaseUtility sharedManager] getLocation:_place_id];
//        int checkInPoint = [pDic[@"point"] integerValue];
//        myPoint = myPoint+checkInPoint;
//        //ノーマルチェックイン
//        if (checkInPoint == 30)
//        {
//            checkInPanel.image = [UIImage imageNamed:@"gettext-30point.png"];
//        }
//        else
//        {
//            checkInPanel.image = [UIImage imageNamed:@"gettext-10point.png"];
//        }
//    }
//    //ポイント保存＆再表示
//    [defaults setInteger:myPoint forKey:KEY_MY_CARD_POINT];
//    //[defaults setInteger:myPoint forKey:@"KEY_MYPOINT"];
//    pointLabel.text = [NSString stringWithFormat:@"%d", myPoint];
//    //ノーマルチェックイン情報を保存
//    //SPチェックインのデバッグ用に再度書き直し。
//    dateStr = [NSString stringWithFormat:@"%@年%@月%@日",year,month,day];
//    [[DatabaseUtility sharedManager] updateLastCheckIn:dateStr withID:_place_id];
//    // TODO:GoogleAnalytics
//    // This event will also be sent with &cd=Home%20Screen.
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_CHECKIN
//                                                          action:@"Normal"
//                                                           label:placeName
//                                                           value:nil] build]];
//}



- (void)setUpParameters
{
    NSMutableDictionary *pDic = [[DatabaseUtility sharedManager] getLocation:self.place_id];
    self.latitude = [pDic[@"latitude"] doubleValue];
    self.longitude = [pDic[@"longitude"] doubleValue];
}

- (void)setUpHeaderParts
{

}

- (void)setUpMiddlePanel
{
    CGFloat topBarHeight = [[Util sharedManager] getAdjustedHeaderPartsHeight];
    CGFloat panelWidth, panelHeight;
    CGFloat adjust = 3.0f;
    NSString *panelImgName;
    panelWidth = 310.0f;
    if (WIN_SIZE.height == 568) {
        panelHeight = 305.0f;
        panelImgName =@"checkin-panel.png";
    } else {
        panelHeight = 220.0f;
        panelImgName = @"checkin-panel-small.png";
    }
    UIImageView *topPanel = [[UIImageView alloc] initWithFrame:CGRectMake((WIN_SIZE.width - panelWidth) / 2, topBarHeight + adjust , panelWidth, panelHeight)];
    topPanel.image = [UIImage imageNamed:panelImgName];
//    [self.view addSubview:topPanel];
    
    CGFloat childImgWidth = 105.0f;
    CGFloat childImagHeight = 65.0f;
    CGFloat sideAdjust = 10.0f;
    UIImageView *cardImg = [[UIImageView alloc] initWithFrame:CGRectMake(panelWidth - (childImgWidth + sideAdjust), panelHeight - childImagHeight - sideAdjust, childImgWidth, childImagHeight)];
    [cardImg setImage:[UIImage imageNamed:@"card-panel.png"]];
    CGFloat labelWidth = 80.0f;
    CGFloat labelHeight = 32.0f;
    CGFloat labelFontSize = 16.0f;
    CGFloat childAdjustHeight;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            childAdjustHeight = childImagHeight * 1.5f;//65*1.25
            break;
        case NSOrderedSame:
            childAdjustHeight = childImagHeight * 1.25f;//65*1.25
            break;
        case NSOrderedDescending:
            childAdjustHeight = childImagHeight * 1.25f;//65*1.25
            break;
        default:
            break;
    }
    cardLabel = [[UILabel alloc] initWithFrame:CGRectMake((childImgWidth - labelWidth) / 2, (childAdjustHeight - labelHeight) / 2, labelWidth, labelHeight)];
    [cardLabel setTextAlignment:NSTextAlignmentCenter];
    [cardLabel setFont:[UIFont fontWithName:MY_FONT size:labelFontSize]];
    [cardLabel setTextColor:[UIColor blackColor]];
    [cardLabel setBackgroundColor:[UIColor clearColor]];
//    [cardImg addSubview:cardLabel];
//    [topPanel addSubview:cardImg];
    
    UIImageView *pointImg = [[UIImageView alloc] initWithFrame:CGRectMake(panelWidth - (childImgWidth + sideAdjust) * 2, panelHeight - childImagHeight - sideAdjust, childImgWidth, childImagHeight)];
    [pointImg setImage:[UIImage imageNamed:@"point-panel.png"]];
    pointLabel = [[UILabel alloc] initWithFrame:CGRectMake((childImgWidth - labelWidth) / 2, (childAdjustHeight - labelHeight) / 2, labelWidth, labelHeight)];
    [pointLabel setTextAlignment:NSTextAlignmentCenter];
    [pointLabel setFont:[UIFont fontWithName:MY_FONT size:labelFontSize]];
    [pointLabel setTextColor:[UIColor blackColor]];
    [pointLabel setBackgroundColor:[UIColor clearColor]];
//    [pointImg addSubview:pointLabel];
//    [topPanel addSubview:pointImg];
}

- (void)setUpFooterParts
{
    CGFloat sideAdjust = 20.0f;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            sideAdjust = 20.0f;
            break;
        case NSOrderedSame:
            sideAdjust = 0.0f;
            break;
        case NSOrderedDescending:
            sideAdjust = 0.0f;
            break;
        default:
            break;
    }
    CGFloat btnWidth = WIN_SIZE.width;
    CGFloat btnHeight = 50.0f;
    UIButton *photoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [photoBtn setImage:[UIImage imageNamed:@"memorialcam_on.png"] forState:UIControlStateNormal];
    [photoBtn setImage:[UIImage imageNamed:@"memorialcam_off.png"] forState:UIControlStateHighlighted];
    photoBtn.frame = CGRectMake(0.0f ,WIN_SIZE.height - btnHeight - sideAdjust, btnWidth, btnHeight);
    [photoBtn addTarget:self action:@selector(moveToCameraViewController:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:photoBtn];
    
    CGFloat mtHight = 105.0f;
    
    CGFloat imgWidth = 250.0f;
    CGFloat imgHeight = 40.0f;
    UIImageView *memorialImg = [[UIImageView alloc] initWithFrame:CGRectMake((WIN_SIZE.width - imgWidth) / 2, WIN_SIZE.height - btnHeight - mtHight - imgHeight - sideAdjust, imgWidth, imgHeight)];
    [memorialImg setImage:[UIImage imageNamed:@"memorial-heading.png"]];
//    [self.view addSubview:memorialImg];

}



#pragma mark - delegate - webView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSLog(@"%@", [request.URL path]);
        NSLog(@"%@", [request.URL absoluteString]);
        
        return NO;
    }
    return YES;
}
- (IBAction)takePhoto:(id)sender {
    [self moveToCameraViewController];

}

- (void)moveToCameraViewController
{
    if ([LocationUtility sharedManager].isLocationEnabled) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            CameraViewController *controller = [[CameraViewController alloc] init];
            [controller setFrom:FromCameraViewController];
            [self presentViewController:controller animated:YES completion:^{
                [controller showCamera];
            }];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"カメラの使用が許可されていません。設定を確認して下さい。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}


//画像が選択された時に呼ばれるデリゲートメソッド
//-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingImage:(UIImage*)image editingInfo:(NSDictionary*)editingInfo{
//    
//    [self dismissModalViewControllerAnimated:NO];  // モーダルビューを閉じる
//    
//    // 渡されてきた画像を加工フェーズへ移動
//    self.original = image;
////    [self performSegueWithIdentifier:@"CameraToFrame" sender:self];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CameraStoryboard" bundle:nil];
//    FrameSetVC *fvc = [storyboard instantiateViewControllerWithIdentifier:@"FrameSetVC"];
//    fvc.original = self.original;
//    [self.navigationController pushViewController:fvc animated:YES];
//}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    if ([[segue identifier] isEqualToString:@"CameraToFrame"]) {
//        FrameSetVC *fvc = [segue destinationViewController];
//        fvc.original = self.original;
//    }
//}
//- (IBAction)checkinViewReturnActionForSegue:(UIStoryboardSegue *)segue
//{
//    NSLog(@"Checkin view return action invoked.");
//}

#pragma mark - LifeCycle
- (void)viewWillAppear:(BOOL)animated
{
    //UA-44275326-1
    // TODO:GoogleAnalytics
    self.screenName = @"CheckInVC";

    [super viewWillAppear:animated];
    
    //    // 広告の設定
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
