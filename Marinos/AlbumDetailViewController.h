//
//  AlbumDetailViewController.h
//  Marinos
//
//  Created by 菊地 拓也 on 2013/08/16.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumDetailViewController : GAITrackedViewController
{
    UIView *infoView;
    UIImageView *cardImage;
}
@property (strong, nonatomic) id detailItem;
@property (nonatomic,assign) UIImageView *imageView;
@property (nonatomic,assign) int cardID;
@property (nonatomic,assign) int card_year;
@property (nonatomic,strong) NSString *albumName;
@property(nonatomic,strong) NSDictionary *possessionDic;
@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *leftLbl;
@property (weak, nonatomic) IBOutlet UILabel *rightLbl;

@end

bool opend;