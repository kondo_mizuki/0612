//
//  FrameCameraVC.h
//  TigersApp
//
//  Created by 菊地 拓也 on 2015/04/10.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ADG/ADGManagerViewController.h>
#import "CameraViewController.h"

@interface FrameCameraVC : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIWebViewDelegate, ADGManagerViewControllerDelegate>
{
    ADGManagerViewController *ADGViewController;
}
@property (nonatomic, retain) ADGManagerViewController *adGManagerViewController;

@end
