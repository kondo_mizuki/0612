//
//  SoundUtility.h
//
//  Created by eagle014 on 2014/04/16.
//  Copyright (c) 2014年 Time Capsule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#define FEDE_SEED 0.01f
#define VOLUME_SEED 0.005f
#define SEQUENCE_INTERVAL 0.5f
#define AVEPW4CH 0

//<!-- sound_name -->
#define SOUND_TAB_BUTTON @"sound_tab_button"
#define SOUND_HTTP_OK @"sound_http_ok"
#define SOUND_HTTP_NG @"sound_http_ng"
#define SOUND_SETTING_ON @"sound_setting_on"
#define SOUND_SETTING_OFF @"sound_setting_off"
#define SOUND_CARD_BUTTON @"sound_card_button"
#define SOUND_CARD_SHOW @"sound_card_show"
#define SOUND_ALBUM_TO_DETAIL @"sound_album_to_detail"
#define SOUND_ALBUM_HOMEGAME_INVALID @"sound_album_homegame_invaild"
#define SOUND_EXTENSION_MP3 @"mp3"
/*
#define SOUND_TITLE_BG [[SoundUtility alloc] initWithFileName:@"kouchanojikan" ofType:@"mp3" repeat:YES delegate:nil]
#define SOUND_BOOK_BG [[SoundUtility alloc] initWithFileName:@"game_maoudamashii_5_town17" ofType:@"mp3" repeat:YES delegate:nil]
#define SOUND_CHAPTER_BG [[SoundUtility alloc] initWithFileName:@"game_maoudamashii_5_town17" ofType:@"mp3" repeat:YES delegate:nil]
#define SOUND_GAME_BG [[SoundUtility alloc] initWithFileName:@"thinking2" ofType:@"mp3" repeat:YES delegate:nil]
#define SOUND_GAME_RIGHT [[SoundUtility alloc] initWithFileName:@"right2" ofType:@"mp3" delegate:nil]
#define SOUND_GAME_WRONG [[SoundUtility alloc] initWithFileName:@"mistake" ofType:@"mp3" delegate:nil]
#define SOUND_RESULT_GOOD [[SoundUtility alloc] initWithFileName:@"game_maoudamashii_9_jingle09" ofType:@"mp3" delegate:nil]
#define SOUND_RESULT_BAD [[SoundUtility alloc] initWithFileName:@"game_maoudamashii_9_jingle10" ofType:@"mp3" delegate:nil]
#define SOUND_SYSTEM_OK [[SoundUtility alloc] initWithFileName:@"se_maoudamashii_system10" ofType:@"mp3" delegate:nil]
#define SOUND_SYSTEM_CANCEL [[SoundUtility alloc] initWithFileName:@"se_maoudamashii_system19" ofType:@"mp3" delegate:nil]
 */
@class SoundUtility;

@protocol SoundUtilityDelegate
- (void) audioSequence:(double)currentTime;
- (void) audioFadein:(double)volume;
- (void) audioFadeout:(double)volume;
@optional
@end

@interface SoundUtility : NSObject <AVAudioPlayerDelegate>
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, weak) id <SoundUtilityDelegate> delegate;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSTimer *timerFade;
@property (nonatomic, assign) double duration;
@property (nonatomic, assign) float power;
@property (nonatomic, assign) float currentVolume;
@property (nonatomic, assign) BOOL fading;

- (id) initWithFileName:(NSString *)filename ofType:(NSString *)ofType delegate:(id)targetDelegate;
- (id) initWithFileName:(NSString *)filename ofType:(NSString *)ofType repeat:(BOOL)repeat delegate:(id)targetDelegate;
- (void) audioStart;
- (void) audioStartWithVolume:(float)volume;
- (void) audioStart:(BOOL)fade currentTime:(double)currentTime volume:(float)volume;
- (void) audioStop:(BOOL)fade;
- (void) audioPause;
- (void) audioPlayThreaded:(id)threadAudio;
- (void) setVolume:(double)volume;

@end
