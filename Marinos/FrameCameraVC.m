//
//  FrameCameraVC.m
//  TigersApp
//
//  Created by 菊地 拓也 on 2015/04/10.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import "FrameCameraVC.h"
#import "Util.h"
#import "LocationUtility.h"

@interface FrameCameraVC ()

@property (assign, nonatomic) bool adGenerationShowFlag;


- (IBAction)takePhoto:(id)sender;

@end

@implementation FrameCameraVC
@synthesize adGManagerViewController = ADGViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.adGenerationShowFlag = false;
    // Do any additional setup after loading the view.
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_TIMELINE]];
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?latitude=%@&longitude=%@", [[Util sharedManager] myServer], PHP_PROGRAM_TRAINSEARCH, @([LocationUtility sharedManager].latitude), @([LocationUtility sharedManager].longitude)]];
//    NSLog(@"%@", url);
//    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.adGenerationShowFlag == false && WIN_SIZE.height > 567) {
        self.adGenerationShowFlag = true;
        [self showADGeneration];
    }
    if(ADGViewController){
        [ADGViewController resumeRefresh];
    }
}

-(void) showADGeneration {
    int screenHeight = [[UIScreen mainScreen] bounds].size.height;
    CGFloat tabBarHeight = self.tabBarController.tabBar.bounds.size.height;
    screenHeight = screenHeight - 50 - tabBarHeight;
    NSDictionary *adgparam = @{
                               @"locationid" : @"10723", //管理画面から払い出された広告枠ID
                               @"adtype" : @(kADG_AdType_Free), //管理画面にて入力した枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free
                               @"originx" : @(0), //広告枠設置起点のx座標 ※下記参照
                               @"originy" : @(screenHeight), //広告枠設置起点のy座標 ※下記参照
                               @"w" : @(320), //広告枠横幅 ※下記参照
                               @"h" : @(50)  //広告枠高さ ※下記参照
                               };
    ADGManagerViewController *adgvc = [[ADGManagerViewController alloc]initWithAdParams :adgparam :self.view];
    self.adGManagerViewController = adgvc;
    //    [adgvc release];
    ADGViewController.delegate = self;
    [ADGViewController loadRequest];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    if ([[segue identifier] isEqualToString:@"CameraToFrame"]) {
//        
//        FrameSetVC *fvc = [segue destinationViewController];
//        fvc.original = self.original;
//    }else if ([[segue identifier] isEqualToString:@"WebToResult"]) {
//        FramePostFinishVC *fvc = [segue destinationViewController];
//        [fvc setPhotPath:self.photoPath withIsFromPost:NO];
//        fvc.titleStr = @"タイムライン";
//    }
}

- (IBAction)takePhoto:(id)sender {
    
    [self moveToCameraViewController];
}

- (void)moveToCameraViewController
{
    if ([LocationUtility sharedManager].isLocationEnabled) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            CameraViewController *controller = [[CameraViewController alloc] init];
            [controller setFrom:FromCameraViewController];
            [self presentViewController:controller animated:YES completion:^{
                [controller showCamera];
            }];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"カメラの使用が許可されていません。設定を確認して下さい。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"LocationUnebleAlert", @"位置情報が取得できないアラート_本文") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}


//- (IBAction)pushTimeline:(id)sender {
////    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MapStoryboard" bundle:nil];
////    TimeLineViewController *fvc = [storyboard instantiateViewControllerWithIdentifier:@"TimeLineViewController"];
////    [self.navigationController pushViewController:fvc animated:YES];
//}
//
//
//    //画像が選択された時に呼ばれるデリゲートメソッド
//-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingImage:(UIImage*)image editingInfo:(NSDictionary*)editingInfo{
//    
//    [self dismissModalViewControllerAnimated:NO];  // モーダルビューを閉じる
//    
//    // 渡されてきた画像を加工フェーズへ移動
//    self.original = image;
//    [self performSegueWithIdentifier:@"CameraToFrame" sender:self];
//}
//
//- (IBAction)showRestoreInfo:(id)sender {
//    NSString *message = @"ここに説明文が入ります。";
//    NSString *title = @"購入履歴の復元とは?";
//    if(IS_OS_8_OR_LATER){
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
//        
//        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            // 何もしない
//        }]];
//        [self presentViewController:alertController animated:YES completion:nil];
//    }else{
//        // アラートを表示
//        UIAlertView* alert= [[UIAlertView alloc] initWithTitle:title message:message delegate:nil
//                                             cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
//}
//
//#pragma mark - delegate - webView
//- (BOOL)webView:(UIWebView *)webView
//shouldStartLoadWithRequest:(NSURLRequest *)request
// navigationType:(UIWebViewNavigationType)navigationType
//{
//    if (navigationType == UIWebViewNavigationTypeLinkClicked)
//    {
//        NSLog(@"%@", [request.URL path]);
//        NSLog(@"%@", [request.URL absoluteString]);
//        
//        self.photoPath = [request.URL absoluteString];
//        [self performSegueWithIdentifier:@"WebToResult" sender:self];
//        
//        return NO;
//    }
//    return YES;
//}

- (void)ADGManagerViewControllerReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerReceiveAd");
}

- (void)ADGManagerViewControllerFailedToReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerFailedToReceiveAd");
}


@end
