//
//  NearPhotoViewController.m
//  Marinos
//
//  Created by eagle014 on 2014/03/05.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import "NearPhotoViewController.h"
#import "Util.h"

@interface NearPhotoViewController ()

@end

@implementation NearPhotoViewController
#pragma mark - iOS7対応
- (UIStatusBarStyle)preferredStatusBarStyle {
    //文字を白くする
    return UIStatusBarStyleLightContent;
}

#pragma mark - LifeCycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setUpParts];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setupParts
- (void)setUpHeaderParts
{
    CGFloat topBarHeight = [[Util sharedManager] getAdjustedHeaderPartsHeight];
    NSString *iOS_adjust;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            iOS_adjust = @"new_topbar-none_iOS6_44pxl.png";
            break;
        case NSOrderedSame:
            iOS_adjust =@"new_topbar-none_iOS7_64pxl.png";
            break;
        case NSOrderedDescending:
            iOS_adjust =@"new_topbar-none_iOS7_64pxl.png";
            break;
        default:
            break;
    }
    UIImage *topBarImg = [UIImage imageNamed:iOS_adjust];
    UIImageView *topBarImgView = [[UIImageView alloc] initWithImage:topBarImg];
    [topBarImgView setFrame:CGRectMake(0.0f, 0.0f, WIN_SIZE.width, topBarHeight)];
    [self.view addSubview:topBarImgView];
    
    CGFloat btnSize = 23.0f;
    CGFloat topbarAdjust = 7.0f;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0.0f, topBarHeight - btnSize - topbarAdjust, btnSize, btnSize);
    [backBtn setImage:[UIImage imageNamed:@"title-arrow.png"] forState:UIControlStateNormal];
    [backBtn setImage:[UIImage imageNamed:@"title-arrow.png"] forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(backToPreviousView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    CGFloat labelWidth = 200.0f;
    CGFloat labelHeight = 36.0f;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((WIN_SIZE.width - labelWidth) / 2, topBarHeight - labelHeight, labelWidth, labelHeight)];
    titleLabel.font = [UIFont fontWithName:MY_FONT size:17];
    titleLabel.text = @"周辺投稿写真";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
}

- (void)setUpParts{
    [self setUpHeaderParts];
}

@end
