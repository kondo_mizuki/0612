//
//  CustomScrollView.h
//  Marinos
//
//  Created by eagle014 on 2014/04/14.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomScrollView;

@protocol CustomScrollViewDelegate <NSObject>

@optional
- (void)tutorialView:(CustomScrollView *)scrollView_sender blindView:(UIView *)blindView_sender;
- (void)scrollViewDidEndDecelerating:(CustomScrollView *)scrollView_sender;
//- (void)scrollViewDidScroll:(CustomScrollView *)scrollView_sender;
//- (void)scrollViewDidEndScrollingAnimation:(CustomScrollView *)scrollView_sender;

@end

@interface CustomScrollView : UIScrollView

@property (nonatomic, weak) id <CustomScrollViewDelegate> customDelegate;

@property (nonatomic, copy) NSString *pageName;
@property (nonatomic, assign) NSInteger pageNumber;

- (void) showTutorialPage;
@end
