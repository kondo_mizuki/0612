#import <UIKit/UIKit.h>

@interface RankingVC : GAITrackedViewController <UIApplicationDelegate, UIAlertViewDelegate>

#define RANKING_PARAMETER_ZERO @"ranking_parameter_0.png"
#define RANKING_PARAMETER_R50 @"ranking_parameter_R50.png"
#define RANKING_PARAMETER_R100 @"ranking_parameter_R100.png"
#define RANKING_PARAMETER_G50 @"ranking_parameter_G50.png"
#define RANKING_PARAMETER_G100 @"ranking_parameter_G100.png"
#define RANKING_PARAMETER_B50 @"ranking_parameter_B50.png"
#define RANKING_PARAMETER_B100 @"ranking_parameter_B100.png"

#define FONT_MAX_PERCENT 22
#define FONT_MIDDLE_PERCENT 18
#define FONT_MIN_PERCENT 12

#define FONT_SOLO_HEADER 20

typedef enum {
    ZERO = 0,
    R50,
    R100,
    G50,
    G100,
    B50,
    B100
} ParameterType;

// index
- (void) setIndexStage;
- (void) removeIndexStage;
- (void) initData;

// analizing
- (void) setAnalizyngAnim;

// detail
- (void) setTeamDetailStage;
- (void) setSoloDetailStage;
- (void) removeTeamDetailStage;
- (void) removeSoloDetailStage;
- (void) setCategoryTitle;
- (void) setTeamCategoryParameter;
- (void) setSoloCategoryParameter;
- (void) initCategoryPercent;
- (void) setCategoryPercent;
- (void) changeParameter;

// callback
-(void) teamCallBack:(UIButton*)button;
-(void) soloCallBack:(UIButton*)button;
-(void) categoryLeftCallBack:(UIButton*)button;
-(void) categoryRightCallBack:(UIButton*)button;

// responder
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

@end
