//
//  SoundUtility.m
//
//  Created by eagle014 on 2014/04/16.
//  Copyright (c) 2014年 Time Capsule. All rights reserved.
//

#import "SoundUtility.h"


@implementation SoundUtility
- (id)initWithFileName:(NSString *)filename ofType:(NSString *)ofType delegate:(id)targetDelegate{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:ofType];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [self.audioPlayer setMeteringEnabled:YES];
    [self.audioPlayer prepareToPlay];
    self.audioPlayer.delegate = targetDelegate;
    self.delegate = targetDelegate;
    self.duration = self.audioPlayer.duration;
    self.fading = NO;
    self.currentVolume = 0.5f;
    return self;
}

- (id)initWithFileName:(NSString *)filename ofType:(NSString *)ofType repeat:(BOOL)repeat delegate:(id)targetDelegate{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:ofType];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [self.audioPlayer setMeteringEnabled:YES];
    [self.audioPlayer prepareToPlay];
    if (repeat) {
        [self.audioPlayer setNumberOfLoops:-1];
    } else {
        [self.audioPlayer setNumberOfLoops:1];
    }
    self.audioPlayer.delegate = targetDelegate;
    self.delegate = targetDelegate;
    self.duration = self.audioPlayer.duration;
    self.fading = NO;
    self.currentVolume = 0.5f;
    return self;
}

- (void)audioSequence{
    if(self.audioPlayer == nil) return;
    if(self.audioPlayer.playing){
        [self.audioPlayer updateMeters];
        self.power = [self.audioPlayer averagePowerForChannel:AVEPW4CH]; // averagePowerForChannel OR peakPowerForChannel
        [self.delegate audioSequence:self.audioPlayer.currentTime];
	}
}

- (void) audioStart{
    if(self.audioPlayer.playing) return;
    [self.audioPlayer play];
    //[NSThread detachNewThreadSelector:@selector(audioPlayThreaded:) toTarget:self withObject:self.audioPlayer];
}
- (void) audioStartWithVolume:(float)volume{
    if(self.audioPlayer.playing) return;
    self.currentVolume = volume;
    self.audioPlayer.volume = volume;
    [NSThread detachNewThreadSelector:@selector(audioPlayThreaded:) toTarget:self withObject:self.audioPlayer];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:SEQUENCE_INTERVAL target:self selector:@selector(audioSequence) userInfo:nil repeats:YES];
}

- (void)audioStart:(BOOL)fade currentTime:(double)currentTime volume:(float)volume{
    if(self.audioPlayer.playing) return;
    self.currentVolume = volume;
    if(fade){
        if(self.fading == NO){
            [self.audioPlayer setVolume:0.0f];
            self.timerFade = [NSTimer scheduledTimerWithTimeInterval:FEDE_SEED target:self selector:@selector(audioFadein) userInfo:nil repeats:YES];
        }
    }
    self.audioPlayer.currentTime = currentTime;
    [NSThread detachNewThreadSelector:@selector(audioPlayThreaded:) toTarget:self withObject:self.audioPlayer];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:SEQUENCE_INTERVAL target:self selector:@selector(audioSequence) userInfo:nil repeats:YES];
}

- (void)audioStop:(BOOL)fade{
    if(self.audioPlayer == nil) return;
    
    if(fade){
        if(self.fading == NO) self.timerFade = [NSTimer scheduledTimerWithTimeInterval:FEDE_SEED target:self selector:@selector(audioFadeout) userInfo:nil repeats:YES];
    } else {
        if(self.audioPlayer.playing){
            [self.audioPlayer prepareToPlay];
            [self.audioPlayer stop];
            [self.audioPlayer setCurrentTime:0.0f];
        }
    }
    
}

- (void)audioPause{
    if(self.audioPlayer == nil) return;
    if(self.audioPlayer.playing){
        [self.audioPlayer pause];
    }
}

- (void)audioFadein{
    
    if(self.audioPlayer.playing){
        if((self.audioPlayer.volume + VOLUME_SEED) < self.currentVolume){
            self.fading = YES;
            [self.delegate audioFadein:self.audioPlayer.volume];
            [self.audioPlayer setVolume:self.audioPlayer.volume + VOLUME_SEED];
        } else {
            self.fading = NO;
            if([self.timerFade isValid]) [self.timerFade invalidate];
        }
    }
    
}

- (void)audioFadeout{
    
    if(self.audioPlayer.volume <= 0.0f){
        self.fading = NO;
        if([self.timerFade isValid]) [self.timerFade invalidate];
        [self audioStop:NO];
    } else {
        self.fading = YES;
        [self.delegate audioFadeout:self.audioPlayer.volume];
        [self.audioPlayer setVolume:self.audioPlayer.volume - VOLUME_SEED];
    }
    
}

-(void)audioPlayThreaded:(id)threadAudio{
    if(threadAudio == nil) return;
    //[(AVAudioPlayer *)threadAudio prepareToPlay];
    [(AVAudioPlayer *)threadAudio play];
}

-(void)setVolume:(double)volume{
    [self.audioPlayer setVolume:volume];
}

@end
