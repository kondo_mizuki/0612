//
//  LocationUtility.m
//  BigbrotherMap
//
//  Created by kyo on 2013/06/06.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "LocationUtility.h"
#import "DatabaseUtility.h"
@interface LocationUtility()

@property (nonatomic, assign, readwrite) float latitude;
@property (nonatomic, assign, readwrite) float longitude;
@property (nonatomic, assign, readwrite) float altitude;
@property (nonatomic, assign, readwrite) BOOL isLocationEnabled;

@property (nonatomic, strong) CLLocationManager *mLocationManager;
@property (nonatomic, assign) BOOL isMagneticNorth;

@end

@implementation LocationUtility

static LocationUtility *sharedInstance = nil;

#pragma mark - singleton method
+ (LocationUtility *)sharedManager {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone {
    return self;
}

#warning Nifty位置切換
- (void) getLocation{
    // 使用可能かの確認
    if ([CLLocationManager locationServicesEnabled]) {
        [self requestUseLocation];
        [self startLocationService:kCLLocationAccuracyBest withDistance:PUSH_UPDATE_METER withDegree:1.0];
        
    } else {
        [self cannotUseLocationAlert];
    }
}

- (void) onPauseLocation {
    if (nil != self.mLocationManager && [CLLocationManager locationServicesEnabled])
        [self.mLocationManager stopUpdatingLocation]; //測位停止
}

- (void)getSignificantLocation {
    // 使用可能かの確認
    if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [self requestUseLocation];
        [self.mLocationManager startMonitoringSignificantLocationChanges];
    } else {
        [self cannotUseLocationAlert];
    }
}

- (void)requestUseLocation {
    if (self.mLocationManager == nil) {
        self.mLocationManager = [[CLLocationManager alloc] init];
        self.mLocationManager.delegate = self;
        if(IS_OS_8_OR_LATER) {
            // Use one or the other, not both. Depending on what you put in info.plist
//            [self.mLocationManager requestWhenInUseAuthorization];
            [self.mLocationManager requestAlwaysAuthorization];
        }
    }
}

- (void)cannotUseLocationAlert {
    UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"" message:@"位置情報を使用できませんでした" delegate:nil
                                         cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void) onPauseSignificantLocation {
    if (nil != self.mLocationManager && [CLLocationManager locationServicesEnabled])
        [self.mLocationManager stopMonitoringSignificantLocationChanges]; //測位停止
}

- (void) startLocationService:(CLLocationAccuracy)locationAccuracy withDistance:(CLLocationDistance)meter withDegree:(CLLocationDegrees)degree {
    //標準位置情報
    if ([CLLocationManager locationServicesEnabled]) {
        //精度・更新距離の設定
        [self.mLocationManager setDesiredAccuracy:locationAccuracy];
        [self.mLocationManager setDistanceFilter:meter];
        [self.mLocationManager setPausesLocationUpdatesAutomatically:NO];
        [self.mLocationManager startUpdatingLocation];
        if(IS_OS_8_OR_LATER)[self.mLocationManager requestWhenInUseAuthorization];
        
        //コンパス情報
        if ([CLLocationManager headingAvailable]) {
            self.isMagneticNorth = NO;
            [self.mLocationManager setHeadingOrientation:CLDeviceOrientationPortrait];
            [self.mLocationManager setHeadingFilter:degree];
            [self.mLocationManager startUpdatingHeading];
        }
    }
}

// 位置情報更新時
- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"位置情報が更新されました");
    self.isLocationEnabled = YES;
    
    // 緯度経度・標高を調べる
    CLLocation *locationData = [locations lastObject];
    self.latitude = locationData.coordinate.latitude;
    self.longitude = locationData.coordinate.longitude;
    self.altitude = locationData.altitude;

#warning データベースが起動するまで対応不可
/*
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
    
        // 設定する前に、設定済みの通知をキャンセルする
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        // 目的地を設定
        CLLocationCoordinate2D target = CLLocationCoordinate2DMake(locationData.coordinate.latitude, locationData.coordinate.longitude);
        CLLocationDistance radius = 300.0;  // 半径何メートルに入ったら通知するか
        NSString *identifier = @"CheckInPoint"; // 通知のID
        
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:target
                                                                     radius:radius
                                                                 identifier:identifier];
        
        // Notificationセット
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = @"近くにチェックインポイントがあります！";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertAction = @"OK";
        localNotification.regionTriggersOnce = YES;
        localNotification.region = region;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
#endif
*/
}

//真北・磁北を調べる
- (void) locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    CLLocationDirection justNorth = [newHeading trueHeading];
    CLLocationDirection magneticNorth = [newHeading magneticHeading];
    
    CLLocationDirection direction;
    if (self.isMagneticNorth) {
        direction = magneticNorth;
    } else {
        direction = justNorth;
    }
}

// 位置情報の取得に失敗した場合にコールされる。
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	if (error) {
//		NSString *message = nil;
		switch ([error code]) {
                // アプリでの位置情報サービスが許可されていない場合
			case kCLErrorDenied:
				// 位置情報取得停止
                self.isLocationEnabled = NO;
				[self.mLocationManager stopUpdatingLocation];
//                message = [NSString stringWithFormat:NSLocalizedString(@"notAllowToGetGPS", @"アラート：位置情報取得不許可_メッセージ")];
				break;
			default:
                self.isLocationEnabled = NO;
				break;
		}
		/*if (message) {
			// アラートを表示
            
			UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
                                                 cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
		}*/
	}
}

@end
