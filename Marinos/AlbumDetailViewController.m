//
//  AlbumDetailViewController.m
//  Marinos
//
//  Created by 菊地 拓也 on 2013/08/16.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "AlbumDetailViewController.h"
#import "Util.h"
#import "DatabaseUtility.h"
#import "TabButton.h"
#import "FileManager.h"
#import "MTPopupWindow.h"
#import "AdButton.h"

@interface AlbumDetailViewController ()

@end

@implementation AlbumDetailViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(void)viewWillDisappear:(BOOL)animated
{
}

-(void)viewWillAppear:(BOOL)animated
{
    //UA-44275326-1
    // TODO:GoogleAnalytics
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self setUpParts];
    
    
    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:[self.possessionDic[@"card_id"]intValue]];
    NSString *file_name = [selectedCardDic objectForKey:@"file_name"];
    NSLog(@"%@",selectedCardDic);
    if (file_name == nil) {
        NSLog(@"ファイル名が空っぽって噂");
    }
    
    NSData *cardData = [[FileManager sharedManager] loadDirectory:DIR_IS_NOT_APPLICABLE_BACK_UP fileName:file_name];
    
    
    UIImage *cardDataImage = [[UIImage alloc] initWithData:cardData];
    _cardImageView.image = cardDataImage;
    
    _nameLbl.text =selectedCardDic[@"card_text_2"];
    
    _leftLbl.text=selectedCardDic[@"card_text_sns"];
    
    opend=true;

}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)setUpParts
{
    UIImageView *background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"showCardBackGround.png"]];
    if (WIN_SIZE.height == 568)
    {
        background.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
        background.image = [UIImage imageNamed:@"showCardBackGround-568h.png"];//showCardBackGround-568hcardget-bg-568h
    }
    background.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    [self.view addSubview:background];

    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:self.cardID];
    NSString *file_name = [selectedCardDic objectForKey:@"file_name"];
    if (file_name == nil) {
        NSLog(@"ファイル名が空っぽって噂");
    }
    cardImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 480.0f)];
    //ステータスバーの分それぞれ２０足した2013/9/25
    cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2-33+20);//WIN_SIZE.height/2-33)
    if (WIN_SIZE.height == 568)
    {
        cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);//WIN_SIZE.height/2-33)
    }
    
    NSData *cardData = [[FileManager sharedManager] loadDirectory:DIR_IS_NOT_APPLICABLE_BACK_UP fileName:file_name];

   
    UIImage *cardDataImage = [[UIImage alloc] initWithData:cardData];
    cardImage.image = cardDataImage;
    cardImage.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [self.view addSubview:cardImage];
    
    //カード情報表示
    infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIN_SIZE.width, 90)];
    infoView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self.view addSubview:infoView];
    CGFloat labelFontSizeAdjust, labelSizeAdjust, labelPositionAdjust;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            labelFontSizeAdjust = 30.0f;
            labelSizeAdjust = 60.0f;
            labelPositionAdjust = 40.0f;
            break;
        case NSOrderedSame:
            labelFontSizeAdjust = 34.0f;
            labelSizeAdjust = 48.0f;
            labelPositionAdjust = 22.0f;
            break;
        case NSOrderedDescending:
            labelFontSizeAdjust = 34.0f;
            labelSizeAdjust = 48.0f;
            labelPositionAdjust = 22.0f;
            break;
        default:
            break;
    }

    //カードID
    NSString *selectedCardIDString = [NSString stringWithFormat:@"No.%03d",_cardID];
    UILabel *cardIDLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width, labelSizeAdjust)];
    cardIDLabel.center = CGPointMake(WIN_SIZE.width/2,labelPositionAdjust);//ここで位置調整
    cardIDLabel.text = selectedCardIDString;
    cardIDLabel.textColor = [UIColor whiteColor];
    cardIDLabel.backgroundColor = [UIColor clearColor];
    cardIDLabel.textAlignment = NSTextAlignmentCenter;
    cardIDLabel.font = [UIFont fontWithName:MY_FONT size:labelFontSizeAdjust];
    [infoView addSubview:cardIDLabel];
    
    NSString *selectedCardCharaName = selectedCardDic[@"card_text_1"];
    UILabel *cardCharaLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width-10, 30)];
    cardCharaLabel.center = CGPointMake(WIN_SIZE.width/2,57);
    cardCharaLabel.text = selectedCardCharaName;
    cardCharaLabel.textColor = [UIColor whiteColor];
    cardCharaLabel.backgroundColor = [UIColor clearColor];
    cardCharaLabel.textAlignment = NSTextAlignmentCenter;
    cardCharaLabel.font = [UIFont fontWithName:MY_FONT size:labelFontSizeAdjust / 2];
    cardIDLabel.adjustsFontSizeToFitWidth = YES;
    [infoView addSubview:cardCharaLabel];

    NSString *selectedCardCharaDetailName = selectedCardDic[@"card_text_2"];
    UILabel *cardCharaDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIN_SIZE.width-10, 30)];
    cardCharaDetailLabel.center = CGPointMake(WIN_SIZE.width/2,79);
    cardCharaDetailLabel.text = selectedCardCharaDetailName;
    cardCharaDetailLabel.textColor = [UIColor whiteColor];
    cardCharaDetailLabel.backgroundColor = [UIColor clearColor];
    cardCharaDetailLabel.textAlignment = NSTextAlignmentCenter;
    cardCharaDetailLabel.font = [UIFont fontWithName:MY_FONT size:labelFontSizeAdjust / 2];
    cardCharaDetailLabel.adjustsFontSizeToFitWidth = YES;
    [infoView addSubview:cardCharaDetailLabel];

    UIImageView *rareMark = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 45, 45)];
    rareMark.center = CGPointMake(280,22.5);
    [infoView addSubview:rareMark];
    int rare_level_id = [selectedCardDic[@"rare_level_id"] integerValue];
    if (rare_level_id == CARD_LEVEL_RARE)
    {
        rareMark.image = [UIImage imageNamed:@"rare-mark.png"];
    }
    if (rare_level_id == CARD_LEVEL_HOMEGAME)
    {
        rareMark.image = [UIImage imageNamed:@"home-mark.png"];
    }
    if (rare_level_id == CARD_LEVEL_VICTORY) {
        rareMark.image = [UIImage imageNamed:@"victory-mark.png"];
    }
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0.0f, 25.0f, 40, 40);
    [backButton setImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(viewBack:) forControlEvents:UIControlEventTouchDown];
    [infoView addSubview:backButton];
    
    CGSize adBtnSize = CGSizeMake(320.0f, 49.0f);
    if ([[selectedCardDic objectForKey:@"ad_title"] length] > 0) {
        AdButton *adUrlButton = [[AdButton alloc] init];
        [adUrlButton setFrame:CGRectMake(0.0f, WIN_SIZE.height - adBtnSize.height - 15.0f, adBtnSize.width, adBtnSize.height)];
        [adUrlButton setImage:[UIImage imageNamed:@"pr_01.png"] forState:UIControlStateNormal];
        [adUrlButton addTarget:self action:@selector(pressAdBannerButton:) forControlEvents:UIControlEventTouchUpInside];
        [adUrlButton setTag:1000];
        NSString *adText = [selectedCardDic objectForKey:@"ad_text"];
        [adUrlButton setAdText:adText];
        [self.view addSubview:adUrlButton];
    }
}

- (void) pressAdBannerButton:(AdButton *)sender {
    switch (sender.tag) {
        case 1000:
            //adurl
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sender.adText]];
            break;
            
        default:
            break;
    }
}

-(void)viewBack:(id)sender
{
    [[Util sharedManager] playSound:SOUND_ALBUM];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (infoView.alpha == 0.0f) {
        [self infoViewAnimation:YES];
    } else {
        [self infoViewAnimation:NO];
    }
}

-(void)infoViewAnimation:(BOOL)appear
{
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         if (appear == YES) {
                             infoView.alpha = 1.0f;
                             if (WIN_SIZE.height == 568) {
                                 cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);//WIN_SIZE.height/2-33)
                             }
                         } else {
                             infoView.alpha = 0.0f;
                             if (WIN_SIZE.height == 568) {
                                 
                             }
                         }
                     } completion:^(BOOL finished) {
                         //なにかあれば書く
                     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
