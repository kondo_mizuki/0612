//
//  UITableViewCellWithDocodemoDoor.m
//  TimeCapsule
//
//  Created by kyo on 2013/05/17.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "UITableViewCellWithDocodemoDoor.h"

@implementation UITableViewCellWithDocodemoDoor

- (void)updateTappedHistoryDelta:(UIButton*)sender {
    NSNotification *n = [NSNotification notificationWithName:@"updateTappedHistoryDelta" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(90, 15, 150, 45)];
        self.title.font = [UIFont systemFontOfSize:14];
        self.title.numberOfLines = 2;
        [self addSubview:self.title];
        
        self.date = [[UILabel alloc] initWithFrame:CGRectMake(90, 5, 220, 15)];
        self.date.font = [UIFont systemFontOfSize:13];
        self.date.textColor = [UIColor grayColor];
        [self addSubview:self.date];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
