#import <UIKit/UIKit.h>

@interface CustomWebViewController : UIView

- (void) setbackButton:(id)target action:(NSString*)selector;

- (void) loadUrl:(NSString *)targetUrl view:(UIView *)view;

@end
