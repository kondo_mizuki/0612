//
//  MapTileAnnotation.h
//  TimeCapsule
//
//  Created by kyo on 2013/04/09.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapTileAnnotation : MKPointAnnotation
<MKAnnotation>

@property (nonatomic, assign) int count;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *identification;

- (id) initWithCoordinate:(CLLocationCoordinate2D) coordinate;

@end
