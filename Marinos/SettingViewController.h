//
//  SettingViewController.h
//  Marinos
//
//  Created by eagle014 on 2014/03/03.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel.h"

@interface SettingViewController : UIViewController <OHAttributedLabelDelegate, UINavigationControllerDelegate>

@end
