//
//  Util.h
//  TimeCapsuleSampleWithARC
//
//  Created by kyo on 2013/01/29.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import "User.h"

@interface UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
@end

@interface Util : NSObject
<CLLocationManagerDelegate>
{
    
}

@property(nonatomic, strong) NSString *transUrl;
@property(nonatomic, assign) BOOL isLaunching;
@property (nonatomic, strong) NSString *myServer;
@property(nonatomic, assign) BOOL isLocationEnabled;
@property(nonatomic, assign) BOOL isMapVCAppeared;

@property(nonatomic,retain)  AVAudioPlayer *buttonSE;
@property(nonatomic,retain)  AVAudioPlayer *cardButtonSE;
@property(nonatomic,retain)  AVAudioPlayer *cardAppearSE;
@property(nonatomic,retain)  AVAudioPlayer *cardZoomUpSE_Normal;
@property(nonatomic,retain)  AVAudioPlayer *cardZoomUpSE_Rare;
@property(nonatomic,retain)  AVAudioPlayer *getTextSE_Rare;
@property(nonatomic,retain)  AVAudioPlayer *albumSE;
@property(nonatomic,retain)  AVAudioPlayer *kiiinSE;
@property(nonatomic,retain)  AVAudioPlayer *chuiuSE;
@property(nonatomic,retain)  AVAudioPlayer *pyoroSE;
@property(nonatomic,retain)  AVAudioPlayer *pironSE;
@property(nonatomic,retain)  AVAudioPlayer *vic_appear_SE;
@property(nonatomic,retain)  AVAudioPlayer *vic_Btn_SE;
@property(nonatomic,retain)  AVAudioPlayer *vic_finish_SE;

@property(nonatomic, assign) BOOL isMAP_TAB_INITED;
@property(nonatomic, assign) BOOL isCARD_TAB_INITED;
@property(nonatomic, assign) BOOL isALBUM_TAB_INITED;

@property (nonatomic, strong) NSMutableArray *getCardImageAry;

+ (Util *) sharedManager;

+ (void) setDefaultsValues;

- (void) sendUiidToServer:(NSString*)uiid;
- (NSString*) createUIID;
- (int) getPhotosSum;
- (NSMutableArray*) getAlbumCells;
- (BOOL) saveHistory:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longtitude:(double)longtitude title:(NSString*)title detail:(NSString*)detail;
- (int) getCheckinSum;
- (void) checkinCheck:(double)latitude with:(double)longtitude;
- (BOOL) showAllRows;
- (NSMutableArray*) getPhotos;
- (NSMutableArray*) getCheckIns;
- (UIImage*) getPhotoFromIdentification:(NSString*)identification;
- (NSMutableArray*) showTables;

-(void)prepareToPlaySounds;
-(void)playSound:(int)soundID;

-(int)getRandom_min:(int)min max:(int)max;
- (NSUInteger)integerVersion:(NSString *)version;
- (BOOL)checkCurrentVersion:(NSString *)version;
-(CGFloat)getStatusbarHeight;

@property(nonatomic, strong) NSString *heroku;

//add_ver2.0.0
//- (NSDictionary *)methodGetHTTP:(NSString *)php_program;
//- (BOOL) createHistorysDeltaTable;
//- (BOOL) createCardImageWithMD5Table;
//- (BOOL) insertCardImageWithMD5Table:(NSString *)file_name withMD5:(NSString *)MD5_str;
//- (BOOL) updateCardImageWithMD5Table:(NSString *)file_name withMD5:(NSString *)MD5_str;
//- (BOOL) saveHistory_delta:(NSString*)latitude with:(NSString*)longtitude delta:(int)delta;
//- (void) callGetAreaNew:(NSDictionary*)coordinates;
- (BOOL) createNewHistorysTable;
//- (BOOL) saveNewHistoryTable:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longitude:(double)longitude title:(NSString*)title detail:(NSString*)detail place_id:(int)place_ID showFlag:(int)show_flag;
//- (BOOL) saveNewHistoryTableForShowFlag:(BOOL)show_flag keyURLForDataBase: (NSString *)key_url;
//- (NSMutableArray *) getNewHistoryTable;
//- (NSMutableArray *) getNewHistoryTableMyPhotoData;
- (CGFloat) getAdjustedHeaderPartsHeight;
//- (CGFloat) getFooterAdjust;
//- (NSMutableArray *) getPhotosFromNewHistorysTable;
//- (NSMutableArray *) getColumnFromTable:(NSString *)tableName;
//- (void) alterColumnToTable:(NSString *)tableName columnName:(NSString *)columnName withType:(NSString *)type;

-(float)getSatusbarHeight;

// rev quest
+ (CGRect) getWinSize;
+ (CGPoint) getCenterPoint;
+ (float) getCenterWidth;
+ (BOOL) isIOS7;

//+ (void) setLoginStatus:(LoginState) state;
//+ (LoginState) getLoginStatus;
+ (void) setUserData:(NSString*) name prefecture:(NSString*)prefecture team:(NSString*)team;
+ (void) setUserDataFromUd;

+ (void) goSafari:(NSString*) urlString;

// Data
+ (NSArray*)prefectures;

@end
