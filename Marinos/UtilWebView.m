#import "UtilWebView.h"

@interface UtilWebView()

@end


@implementation UtilWebView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.backgroundColor = [UIColor whiteColor];
        self.scalesPageToFit = NO;
        self.opaque = NO;
    }
    return self;
}

#pragma mark - delegate method
//
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

//
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopActivityIndicatorAnimation];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


// Load Url
- (UtilWebView*)loadUrl:(NSString *)targetUrl view:(UIView *)view
{
    NSURL *url = [NSURL URLWithString:targetUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self loadRequest:request];
    [view addSubview:self];

    // アクティビティインジケータ表示
    [self startActivityIndicatorAnimation:view];

    return self;
}


// アクティビティインジケータ表示
- (void)startActivityIndicatorAnimation:(UIView *)view
{
    _indicator = [[UIActivityIndicatorView alloc] init];
    _indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    _indicator.frame = CGRectMake(0, 0, 20, 20);
    _indicator.center = view.center;
    _indicator.hidesWhenStopped = YES;
    [_indicator startAnimating];
    [view addSubview:_indicator];
}

// アクティビティインジケータ非表示
- (void)stopActivityIndicatorAnimation
{
    [_indicator stopAnimating];
}

@end
