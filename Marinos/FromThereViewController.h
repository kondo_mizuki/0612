//
//  FromThereViewController.h
//  TimeCapsule
//
//  Created by kyo on 2013/05/20.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FromThereViewController;

@protocol FromThereViewControllerDelegate <NSObject>

- (void)dissMissFromThereViewController:(FromThereViewController *)sender;

@end
@interface FromThereViewController : UIViewController
<UIWebViewDelegate>

@property(nonatomic, strong)NSString *latitude;
@property(nonatomic, strong)NSString *longitude;
@property(nonatomic, strong)NSString *recordDate;

@property (nonatomic, weak) id <FromThereViewControllerDelegate> delegate;
@end
