//
//  XMLUtility.m
//  Marinos
//
//  Created by eagle014 on 2014/02/10.
//  Copyright (c) 2014年 菊地 一貴. All rights reserved.
//

#import "XMLUtility.h"
#import "XMLWriter.h"
#import "XMLReader.h"

#import "DatabaseUtility.h"

@implementation XMLUtility

+ (NSString *)makeXML{
    NSString *xmlStr;
    //<?xml version="1.0" encoding="UTF-8"?>
    XMLWriter *writer = [[XMLWriter alloc] init];
    [writer writeStartDocumentWithEncodingAndVersion:@"UTF-8" version:@"1.0"];
    [writer writeStartElement:@"root"];  // rootタグの開始
    
    [writer writeStartElement:@"card"]; // cardタグの開始
    
    static int serial_number = 0;
    for (int i = 0; i < [[DatabaseUtility sharedManager] getCardCount]; i++)
    {
        serial_number++;
        [writer writeStartElement:@"possesion"];
        [writer writeAttribute:@"id" value:[NSString stringWithFormat:@"%d",serial_number]];
        [writer writeCharacters:[NSString stringWithFormat:@"%d",[[DatabaseUtility sharedManager] getGettedCardPossession:i+1]]];
        [writer writeEndElement];
    }
//    
//    for (int i=0; i < [[DatabaseUtility sharedManager] getCardCount:[ALBUM_2014_OFF intValue]]; i++)
//    {
//        serial_number++;
//        [writer writeStartElement:@"possesion"];
//        [writer writeAttribute:@"id" value:[NSString stringWithFormat:@"%d",serial_number]];
//        [writer writeCharacters:[NSString stringWithFormat:@"%d",[[DatabaseUtility sharedManager] getGettedCardPossession:i+1 year:[ALBUM_2014_OFF intValue]]]];
//        [writer writeEndElement];
//    }
//    
//    for (int i=0; i < [[DatabaseUtility sharedManager] getCardCount:[ALBUM_2014 intValue]]; i++)
//    {
//        serial_number++;
//        [writer writeStartElement:@"possesion"];
//        [writer writeAttribute:@"id" value:[NSString stringWithFormat:@"%d",serial_number]];
//        [writer writeCharacters:[NSString stringWithFormat:@"%d",[[DatabaseUtility sharedManager] getGettedCardPossession:i+1 year:[ALBUM_2014 intValue]]]];
//        [writer writeEndElement];
//    }
    
    [writer writeEndElement]; // cardタグのとじタグ
    
    NSMutableArray *placeAry = [[DatabaseUtility sharedManager] getAllLocation];
    NSLog(@"placeCnt:%d",[placeAry count]);
    
    for(NSMutableDictionary *placeDic in placeAry){
//        NSMutableDictionary *placeDic = [[DatabaseUtility sharedManager] getLocation:i+1];
        NSString *placeId = [NSString stringWithFormat:@"%@",placeDic[@"identifier"]];
        
        //place記述
        [writer writeStartElement:@"place"];
        //この間にplace記述
        [writer writeStartElement:@"check_in_count"];
        [writer writeAttribute:@"id" value:placeId];
        [writer writeCharacters:[NSString stringWithFormat:@"%d",[[placeDic objectForKey:@"check_in_count"] intValue]]];
        [writer writeEndElement];
        
        [writer writeStartElement:@"sp_check_in_count"];
        [writer writeAttribute:@"id" value:placeId];
        [writer writeCharacters:[NSString stringWithFormat:@"%d",[[placeDic objectForKey:@"sp_check_in_count"] intValue]]];
        [writer writeEndElement];
        
        [writer writeStartElement:@"last_check_in_date"];
        [writer writeAttribute:@"id" value:placeId];
        [writer writeCharacters:[placeDic objectForKey:@"last_check_in_date"]];
        [writer writeEndElement];
        
        [writer writeStartElement:@"last_sp_check_in_date"];
        [writer writeAttribute:@"id" value:placeId];
        [writer writeCharacters:[placeDic objectForKey:@"last_sp_check_in_date"]];
        [writer writeEndElement];
        
        [writer writeEndElement]; //placeタグの閉じタグ
    }
    
    
    [writer writeStartElement:@"common"];//この間にcommon記述
    //uid
    [writer writeStartElement:@"uid"];
    [writer writeAttribute:@"id" value:@"0"];
    [writer writeCharacters:[NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_ID] intValue]]];
    [writer writeEndElement];
    //point
    [writer writeStartElement:@"point"];
    [writer writeAttribute:@"id" value:@"1"];
    [writer writeCharacters:[NSString stringWithFormat:@"%d",[[NSUserDefaults standardUserDefaults] integerForKey:@"KEY_MYPOINT"]]];
    [writer writeEndElement];
    [writer writeEndElement]; //commmonタグの閉じタグ
    
    [writer writeEndElement]; // rootタグのとじタグ
    
    xmlStr = [writer toString];
    return xmlStr;
}

- (id)init{
    self = [super init];
    if (self) {
        //
    }
    return  self;
}
@end
