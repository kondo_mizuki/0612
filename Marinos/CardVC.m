//
//  CardVC.m
//  Marinos
//
//  Created by KEi on 13/08/06.
//  Copyright (c) 2013年 Keishi Kuwabara. All rights reserved.
//


#define TAB_SPACE 60

#import "CardVC.h"

#import "Util.h"
#import "DatabaseUtility.h"
#import "Toast+UIView.h"
#import "SoundUtility.h"

#import "Target.h"

typedef enum {
    normalType,victoryType
}buttonType;

@interface CardVC ()
@property (nonatomic, assign) int cardMyPoint;
@property (nonatomic, assign) int cardGetPoint;
@property (nonatomic, assign) int getCardID;

@property (nonatomic, strong) NSTimer *generatorTimer;
@property (nonatomic, strong) NSMutableArray *snowLayers;
@property (nonatomic, strong) SoundUtility *cardBtnSnd;
@property (nonatomic, strong) SoundUtility *cardShowSnd;
@property (nonatomic ,assign) BOOL hiddenTabBar;
@property (nonatomic, assign) BOOL tabBarIsVisible;


@end

@implementation CardVC
{
    Target *target;
}

@synthesize adGManagerViewController = ADGViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.cardBtnSnd = [[SoundUtility alloc] initWithFileName:SOUND_CARD_BUTTON ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
//        self.cardShowSnd = [[SoundUtility alloc] initWithFileName:SOUND_CARD_SHOW ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
    }
    return self;
}

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    if([[UIScreen mainScreen] bounds].size.height >= 568.0f) {
        [self showADGeneration];
    }
    [self setUpParts];

}

-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"CardVC";
    NSLog(@"cardVC");
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(ADGViewController){
        [ADGViewController resumeRefresh];
    }
}

//------------------------------------------------------------------------------------//
#pragma mark - CustomTabBarControllerDelegate
//------------------------------------------------------------------------------------//
- (void) didSelect:(CustomTabBarController *)tabBarController {
}



#pragma mark - スコア
- (void) culScore:(int)score
{
    int currentScore = [scoreLabel.text intValue];
    currentScore += score;
    [scoreLabel setText:[NSString stringWithFormat:@"%d",currentScore]];
}

#pragma mark - タイマーの開始と停止
- (void) stopTimer
{
    if (spawnTimer)
    {
        [spawnTimer invalidate];
        spawnTimer = nil;
    }
}
- (void) startTimer
{
    spawnTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                  target:self
                                                selector:@selector(spawn)
                                                userInfo:nil
                                                 repeats:YES];
}


- (void) doStop
{
    [self stopTimer];
    [target doStop];
    
    if ([scoreLabel.text intValue]>29) {
    [self cardChoice];
    }else{
        [self gameFailed];
    }
}

-(void)gameFailed{
    [scoreLabel setText:@"ゲーム失敗！"];
    [self againButtonAndBG];
}


- (void) spawn
{
    //タイムリミット管理
    timeCount-=0.1f;
    [timeLabel setText:[NSString stringWithFormat:@"%.1f",timeCount]];
    
    if (timeCount <= 0.0f)
    {
        [self doStop];
        return;
    }
}


#pragma mark - タッチイベント
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //タッチした位置の取得
    UITouch *touch = [touches anyObject];
    CGPoint touchPos = [touch locationInView:[self view]];
    
    NSLog(@"touchPos:%@",NSStringFromCGPoint(touchPos));
    NSLog(@"[target frame]:%@",NSStringFromCGRect([target.layer.presentationLayer frame]));
    
    //タッチ判定
    if (CGRectContainsPoint([target.layer.presentationLayer frame], touchPos)){
        [self culScore:1];
    }else{
        [self culScore:-1];
    }
    
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

#pragma mark - 初期化処理
- (void) setUpParts
{
    
    //スコアラベル
    scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(320 - 150, 50, 150, 20)];
    [scoreLabel setText:@"0"];
    [scoreLabel setTextColor:[UIColor blackColor]];
    [scoreLabel setTextAlignment:NSTextAlignmentRight];
    [scoreLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:scoreLabel];
    
    //タイムラベル
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 100, 20)];
    [timeLabel setText:[NSString stringWithFormat:@"%.2f",timeCount]];
    [timeLabel setTextColor:[UIColor blackColor]];
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:timeLabel];
    
    target = [Target targetWithType];
    target.hidden = YES;
    [self.view addSubview:target];
}

-(void) showADGeneration {
    NSDictionary *adgparam = @{
                               @"locationid" : @"10723", //管理画面から払い出された広告枠ID
                               @"adtype" : @(kADG_AdType_Free), //管理画面にて入力した枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free
                               @"originx" : @(0), //広告枠設置起点のx座標 ※下記参照
                               @"originy" : @(0), //広告枠設置起点のy座標 ※下記参照
                               @"w" : @(320), //広告枠横幅 ※下記参照
                               @"h" : @(50)  //広告枠高さ ※下記参照
                               };
    ADGManagerViewController *adgvc = [[ADGManagerViewController alloc]initWithAdParams :adgparam :self.view];
    self.adGManagerViewController = adgvc;
    //    [adgvc release];
    ADGViewController.delegate = self;
    [ADGViewController loadRequest];
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)startBtnPush:(id)sender {
    timeCount = 20.0;
    [scoreLabel setText:@"0"];
    target.hidden=NO;
    [self startTimer];
    [target startTimer];
    self.startBtn.hidden=YES;
    
}

//成功時カードアニメーションなど処理

#pragma mark - 獲得するカードを決定する
-(void)cardChoice
{
    
   NSString *file_name =@"testCard.jpg";
   // NSString *file_name =[NSString stringWithFormat:@"testCard.jpg"];
        
        UIImageView *cardImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:file_name]];
        cardImage.frame = CGRectMake(0, 0, 320, 480);
        cardImage.tag=CARD_TAG;
        cardImage.backgroundColor = [UIColor whiteColor];
        cardImage.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
        cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2 - 30);
        [self.view addSubview:cardImage];
        
        [self cardAppearAnime:cardImage];
    
}



#pragma mark - アニメーション
-(void)cardAppearAnime:(UIImageView*)card
{
    //サウンド再生
    [[Util sharedManager] playSound:SOUND_CARD_APPEAR];
    
    [UIView animateWithDuration:1.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         card.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                         card.alpha = 0.5f;
                         card.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);//-30
                     }
                     completion:^(BOOL finished) {
                         [self cardGetAnime:card];
                     }];
}

#pragma mark - カード出現アニメーション
-(void)cardGetAnime:(UIImageView*)card
{
    //パーティクルを表示
   // [self getParticle];
    
    //pointBase.alpha = 1.0f;
    card.transform = CGAffineTransformMakeScale(0.7, 0.7);
    card.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2-58);
    card.alpha = 0.0f;
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         card.alpha = 1.0f;
                         
                     }
                     completion:^(BOOL finished) {
                         
                        [self performSelector:@selector(againButtonAndBG) withObject:nil afterDelay:1.7];//1.7
                         //TODO:もう一度ボタンも表示する

                     }];
}

-(void)againButtonAndBG{
#pragma mark もう一度ボタン
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(0, 0, 200, 40);
    moreButton.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height - 180);
    [moreButton setBackgroundImage:[UIImage imageNamed:@"bn_red.png"] forState:UIControlStateNormal];
    [moreButton setBackgroundImage:[UIImage imageNamed:@"bn_red.png"] forState:UIControlStateHighlighted];
    [moreButton setTitle:@"もう一度カードを引く" forState:UIControlStateNormal];
    [moreButton setTitle:@"もう一度カードを引く" forState:UIControlStateHighlighted];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    moreButton.titleLabel.font = [ UIFont fontWithName:@"HiraKakuProN-W6" size:16];
    moreButton.tag = MORE_BTN;
    [moreButton addTarget:self action:@selector(againButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moreButton];

    
   
}

-(void)againButtonPushed:(id)sender{
    [timeLabel setText:@"20.0"];
    [self culScore:0];
    self.startBtn.hidden=NO;
    
    UIView *card = [self.view viewWithTag:CARD_TAG];
    [card removeFromSuperview];
    
    UIView *moreButton = [self.view viewWithTag:MORE_BTN];
    [moreButton removeFromSuperview];
}

#pragma mark - パーティクルセットアップ
//-(void)getParticle
//{
//    self.emitterLayer = [CAEmitterLayer layer];
//    self.emitterLayer.beginTime = 1.0;
//    
//    self.emitterLayer.renderMode = kCAEmitterLayerAdditive;
//    [self.view.layer addSublayer:self.emitterLayer];
//    //-----------
//    // パーティクル画像
//    UIImage *particleImage = [UIImage imageNamed:@"particle.png"];
//    
//    /*
//     birthRate:１秒間に生成するパーティクルの数。
//     lifetime:パーティクルが発生してから消えるまでの時間。単位は秒。
//     color:パーティクルの色。
//     velocity:パーティクルの秒速。
//     emissionRange:パーティクルを発生する角度の範囲。単位はラジアン。
//     
//     */
//    // 花火自体の発生源
//    CAEmitterCell *baseCell = [CAEmitterCell emitterCell];
//    baseCell.emissionLongitude = -M_PI / 2;
//    baseCell.emissionLatitude = 0;
//    baseCell.emissionRange = M_PI / 5;
//    baseCell.lifetime = 1.0;
//    baseCell.scale = 0.6;
//    baseCell.birthRate = 1*(cardLevel*1.5);//ノーマル１　レア２だったから使ってみた。
//    baseCell.velocity = 400;
//    baseCell.velocityRange = 50;
//    baseCell.yAcceleration = 300;
//    baseCell.color = CGColorCreateCopy([UIColor colorWithRed:0.5
//                                                       green:0.5
//                                                        blue:0.5
//                                                       alpha:0.5].CGColor);
//    baseCell.redRange   = 0.5;
//    baseCell.greenRange = 0.5;
//    baseCell.blueRange  = 0.5;
//    baseCell.alphaRange = 0.5;
//    baseCell.name = @"fireworks";
//    
//    // 破裂後に飛散するパーティクルの発生源
//    CAEmitterCell *sparkCell = [CAEmitterCell emitterCell];
//    sparkCell.contents = (__bridge id)particleImage.CGImage;
//    sparkCell.emissionRange = 2 * M_PI;
//    sparkCell.birthRate = 600;
//    sparkCell.scale = 0.4;
//    sparkCell.velocity = 300;
//    sparkCell.lifetime = 0.4;
//    sparkCell.yAcceleration = 80;
//    sparkCell.beginTime = 0.01;//risingCell.lifetime
//    sparkCell.duration = 0.1;
//    sparkCell.alphaSpeed = -0.2;
//    sparkCell.scaleSpeed = -0.6;
//    
//    // baseCellからrisingCellとsparkCellを発生させる
//    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
//    {  //iOS以上の場合
//        
//        // 上昇中のパーティクルの発生源
//        CAEmitterCell *risingCell = [CAEmitterCell emitterCell];
//        risingCell.contents = (__bridge id)particleImage.CGImage;
//        risingCell.emissionLongitude = (4 * M_PI) / 2;
//        risingCell.emissionRange = M_PI / 7;
//        risingCell.scale = 0.4;
//        risingCell.velocity = 100;
//        risingCell.birthRate = 50;
//        risingCell.lifetime = 1.5;
//        risingCell.yAcceleration = 350;
//        risingCell.alphaSpeed = -0.7;
//        risingCell.scaleSpeed = -0.1;
//        risingCell.scaleRange = 0.1;
//        risingCell.beginTime = 0.01;
//        risingCell.duration = 0.7;
//        
//        baseCell.emitterCells = [NSArray arrayWithObjects:risingCell, nil];
//        risingCell.emitterCells = [NSArray arrayWithObjects:sparkCell, nil];
//    }
//    // baseCellはemitterLayerから発生させる
//    else
//    {
//        baseCell.emitterCells = [NSArray arrayWithObjects:sparkCell, nil];
//    }
//    self.emitterLayer.emitterCells = [NSArray arrayWithObjects:baseCell, nil];
//    
//    //-----------
//    CGSize size = self.view.bounds.size;
//    self.emitterLayer.emitterPosition = CGPointMake(size.width / 2, size.height * 4 / 5);
//}

#pragma mark - マイポイント読込・記録・使用
//-(void)myPointLoad
//{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    myPoint = (int)[ud integerForKey:@"KEY_MYPOINT"];
//    _myPointLabel.text = [NSString stringWithFormat:@"残り%dポイント",myPoint];
//}
//
//
//
//-(void)myPointSave
//{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    [ud setInteger:myPoint forKey:@"KEY_MYPOINT"];
//    _myPointLabel.text = [NSString stringWithFormat:@"残り%dポイント",myPoint];
//}
//
//-(void)myPointUseAndSave:(int)usePoint
//{
//    myPoint = myPoint - usePoint;
//    [self myPointSave];
//}

@end
