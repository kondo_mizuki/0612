#import "RankingVC.h"
#import "AppDelegate.h"
#import "Util.h"
#import "ViewManager.h"

@interface RankingVC ()
{
}
// index
@property (nonatomic, strong) UIImageView *bg;
@property (nonatomic, strong) UIButton *teamButton;
@property (nonatomic, strong) UIButton *soloButton;
@property (nonatomic, strong) UILabel *forRankingLabel;

// detail
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIImageView *rankingIcon;
@property (nonatomic, strong) UILabel *categoryLabel;

// team detail
@property (nonatomic, strong) UIImageView *categoryParameter;
@property (nonatomic, strong) UILabel *redPercentLabel;
@property (nonatomic, strong) UILabel *greenPercentLabel;
@property (nonatomic, strong) UILabel *bluePercentLabel;

// solo detail
@property (nonatomic, strong) UILabel *soloHeader;
@property (nonatomic, strong) UIImageView *soloTopRankerLogo;
@property (nonatomic, strong) UIScrollView *sv;
@property (nonatomic, strong) UILabel *topRanking;

// data
@property (nonatomic, retain) NSArray *categories;
@property (readwrite) int categoryIndex;
@property (readwrite) int parameterType;
@property (readwrite) int isTeam;
@property (nonatomic, retain) NSArray *topRankings;

@end


@implementation RankingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated
{
    [self initData];
    [self setIndexStage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) setIndexStage
{
    _bg = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [Util getWinSize].size.width, [Util getWinSize].size.height)];
    _bg.backgroundColor = [UIColor clearColor];
    _bg.center = [Util getCenterPoint];
    [_bg setImage:[UIImage imageNamed:@"ranking_bg.png"]];
    [self.view addSubview:_bg];

    _teamButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageTeam = [UIImage imageNamed:@"ranking_btn_team.png"];
    _teamButton.frame = CGRectMake(0, 0, imageTeam.size.width, imageTeam.size.height);
    _teamButton.center = CGPointMake([Util getCenterWidth], 190);
    _teamButton.backgroundColor = [UIColor clearColor];
    [_teamButton setBackgroundImage:imageTeam forState:UIControlStateNormal];
    [_teamButton setBackgroundImage:imageTeam forState:UIControlStateHighlighted];
    [_teamButton addTarget:self action:@selector(teamCallBack:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:_teamButton];

    _soloButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageSolo = [UIImage imageNamed:@"ranking_btn_solo.png"];
    _soloButton.frame = CGRectMake(0, 0, imageSolo.size.width, imageSolo.size.height);
    _soloButton.center = CGPointMake([Util getCenterWidth], 340);
    _soloButton.backgroundColor = [UIColor clearColor];
    [_soloButton setBackgroundImage:imageSolo forState:UIControlStateNormal];
    [_soloButton setBackgroundImage:imageSolo forState:UIControlStateHighlighted];
    [_soloButton addTarget:self action:@selector(soloCallBack:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:_soloButton];


    NSString *forRanking = @"※ランキングについての説明はコチラ";
    NSMutableAttributedString *attrStr;
    attrStr = [[NSMutableAttributedString alloc] initWithString:forRanking];
    NSDictionary *attributes = @{NSStrokeColorAttributeName: [UIColor magentaColor],
                                 NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                 NSKernAttributeName: @1.5};
    [attrStr addAttributes:attributes
                     range:NSMakeRange(0, [attrStr length])];
    _forRankingLabel = [[UILabel alloc] init];
    _forRankingLabel.frame = CGRectMake(10, 10, 300, 50);
    _forRankingLabel.center = CGPointMake([Util getCenterWidth], 450);
    _forRankingLabel.backgroundColor = [UIColor clearColor];
    _forRankingLabel.textColor = [UIColor whiteColor];
    _forRankingLabel.font = [UIFont fontWithName:MY_FONT size:12];
    _forRankingLabel.textAlignment = NSTextAlignmentCenter;
    _forRankingLabel.userInteractionEnabled = YES;
    [_forRankingLabel setAttributedText:attrStr];
    [self.view addSubview:_forRankingLabel];

    [[ViewManager sharedManager] setHeader:self.view];
}

- (void) removeIndexStage {
    [_bg removeFromSuperview];
    [_teamButton removeFromSuperview];
    [_soloButton removeFromSuperview];
    [_forRankingLabel removeFromSuperview];
}

- (void) initData {
    _isTeam = FALSE;
    _categoryIndex = 0;
    _categories = @[@"チーム総合", @"チーム月間"];
    _topRankings = @[@"1位 ヤハマ好き\n 獲得枚数:85枚 所属:teamR\n2位 SR同好会代表\n 獲得枚数:84枚 所属:teamB\n3位 SR同好会代表\n 獲得枚数:83枚 所属:teamB\n4位 SR同好会代表\n 獲得枚数:82枚 所属:teamB\n5位 SR同好会代表\n 獲得枚数:81枚 所属:teamB\n6位 SR同好会代表\n 獲得枚数:80枚 所属:teamG\n7位 SR同好会代表\n 獲得枚数:79枚 所属:teamR\n8位 SR同好会代表\n 獲得枚数:78枚 所属:teamB\n9位 SR同好会代表\n 7獲得枚数:6枚 所属:teamR\n10位 SR同好会代表\n 獲得枚数:70枚 所属:teamG", @"1位 ヤハマ好き\n 獲得枚数:85枚 所属:teamR\n2位 SR同好会代表\n 獲得枚数:84枚 所属:teamB\n3位 SR同好会代表\n 獲得枚数:83枚 所属:teamB\n4位 SR同好会代表\n 獲得枚数:82枚 所属:teamB\n5位 SR同好会代表\n 獲得枚数:81枚 所属:teamB\n6位 SR同好会代表\n 獲得枚数:80枚 所属:teamG\n7位 SR同好会代表\n 獲得枚数:79枚 所属:teamR\n8位 SR同好会代表\n 獲得枚数:78枚 所属:teamB\n9位 SR同好会代表\n", @"1位 ヤハマ好き\n 獲得枚数:100枚 所属:teamR\n2位 SR同好会代表\n 獲得枚数:84枚 所属:teamB\n3位 SR同好会代表\n 獲得枚数:83枚 所属:teamB\n4位 SR同好会代表\n 獲得枚数:82枚 所属:teamB\n5位 SR同好会代表\n 獲得枚数:81枚 所属:teamB\n6位 SR同好会代表\n 獲得枚数:80枚 所属:teamG\n7位 SR同好会代表\n 獲得枚数:79枚 所属:teamR\n8位 SR同好会代表\n 獲得枚数:78枚 所属:teamB\n9位 SR同好会代表\n 7獲得枚数:6枚 所属:teamR\n10位 SR同好会代表\n 獲得枚数:70枚 所属:teamG"];
    // TODO: from server data

    _parameterType = 6; // for test
}


- (void) setAnalizyngAnim {
    // 1. init
    [self removeIndexStage];
    [[ViewManager sharedManager] removeHeader];

    // 2. set animation
    [[ViewManager sharedManager] setCommonBg:self.view];
    [[ViewManager sharedManager] setHeader:self.view];
    [UIView animateWithDuration:1.0f
                     animations:^{
                         UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
                         NSMutableArray *imageList = [NSMutableArray array];

                         for (NSInteger i = 1; i < 4; i++) {
                             NSString *imagePath = [NSString stringWithFormat:@"analyzing%d.png", i];
                             UIImage *img = [UIImage imageNamed:imagePath];
                             [imageList addObject:img];
                         }

                         [self.view addSubview:imageView];
                         
                         imageView.animationImages = imageList;
                         imageView.animationDuration = 1.0f;
                         imageView.animationRepeatCount = 1;
                         [imageView startAnimating];
                     }
                     completion:^(BOOL finished){
                     }];
    if (_isTeam) {
        [self performSelector:@selector(setTeamDetailStage) withObject:nil afterDelay:1.0];
    } else {
        [self performSelector:@selector(setSoloDetailStage) withObject:nil afterDelay:1.0];
    }
}

- (void) setTeamDetailStage {
    [[ViewManager sharedManager] removeHeader];
    [[ViewManager sharedManager] setHeaderWithRetutn:self.view target:self action:@"backCallBack:"];

    [self setCategoryTitle];
    [self setTeamCategoryParameter];
}

- (void) setSoloDetailStage {
    [[ViewManager sharedManager] removeHeader];
    [[ViewManager sharedManager] setHeaderWithRetutn:self.view target:self action:@"backCallBack:"];

    [self setCategoryTitle];
    [self setSoloCategoryParameter];
}

- (void) removeTeamDetailStage {
    [_leftButton removeFromSuperview];
    [_rightButton removeFromSuperview];
    [_rankingIcon removeFromSuperview];
    [_categoryParameter removeFromSuperview];
}

- (void) removeSoloDetailStage {
    [_leftButton removeFromSuperview];
    [_rightButton removeFromSuperview];
    [_rankingIcon removeFromSuperview];
    [_soloHeader removeFromSuperview];
    [_soloTopRankerLogo removeFromSuperview];
    [_topRanking removeFromSuperview];
}

- (void) setCategoryTitle {
    _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageLeft = [UIImage imageNamed:@"ranking_arrow_left.png"];
    _leftButton.frame = CGRectMake(0, 0, imageLeft.size.width, imageLeft.size.height);
    _leftButton.center = CGPointMake(60, 70);
    _leftButton.backgroundColor = [UIColor clearColor];
    [_leftButton setBackgroundImage:imageLeft forState:UIControlStateNormal];
    [_leftButton setBackgroundImage:imageLeft forState:UIControlStateHighlighted];
    [_leftButton addTarget:self action:@selector(categoryLeftCallBack:) forControlEvents:UIControlEventTouchDown];
    _leftButton.hidden = YES;
    [self.view addSubview:_leftButton];

    _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageRight = [UIImage imageNamed:@"ranking_arrow_right.png"];
    _rightButton.frame = CGRectMake(0, 0, imageRight.size.width, imageRight.size.height);
    _rightButton.center = CGPointMake(260, 70);
    _rightButton.backgroundColor = [UIColor clearColor];
    [_rightButton setBackgroundImage:imageRight forState:UIControlStateNormal];
    [_rightButton setBackgroundImage:imageRight forState:UIControlStateHighlighted];
    [_rightButton addTarget:self action:@selector(categoryRightCallBack:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:_rightButton];

    UIImage *imageRankingIcon = [UIImage imageNamed:@"ranking_icon_tab.png"];
    _rankingIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, imageRankingIcon.size.width, imageRankingIcon.size.height)];
    _rankingIcon.backgroundColor = [UIColor clearColor];
    _rankingIcon.center = CGPointMake(90, 70);
    [_rankingIcon setImage:imageRankingIcon];
    [self.view addSubview:_rankingIcon];

    _categoryLabel = [[UILabel alloc] init];
    _categoryLabel.frame = CGRectMake(10, 10, 130, 50);
    _categoryLabel.center = CGPointMake(160, 70);
    _categoryLabel.backgroundColor = [UIColor clearColor];
    _categoryLabel.textColor = [UIColor colorWithRed:0.0 green:0.616 blue:0.745 alpha:1.0];
    _categoryLabel.font = [UIFont fontWithName:MY_FONT size:14];
    _categoryLabel.textAlignment = NSTextAlignmentCenter;
    _categoryLabel.userInteractionEnabled = YES;
    _categoryLabel.text = _categories[_categoryIndex];
    [self.view addSubview:_categoryLabel];
}

- (void) setTeamCategoryParameter {
    if (_categoryParameter) {
        [_categoryParameter removeFromSuperview];
    }

    UIImage *imageRankingParameter;
    switch (_parameterType) {
        case ZERO:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_ZERO];
            break;
        case R50:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_R50];
            break;
        case R100:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_R100];
            break;
        case G50:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_G50];
            break;
        case G100:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_G100];
            break;
        case B50:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_B50];
            break;
        case B100:
            imageRankingParameter = [UIImage imageNamed:RANKING_PARAMETER_B100];
            break;

        default:
            break;
    }

    _categoryParameter = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, imageRankingParameter.size.width, imageRankingParameter.size.height)];
    _categoryParameter.backgroundColor = [UIColor clearColor];
    _categoryParameter.center = [Util getCenterPoint];
    [_categoryParameter setImage:imageRankingParameter];
    [self.view addSubview:_categoryParameter];

    [self initCategoryPercent];
    [self setCategoryPercent];
}

- (void) setSoloCategoryParameter {
    if (_sv) {
        [_sv removeFromSuperview];
    }

    _sv = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    _sv.backgroundColor = [UIColor clearColor];
    _sv.center = CGPointMake([Util getCenterWidth], 350);

    UIView *uv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [Util getWinSize].size.width, 1000)];
    [_sv addSubview:uv];
    _sv.contentSize = uv.bounds.size;
    [self.view addSubview:_sv];

    _soloHeader = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, [Util getWinSize].size.width, 70)];
    _soloHeader.numberOfLines = 2;
    _soloHeader.backgroundColor = [UIColor clearColor];
    _soloHeader.textColor = [UIColor whiteColor];
    _soloHeader.textAlignment = NSTextAlignmentLeft;
    _soloHeader.font = [UIFont fontWithName:MY_FONT size:FONT_SOLO_HEADER];
    NSString *str = [NSString stringWithFormat:@"あなたの順位 : %d位\n獲得枚数 : %d枚",258, 41]; // from server
    _soloHeader.text = str;
    [uv addSubview:_soloHeader];


    UIImage *imageSoloTopRankerLogo = [UIImage imageNamed:@"ranking_topranker.png"];
    _soloTopRankerLogo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, imageSoloTopRankerLogo.size.width, imageSoloTopRankerLogo.size.height)];
    _soloTopRankerLogo.backgroundColor = [UIColor clearColor];
    _soloTopRankerLogo.center = CGPointMake([Util getCenterPoint].x, 90);
    [_soloTopRankerLogo setImage:imageSoloTopRankerLogo];
    [uv addSubview:_soloTopRankerLogo];


    _topRanking = [[UILabel alloc] initWithFrame:CGRectMake(40, 110, [Util getWinSize].size.width, 70)];
    _topRanking.numberOfLines = 0;
    _topRanking.backgroundColor = [UIColor clearColor];
    _topRanking.textColor = [UIColor whiteColor];
    _topRanking.textAlignment = NSTextAlignmentLeft;
    _topRanking.font = [UIFont fontWithName:MY_FONT size:FONT_SOLO_HEADER];
    NSString *rankings = _topRankings[_categoryIndex]; // from server
    CGFloat lineHeight = 25.0 ;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init] ;
    [paragraphStyle setMinimumLineHeight:lineHeight];
    [paragraphStyle setMaximumLineHeight:lineHeight];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:paragraphStyle,NSParagraphStyleAttributeName,nil] ;
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:rankings attributes:attributes] ;
    [_topRanking setAttributedText:attributedText];
    [_topRanking sizeToFit];
    [uv addSubview:_topRanking];
}

- (void) initCategoryPercent {
    _redPercentLabel = [[UILabel alloc] init];
    _redPercentLabel.frame = CGRectMake(0, 0, 100, 50);
    _redPercentLabel.center = CGPointMake(0, 0);
    _redPercentLabel.backgroundColor = [UIColor clearColor];
    _redPercentLabel.textColor = [UIColor whiteColor];
    _redPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
    _redPercentLabel.textAlignment = NSTextAlignmentCenter;
    _redPercentLabel.userInteractionEnabled = NO;
    _redPercentLabel.hidden = NO;
    [_categoryParameter addSubview:_redPercentLabel];

    _greenPercentLabel = [[UILabel alloc] init];
    _greenPercentLabel.frame = CGRectMake(0, 0, 100, 50);
    _greenPercentLabel.center = CGPointMake(0, 0);
    _greenPercentLabel.backgroundColor = [UIColor clearColor];
    _greenPercentLabel.textColor = [UIColor whiteColor];
    _greenPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
    _greenPercentLabel.textAlignment = NSTextAlignmentCenter;
    _greenPercentLabel.userInteractionEnabled = NO;
    _greenPercentLabel.hidden = NO;
    [_categoryParameter addSubview:_greenPercentLabel];

    _bluePercentLabel = [[UILabel alloc] init];
    _bluePercentLabel.frame = CGRectMake(0, 0, 100, 50);
    _bluePercentLabel.center = CGPointMake(0, 0);
    _bluePercentLabel.backgroundColor = [UIColor clearColor];
    _bluePercentLabel.textColor = [UIColor whiteColor];
    _bluePercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
    _bluePercentLabel.textAlignment = NSTextAlignmentCenter;
    _bluePercentLabel.userInteractionEnabled = NO;
    _bluePercentLabel.hidden = NO;
    [_categoryParameter addSubview:_bluePercentLabel];

}

- (void) setCategoryPercent {
    switch (_parameterType) {
        case ZERO:
            if (_redPercentLabel) {
                _redPercentLabel.hidden = YES;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.hidden = YES;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.hidden = YES;
            }
            break;

        case R50:
            if (_redPercentLabel) {
                _redPercentLabel.text = @"50%";// TODO: from server
                _redPercentLabel.center = CGPointMake(130, 150);
                _redPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIDDLE_PERCENT];
                _redPercentLabel.hidden = NO;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.text = @"20%";// TODO: from server
                _greenPercentLabel.center = CGPointMake(172, 275);
                _greenPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
                _greenPercentLabel.hidden = NO;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.text = @"20%";// TODO: from server
                _bluePercentLabel.center = CGPointMake(238, 240);
                _bluePercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
                _bluePercentLabel.hidden = NO;
            }
            break;

        case R100:
            if (_redPercentLabel) {
                _redPercentLabel.text = @"100%";
                _redPercentLabel.center = CGPointMake(140, 170);
                _redPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MAX_PERCENT];
                _redPercentLabel.hidden = NO;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.hidden = YES;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.hidden = YES;
            }
            break;

        case G50:
            if (_redPercentLabel) {
                _redPercentLabel.text = @"50%";// TODO: from server
                _redPercentLabel.center = CGPointMake(172, 55);
                _redPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
                _redPercentLabel.hidden = NO;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.text = @"20%";// TODO: from server
                _greenPercentLabel.center = CGPointMake(135, 230);
                _greenPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIDDLE_PERCENT];
                _greenPercentLabel.hidden = NO;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.text = @"20%";// TODO: from server
                _bluePercentLabel.center = CGPointMake(238, 90);
                _bluePercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
                _bluePercentLabel.hidden = NO;
            }
            break;

        case G100:
            if (_redPercentLabel) {
                _redPercentLabel.hidden = YES;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.text = @"100%";
                _greenPercentLabel.center = CGPointMake(140, 170);
                _greenPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MAX_PERCENT];
                _greenPercentLabel.hidden = NO;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.hidden = YES;
            }
            break;

        case B50:
            if (_redPercentLabel) {
                _redPercentLabel.text = @"20%";// TODO: from server
                _redPercentLabel.center = CGPointMake(43, 80);
                _redPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
                _redPercentLabel.hidden = NO;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.text = @"30%";// TODO: from server
                _greenPercentLabel.center = CGPointMake(43, 150);
                _greenPercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIN_PERCENT];
                _greenPercentLabel.hidden = NO;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.text = @"50%";// TODO: from server
                _bluePercentLabel.center = CGPointMake(188, 140);
                _bluePercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MIDDLE_PERCENT];
                _bluePercentLabel.hidden = NO;
            }
            break;

        case B100:
            if (_redPercentLabel) {
                _redPercentLabel.hidden = YES;
            }
            if (_greenPercentLabel) {
                _greenPercentLabel.hidden = YES;
            }
            if (_bluePercentLabel) {
                _bluePercentLabel.text = @"100%";
                _bluePercentLabel.center = CGPointMake(140, 170);
                _bluePercentLabel.font = [UIFont fontWithName:MY_FONT size:FONT_MAX_PERCENT];
                _bluePercentLabel.hidden = NO;
            }
            break;
        default:
            break;
    }
}

- (void) changeParameter {
    _parameterType = _categoryIndex; // for test
    if (_isTeam) {
        [self setTeamCategoryParameter];
    } else {
        [self setSoloCategoryParameter];
    }
}

// callback
- (void) teamCallBack:(UIButton*)button {
    _isTeam = TRUE;
    [self setAnalizyngAnim];
}

- (void) soloCallBack:(UIButton*)button {
    _isTeam = FALSE;
    [self setAnalizyngAnim];
}

-(void) categoryLeftCallBack:(UIButton*)button {
    if (_categoryIndex - 1 >= 0) {
        _categoryIndex--;
        _categoryLabel.text = _categories[_categoryIndex];
        [self changeParameter];
    }

    if (_categoryIndex == 0) {
        _leftButton.hidden = YES;
        _rightButton.hidden = NO;
    } else {
        _leftButton.hidden = NO;
        _rightButton.hidden = NO;
    }
}

-(void) categoryRightCallBack:(UIButton*)button {
    if (_categoryIndex < _categories.count) {
        _categoryIndex++;
        _categoryLabel.text = _categories[_categoryIndex];
        [self changeParameter];
    }

    if (_categoryIndex + 1 == _categories.count) {
        _leftButton.hidden = NO;
        _rightButton.hidden = YES;
    } else {
        _leftButton.hidden = NO;
        _rightButton.hidden = NO;
    }
}

-(void)backCallBack:(UIButton*)button {
    [[ViewManager sharedManager] removeHeader];
    [[ViewManager sharedManager] setHeader:self.view];
    if (_isTeam) {
        [self removeTeamDetailStage];
    } else {
        [self removeSoloDetailStage];
    }
    [self setIndexStage];
}

// for keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if (touch.view == _forRankingLabel) {
        [[ViewManager sharedManager] setWebViewWithBackButton:self.view urlString:URL_REGIST_EXPLANATION];
    }
}

@end
