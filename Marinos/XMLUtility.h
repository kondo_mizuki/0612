//
//  XMLUtility.h
//  Marinos
//
//  Created by eagle014 on 2014/02/10.
//  Copyright (c) 2014年 菊地 一貴. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLUtility : NSObject

+ (NSString *)makeXML;

@end
