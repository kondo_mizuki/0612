//
//  FrameLabel.h
//  TimeCapsule
//
//  Created by kyo on 2013/04/09.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrameLabel : UILabel

@property (nonatomic, strong) UIColor *OutlineColor;
@property (nonatomic, assign) CGFloat OutlineWidth;

@end
