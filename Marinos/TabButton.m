//
//  TabButton.m
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/14.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "TabButton.h"
#import "TopVC.h"
#import "MapVC.h"
#import "CardVC.h"
#import "AlbumVC.h"
#import "SoundUtility.h"
@interface TabButton() {
    
}
@property (nonatomic, strong) SoundUtility *mySound;
@end
@implementation TabButton

-(id)initTabButtons:(int)senderTag
{
    if ((self = [super init]))
    {
        [self setUpButtons:senderTag];
    }return self;
}

// TODO:ver2.0.0 - ボタン５個にする。
#pragma mark - ボタン４つ作成
-(void)setUpButtons:(int)senderTag
{
    self.mySound = [[SoundUtility alloc] initWithFileName:SOUND_TAB_BUTTON ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
    //背景作成
    //不要かな？
    self.frame = CGRectMake(0, 0, WIN_SIZE.width, 50);
    //self.backgroundColor = [UIColor yellowColor];
    
    //ボタン４つ作成
    int buttonWidth = WIN_SIZE.width / 5;
    UIButton *topButton = [UIButton buttonWithType:UIButtonTypeCustom];
    topButton.frame = CGRectMake(0,0,buttonWidth,50);
    topButton.tag = TAB_TOP;
    [topButton setImage:[UIImage imageNamed:@"toptab.png"] forState:UIControlStateNormal];
    [topButton setImage:[UIImage imageNamed:@"toptab-tap.png"] forState:UIControlStateHighlighted];
    [topButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:topButton];

    UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mapButton.frame = CGRectMake(buttonWidth,0,buttonWidth,50);
    mapButton.tag = TAB_MAP;
    [mapButton setImage:[UIImage imageNamed:@"maptab"] forState:UIControlStateNormal];
    [mapButton setImage:[UIImage imageNamed:@"maptab-tap"] forState:UIControlStateHighlighted];
    [mapButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:mapButton];
    
    UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraButton.frame = CGRectMake(buttonWidth * 2,0,buttonWidth,50);
    cameraButton.tag = TAB_CAMERA;
    [cameraButton setImage:[UIImage imageNamed:@"cameratab.png"] forState:UIControlStateNormal];
    [cameraButton setImage:[UIImage imageNamed:@"cameratab-tap.png"] forState:UIControlStateHighlighted];
    [cameraButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cameraButton];

    UIButton *cardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cardButton.frame = CGRectMake(buttonWidth * 3,0,buttonWidth,50);//(160,-5,80,55)
    cardButton.tag = TAB_CARD;
    [cardButton setImage:[UIImage imageNamed:@"challengetab.png"] forState:UIControlStateNormal];
    [cardButton setImage:[UIImage imageNamed:@"challengetab-tap.png"] forState:UIControlStateHighlighted];
    [cardButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cardButton];
    
    UIButton *albumButton = [UIButton buttonWithType:UIButtonTypeCustom];
    albumButton.frame = CGRectMake(buttonWidth * 4,0,buttonWidth,50);
    albumButton.tag = TAB_ALBUM;
    [albumButton setImage:[UIImage imageNamed:@"albumtab"] forState:UIControlStateNormal];
    [albumButton setImage:[UIImage imageNamed:@"albumtab-tap"] forState:UIControlStateHighlighted];
    [albumButton addTarget:self action:@selector(buttonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:albumButton];
    
    if(senderTag == TAB_TOP)
    {
        [topButton setImage:[UIImage imageNamed:@"toptab-active"] forState:UIControlStateNormal];
        
    }
    else if (senderTag == TAB_MAP)
    {
        [mapButton setImage:[UIImage imageNamed:@"maptab-active"] forState:UIControlStateNormal];
        
    }
    else if (senderTag == TAB_CARD)
    {
        [cardButton setImage:[UIImage imageNamed:@"challengetab-active"] forState:UIControlStateNormal];

    }
    else if (senderTag == TAB_ALBUM)
    {
        [albumButton setImage:[UIImage imageNamed:@"albumtab-active"] forState:UIControlStateNormal];

    }
    else if (senderTag == TAB_CAMERA)
    {
        [cameraButton setImage:[UIImage imageNamed:@"cameratab-active.png"] forState:UIControlStateNormal];
    }
}



-(void)buttonPushed:(id)sender
{
    [self.mySound audioStart];
    self.tag = [sender tag];
    [self.delegate anyButtonPushed:self];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
