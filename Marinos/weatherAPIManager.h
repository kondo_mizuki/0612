//
//  weatherAPIManager.h
//  TigersApp
//
//  Created by timecapsule02 on 2015/06/24.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface weatherAPIManager : NSObject
+ (weatherAPIManager*)sharedManager;

-(NSArray*)weatherAPI:(int)adjustNum;
//-(NSArray*)tideAPI;
@end
