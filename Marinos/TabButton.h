//
//  TabButton.h
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/14.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TabButton;

@protocol tabButtonDelegate <NSObject>

- (void)anyButtonPushed:(TabButton *)sender;

@end
@interface TabButton : UIButton
{
    
}
-(id)initTabButtons:(int)senderTag;

@property (nonatomic, weak) id <tabButtonDelegate> delegate;

@end
