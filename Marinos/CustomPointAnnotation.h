#import <MapKit/MapKit.h>
#import "Define.h"

@interface CustomPointAnnotation : MKPointAnnotation

@property (nonatomic, assign) int placeID;
@property (nonatomic, assign) double radius;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, copy) NSString *ad_text;
@property (nonatomic, assign) int placeCategoryIdentifier;

@end
