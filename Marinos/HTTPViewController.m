
//
//  HTTPViewController.m
//  Marinos
//
//  Created by eagle014 on 2014/02/15.
//  Copyright (c) 2014年 井上　貴文. All rights reserved.
//

#import "HTTPViewController.h"
#import "XMLReader.h"
#import "XMLUtility.h"
#import "SVProgressHUD.h"
#import "R9HTTPRequest.h"
#import "DatabaseUtility.h"
#import "Util.h"
#import "FileDownloadObject.h"

@interface HTTPViewController ()

@end

@implementation HTTPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - showAlert
- (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView setTag:otherAlert];
    [alertView show];
}
#pragma mark - alertDelegate
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *user_ID = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID];
    NSNumber *boolObj = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_SEND_FLAG];
    BOOL sendFlag = [boolObj boolValue];
    AlertType at = [alertView tag];
    switch (at) {
        case firstAlert:
            NSLog(@"First");
            if (buttonIndex == 0) {
                //
            }
            break;
        case otherAlert:
            NSLog(@"Other");
            if (buttonIndex == 0) {
                if ([user_ID length] == 0 || [user_ID isEqualToString:@""]) {
                    //USER_ID再取得
                    [self checkUserID:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID]];
                }else if (!sendFlag){
                    //XML再送
                    NSString *xmlStr = [XMLUtility makeXML];
                    if (xmlStr != nil && [xmlStr length] > 0) {
                        //OK
                        NSLog(@"XML空じゃない");
                        [self postXML:xmlStr];
                    }
                }else if (sendFlag){
                    //[self checkUserVersion:[[NSUserDefaults standardUserDefaults] objectForKey:KEY_CURRENT_VERSION]];
                    [self goBackTopVC];
                }else{
                    [self goBackTopVC];
                }
            }
            break;
            
        default:
            break;
    }

}
#pragma mark - setParts
- (void)setParts{
    
    NSString *imgStr;
    if (WIN_SIZE.height == 568) {
        imgStr = @"Default.png";
    }else{
        imgStr = @"Default-568h.png";
    }
    UIImageView *bgImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgStr]];
    [bgImg setAlpha:1.0f];
    [bgImg setFrame:CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height)];
    [bgImg setCenter:CGPointMake(WIN_SIZE.width * 0.5f, WIN_SIZE.height * 0.5f)];
    [self.view addSubview:bgImg];
}

#pragma mark - checkUserID
- (void)checkUserID:(NSString *)user_ID{
    [SVProgressHUD showWithStatus:@"CheckingUserID..." maskType:SVProgressHUDMaskTypeGradient];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSURL *url;
    if ([user_ID length] == 0 || [user_ID isEqualToString:@""]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_CONNECT]];
    }else{
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_CONNECT, [NSString stringWithFormat:@"?uid=%d",[user_ID intValue]]]];
    }
    NSLog(@"URL:%@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //[request setValue:@"MarinosApp" forHTTPHeaderField:@"User-Agent"];

    // リクエストを送信する。
    // NSURLConnectionのインスタンスを作成したら、すぐに
    // 指定したURLへリクエストを送信し始めます。
    // delegate指定すると、サーバーからデータを受信したり、
    // エラーが発生したりするとメソッドが呼び出される。
    self.myConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    // 作成に失敗する場合には、リクエストが送信されないので
    // チェックする
    if (!self.myConnect) {
        NSLog(@"connection error.");
    }
}

#pragma mark - makeXML
- (void)postXML:(NSString *)xmlStr{
    [SVProgressHUD showWithStatus:@"SendingUserData..." maskType:SVProgressHUDMaskTypeGradient];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    //TODO:time_out
    //実装予定
    self.timer = [NSTimer timerWithTimeInterval:TIMEOUT_TIME target:self selector:@selector(timeOut:) userInfo:nil repeats:NO];
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] stringForKey:KEY_USER_ID];
    NSLog(@"userID:%@",userID);
    //NSString *sendStr = [NSString stringWithFormat:@"http://image.marinos.capsule-server.info/xml/%d.xml",[userID intValue]];
    NSString *sendStr = [NSString stringWithFormat:@"%@%@" , [[Util sharedManager] myServer] ,PHP_PROGRAM_SYNC];//sync
    NSLog(@"sendString:%@",sendStr);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:sendStr]];//?xmlname=[会員ID]_～.xml
    NSError *error = nil;
    NSData *xmlData = [xmlStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDic = [XMLReader dictionaryForXMLData:xmlData error:&error];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDic options:NSJSONWritingPrettyPrinted error:&error];
    request.HTTPMethod = @"POST";
    //⬇もしかしたら先方の都合でいるかもしれない
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"MarinosApp" forHTTPHeaderField:@"User-Agent"];
    
    request.HTTPBody = jsonData;
    
    //NSURLConnection *connect = [NSURLConnection connectionWithRequest:request delegate:self];
    self.myConnect = [NSURLConnection connectionWithRequest:request delegate:self];
    if (self.myConnect) {
        NSLog(@"SUCCESS");
    } else {
        NSLog(@"FAILD");
    }
}
/*
#pragma mark - checkUserVersion
- (void)checkUserVersion:(NSString *)version{
    [SVProgressHUD showWithStatus:@"CheckingUserInfo..." maskType:SVProgressHUDMaskTypeGradient];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSString *user_ID = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_VERSION, [NSString stringWithFormat:@"?uid=%d",[user_ID intValue]]]];;
    NSLog(@"URL:%@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //[request addValue:@"MarinosApp" forHTTPHeaderField:@"User-Agent"];
    // リクエストを送信する。
    // NSURLConnectionのインスタンスを作成したら、すぐに
    // 指定したURLへリクエストを送信し始めます。
    // delegate指定すると、サーバーからデータを受信したり、
    // エラーが発生したりするとメソッドが呼び出される。
    self.myConnect = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    // 作成に失敗する場合には、リクエストが送信されないので
    // チェックする
    if (!self.myConnect) {
        NSLog(@"connection error.");
        
    }
}
*/

#pragma mark - selfDelegate
- (void)goBackTopVC{
    //sound
    [[Util sharedManager] playSound:SOUND_TAB];
    
    [self.delegate HTTPDidFinished:self];
}

#pragma mark - timeOut
- (void)timeOut:(NSTimer *)time{
    if (self.myConnect) {
        [self.myConnect cancel];
        [self.timer invalidate];
        self.timer = nil;
        [SVProgressHUD dismiss];
        [self showAlertMessage:@"PleaseReTry" withTitle:@"Network Error"];
    }
}

#pragma mark - NSURLConnectionDelegate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
	if(httpResponse.statusCode == 200) {
		NSLog(@"Success ٩꒰๑ ´∇`๑꒱۶✧");
	} else {
		NSLog(@"Failed (´；ω；｀)");
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    // 取得したデータをreceivedDataへ格納する
	NSLog(@"受信データ（バイト数）: %d", [data length]);
	//[self.receivedData appendData:data];
    self.receivedData = data;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    self.myConnect = nil;
    static int faildCount = 0;
    //レスポンスからJSONデータ作成
    NSDictionary *jsonObj = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"%@",jsonObj);
    //JSONから必要項目を抜き出す
    NSString *code = [jsonObj objectForKey:@"code"];
    if ([jsonObj objectForKey:@"code"] == [NSNull null] || [jsonObj count] == 0) {
        //faild_serverData
        [SVProgressHUD dismiss];
        [self showAlertMessage:@"Please retry!!" withTitle:@"Upload Error"];
    }else{
        //success_serverData
        if ([code isEqualToString:CONNECT_CODE_CREATE]) {
            //ここで保存してあるデータと、返って来たデータとの乖離を比較する必要がある。
            int uid = [[jsonObj objectForKey:@"uid"] intValue];
            NSString *userID = [NSString stringWithFormat:@"%d",uid];
            NSString *uuid = [jsonObj objectForKey:@"uuid"];
            NSLog(@"USER_IDの取得に成功！code:%@とuserID:%@とuuid:%@", code, userID, uuid);
            if (uid == [[[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID] intValue]) {
                return;
            }else{
                [[NSUserDefaults standardUserDefaults] setObject:userID forKey:KEY_USER_ID];
                [[NSUserDefaults standardUserDefaults] setObject:uuid forKey:KEY_UNIVERSAL_UNIQUE_ID];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [SVProgressHUD dismiss];
                NSString *xmlStr = [XMLUtility makeXML];
                if (xmlStr != nil && [xmlStr length] > 0) {
                    //OK
                    NSLog(@"XML空じゃない");
                    [self postXML:xmlStr];
                }
            }
        } else if ([code isEqualToString:CONNECT_CODE_FINISHED]){
            NSLog(@"USER_IDは既に取得済みだけど、XML通信が終わっているかは別の話");
            NSString *user_ID = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USER_ID];
            NSNumber *boolObj = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_SEND_FLAG];
            BOOL sendFlag = [boolObj boolValue];
            if ((user_ID != nil && [user_ID length] > 0) && (!sendFlag)) {
                NSLog(@"POST通信「も」成功");
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:KEY_SEND_FLAG];
                //新しくポイントを保存
                int previousMyPoint = [[NSUserDefaults standardUserDefaults] integerForKey:@"KEY_MYPOINT"];
                [[NSUserDefaults standardUserDefaults] setInteger:previousMyPoint forKey:KEY_MY_CARD_POINT];
                [[NSUserDefaults standardUserDefaults] synchronize];
                //リソースデータをnativePathに保存
                /*
                [SVProgressHUD showWithStatus:@"Data Miraging..." maskType:SVProgressHUDMaskTypeGradient];
                dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_queue_t mainQueue = dispatch_get_main_queue();
                dispatch_async(globalQueue, ^(void){
                    [[FileDownloadObject sharedManager] dataMigrationFromResource];
                    dispatch_async(mainQueue, ^(void){
                        [SVProgressHUD showSuccessWithStatus:@"Completed!!"];
                        NSLog(@"Finish!!");
                        [self goBackTopVC];
                    });
                });
                 */
                //[[FileDownloadObject sharedManager] dataMigrationFromResource];
                //[SVProgressHUD dismiss];
                //[self goBackTopVC];
            }else{
                [SVProgressHUD dismiss];
                [self goBackTopVC];
            }
        }else if ([code isEqualToString:CONNECT_CODE_ERROR_FAILD]){
            //エラーコードを受け取った場合
            faildCount++;
            if (faildCount >= MAX_FAILD_C0UNT) {
                [self showAlertMessage:@"Please retry!!" withTitle:@"Upload Error"];
                //強制終了
                [SVProgressHUD dismiss];
                return;
            }
            /*
            NSString *xmlStr = [XMLUtility makeXML];
            if (xmlStr != nil && [xmlStr length] > 0) {
                //OK
                NSLog(@"XML空じゃない");
                [self postXML:xmlStr];
            }*/
        }else if ([code isEqualToString:CONNECT_CODE_ERROR_EXCEPTION]){
            //例外処理USER_IDが無い
            [self checkUserID:nil];
        }
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    NSLog(@"サーバーエラー：%@", error);
    self.myConnect = nil;
    [self showAlertMessage:@"データを送信します\n通信環境の良い所で行って下さい。" withTitle:@"サーバーエラー"];
    [SVProgressHUD dismiss];
}

#pragma mark - FileObjDelegate
- (void) fileDownloadObj:(FileDownloadObject *)obj downloadProcessSuccess:(NSDictionary *)dataDic{
    [SVProgressHUD showSuccessWithStatus:@"Complete!"];
}

- (void) fileDownloadObj:(FileDownloadObject *)obj downloadProcessFaild:(NSDictionary *)dataDic{
    
}

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setParts];
}

- (void)viewWillDisappear:(BOOL)animated{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
