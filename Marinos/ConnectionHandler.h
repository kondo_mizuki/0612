//
//  ConnectionHandler.h
//  TimeCapsuleSampleWithARC
//
//  Created by kyo on 2013/01/25.
//  Copyright (c) 2013年 kyo. All rights reserved.


#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CalledNetworkAPI)
{
    NoDesignation = 0,
    SaveUUID,
    TmpImage,
    CommitImage,
    getMapCount,
    getAreaNew
};

@interface ConnectionHandler : NSObject {
    NSMutableData *receivedData;
	NSStringEncoding receivedDataEncoding;
}

@property(nonatomic, assign) CalledNetworkAPI api;
@property(nonatomic, strong) NSString *html_path;
@property(nonatomic, strong) NSDictionary *AMEDASMapTiles;

@end
