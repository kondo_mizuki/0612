//
//  UMKSEManager.h
//  Umiken
//
//  Created by timecapsule07 on 2015/03/17.
//  Copyright (c) 2015年 timecapsule.inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SE_VOLUME @"volume"
#define BGM_VOLUME @"bgm_volume"

#define SE_CANCEL @"cancel.mp3"
#define SE_BUY @"clearing1.mp3"
#define SE_TAP @"tap.mp3"
#define SE_TREASURE @"treasure_get.mp3"
#define SE_JK @"tap_character.mp3"
#define SE_JK_OLD @"tap_character_old.mp3"


@interface JKCSEManager : NSObject{
    NSMutableArray *soundArray;
}

@property(nonatomic) float soundVolume;
@property(nonatomic) float bgmVolume;
+ (JKCSEManager *)sharedManager;
- (NSTimeInterval)playSound:(NSString *)soundName;    //ファイル名を指定してSEを再生
- (void)playBGM:(NSString*)bgmName,...NS_REQUIRES_NIL_TERMINATION;  //ファイル名を指定してBGMを再生 BGMは無限ループ 呼び出し時点で再生中のBGMは全て停止される

- (void)stopBGM;
- (void)restartBGM;
- (void)playTapSE;


@end
