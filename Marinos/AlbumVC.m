#import "AlbumVC.h"
#import "AlbumCell.h"
#import "ViewManager.h"
#import "Util.h"
#import "DatabaseUtility.h"
#import "SVProgressHUD.h"
#import "Toast+UIView.h"
#import "CustomTabBarController.h"
#import "JKCSEManager.h"

@interface AlbumVC (){
    BOOL toggle;
    NSString *albumName;
    NSDictionary *possessionDic;
}

@property (nonatomic, strong) UIRefreshControl *myRefresh;
@property (nonatomic, assign) BOOL tabBarIsVisible;


@end

@implementation AlbumVC
@synthesize adGManagerViewController = ADGViewController;

#pragma mark - iOS7対応
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - LifeCycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    opend=false;
    
    [self setStage];
    //[self setToggle];
    [self createCollectionView];
    
    [self showADGeneration];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(ADGViewController){
        [ADGViewController resumeRefresh];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    if(!opend){
        self.screenName = @"AlbumVC";
        self.navigationController.navigationBarHidden = YES;
        [self reloadCards:nil];
    }
    opend=false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) showADGeneration {
    int screenHeight = [[UIScreen mainScreen] bounds].size.height;
    CGFloat tabBarHeight = self.tabBarController.tabBar.bounds.size.height;
    screenHeight = screenHeight - 50 - tabBarHeight;
    NSDictionary *adgparam = @{
                               @"locationid" : @"10723", //管理画面から払い出された広告枠ID
                               @"adtype" : @(kADG_AdType_Free), //管理画面にて入力した枠サイズ(kADG_AdType_Sp：320x50, kADG_AdType_Large:320x100, kADG_AdType_Rect:300x250, kADG_AdType_Tablet:728x90, kADG_AdType_Free
                               @"originx" : @(0), //広告枠設置起点のx座標 ※下記参照
                               @"originy" : @(screenHeight), //広告枠設置起点のy座標 ※下記参照
                               @"w" : @(320), //広告枠横幅 ※下記参照
                               @"h" : @(50)  //広告枠高さ ※下記参照
                               };
    ADGManagerViewController *adgvc = [[ADGManagerViewController alloc]initWithAdParams :adgparam :self.view];
    self.adGManagerViewController = adgvc;
    //    [adgvc release];
    ADGViewController.delegate = self;
    [ADGViewController loadRequest];
}

-(void)albumDataLoad
{
    if ([self.possessionArray count] > 0 || self.possessionArray != nil) {
        [self.possessionArray removeAllObjects];
        self.possessionArray = nil;
        self.possessionArray = [NSMutableArray array];
    } else {
        self.possessionArray = [NSMutableArray array];
    }
    
    NSArray *cardDetailArray = [[DatabaseUtility sharedManager] getCardDataForAlbumPossession];
    self.possessionArray = [NSMutableArray arrayWithArray:cardDetailArray];
    NSLog(@"カード枚数は%@枚", @([self.possessionArray count]));
//    NSLog(@"possessionAry:%@", self.possessionArray);
}

- (void) setStage {
//        背景が画像であれば、入れる。今の所は単色なので、Tint Colorで対応済
//    [_navigationBar setBackgroundImage:[UIImage imageNamed:@"header_bg"] forBarMetrics:UIBarMetricsDefault];
}

//- (void) setToggle {
//    ToggleView *toggleView = [[ToggleView alloc]initWithFrame:CGRectMake(0, 0, 200, 25) toggleViewType:ToggleViewTypeWithLabel toggleBaseType:ToggleBaseTypeChangeImage toggleButtonType:ToggleButtonTypeDefault];
//    toggleView.toggleDelegate = self;
//    toggleView.center = CGPointMake([Util getCenterPoint].x, 75);
//    [self.view addSubview:toggleView];
//}

- (void)startRefresh:(UIRefreshControl *)sender{
    [self reloadCards:sender];
}

- (void)reloadCards:(UIRefreshControl *)sender{
    [SVProgressHUD showWithStatus:@"Data Loading..." maskType:SVProgressHUDMaskTypeGradient];
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    dispatch_async(globalQueue, ^{
        [self albumDataLoad];
        dispatch_async(mainQueue, ^{
            [self.collectionView reloadData];
            self.tabBarIsVisible = YES;
            [SVProgressHUD showSuccessWithStatus:@"Completed!!"];
            if(sender!=nil)[self.myRefresh endRefreshing];
        });
    });
}

#pragma mark -
-(void)createCollectionView
{
    _flowLayout = [[UICollectionViewFlowLayout alloc] init];
    _flowLayout.itemSize = CGSizeMake(80,119);
    _flowLayout.headerReferenceSize = CGSizeMake(0, 0);
    _flowLayout.footerReferenceSize = CGSizeMake(0, 0);
    _flowLayout.minimumLineSpacing = 4.0;
    _flowLayout.minimumInteritemSpacing = 0;
    _flowLayout.sectionInset = UIEdgeInsetsMake(0,0,43,0);
    
    _collectionView.collectionViewLayout=_flowLayout;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [_collectionView registerClass:[AlbumCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    
    self.myRefresh = [[UIRefreshControl alloc] init];
    [self.myRefresh addTarget:self action:@selector(startRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.myRefresh];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //return [self.possessionArray count];
   return 10;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    int card_category = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"card_category"] intValue];
    /*int cardID = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"card_id"] intValue];
    int card_year = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"card_year"] intValue];*/
    int possession = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"possession"] intValue];
    
    
    possessionDic=self.possessionArray[indexPath.row];
    if (possession >= 1) {
        [self performSegueWithIdentifier:@"albumDetailSegue" sender:self];
        
        [[Util sharedManager] playSound:SOUND_ALBUM];
    } else if (possession == 0){
        //獲得枚数ゼロ
        if (card_category == CARD_CATEGORY_HOMEGAME) {
            [self.view makeToast:@"試合会場でチェックインして\n手に入れよう！" duration:1.0f position:@"center"];
            [[Util sharedManager] playSound:SOUND_KIIIN];
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"UICollectionViewCell";
    AlbumCell *cell = (AlbumCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    for (UIView *subview in [cell.contentView subviews])
    {
        [subview removeFromSuperview];
    }
    
//    int cardID = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"card_id"] intValue];
//
//    int possession = 0;
//    possession = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"possession"] intValue];
//
//    int card_category = [[[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"card_category"] intValue];
//    NSString *card_text = [[self.possessionArray objectAtIndex:indexPath.row] objectForKey:@"card_text_1"];
//    [cell displayCard:cardID possession:possession albumName:albumName cardCategory:card_category cardText:card_text];
     [cell displayCard:nil possession:nil albumName:nil cardCategory:nil cardText:nil];
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"albumDetailSegue"]){
        AlbumDetailViewController *controller = segue.destinationViewController;
        [controller setHidesBottomBarWhenPushed:YES];
        controller.possessionDic=possessionDic;
    }
}

- (void)selectLeftButton
{
    NSLog(@"LeftButton Selected");
    [self albumDataLoad];
    [self.collectionView reloadData];
    [self.myRefresh endRefreshing];
}

- (void)selectRightButton
{
    NSLog(@"RightButton Selected");
    [self albumDataLoad];
    [self.collectionView reloadData];
    [self.myRefresh endRefreshing];
}

- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}

- (void)ADGManagerViewControllerReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerReceiveAd");
}

- (void)ADGManagerViewControllerFailedToReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerFailedToReceiveAd");
}


@end