//
//  FromThereViewController.m
//  TimeCapsule
//
//  Created by kyo on 2013/05/20.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "FromThereViewController.h"
//#import "SFIAdManager.h"
#import "Util.h"
#import "SVProgressHUD1.h"
#import "DetailViewController.h"

@interface FromThereViewController ()

@property(nonatomic, strong)Util *util;

@end

@implementation FromThereViewController
#pragma mark - iOS7対応
- (UIStatusBarStyle)preferredStatusBarStyle {
    //文字を白くする
    return UIStatusBarStyleLightContent;
}

- (void)backToHistoryView {
    //NSArray *views = self.navigationController.viewControllers;
    //UIViewController *vc = [views objectAtIndex:views.count - 2];
    /*
    SFI_ADMANAGER.viewController = vc;
    [SFI_ADMANAGER refreshBanner];
    */
    [self.delegate dissMissFromThereViewController:self];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked && ![[request.URL path] isEqualToString:@"/more"]) {
        NSLog(@"%@", [request.URL path]);
        NSLog(@"%@", [request.URL absoluteString]);
        
        [self appearDetailViewController:request.URL];
        
        return NO;
    } else if (navigationType == UIWebViewNavigationTypeLinkClicked && [[webView stringByEvaluatingJavaScriptFromString:@"document.URL"] isEqualToString:@"http://www.where.eagle-inc.jp/"]) {
        [self appearDetailViewController:request.URL];
    } else if ([[request.URL absoluteString] hasPrefix:self.util.heroku]) {
        [SVProgressHUD1 showWithStatus:@"Now Searching..." maskType:SVProgressHUD1MaskTypeNone];
    }
    
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView*)webView {
    [SVProgressHUD1 dismiss];
}

- (void)appearDetailViewController:(NSURL*)url {
    [SVProgressHUD1 dismiss];
    //[SFI_ADMANAGER refreshBanner];
    DetailViewController *controller = [[DetailViewController alloc] init];
    controller.delegate = self;
    controller.url = url;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)dissMissMyView{
    //[self.delegate dissMissFromThereViewController:self];
    [[self navigationController] popViewControllerAnimated:NO];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.util = [Util sharedManager];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    backgroundImageView.frame = CGRectMake(0, 0, 320, 568);
    [self.view addSubview:backgroundImageView];
    
    UIImageView *headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_bg"]];
    headerImageView.frame = CGRectMake(0, 0, 320, 49);
    [self.view addSubview:headerImageView];
    
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vft_title"]];
    titleImageView.frame = CGRectMake(80, 0, 160, 49);
    [self.view addSubview:titleImageView];
    
    UIButton *closebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closebtn.frame = CGRectMake(7, 7, 48, 29);
    [closebtn setImage:[UIImage imageNamed:@"backbtn"] forState:UIControlStateNormal];
    [closebtn setImage:[UIImage imageNamed:@"backbtn_active"] forState:UIControlStateHighlighted];
    [closebtn addTarget:self action:@selector(dissMissMyView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closebtn];
    
    // 緯度経度表示用リボン
    UIImageView *laloRibbon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lalo_ribbon"]];
    laloRibbon.frame = CGRectMake(320-130, 49, 144, 21);
    [self.view addSubview:laloRibbon];
    
    // リボンへの経度の表示
    UILabel *laLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 50, 100, 18)];
    laLabel.font = [UIFont systemFontOfSize:13];
    laLabel.textColor = RGBA(255, 249, 192, 1.0);
    laLabel.backgroundColor = [UIColor clearColor];
    laLabel.text = self.latitude;
    [self.view addSubview:laLabel];
    
    // リボンへの経度の表示
    UILabel *loLabel = [[UILabel alloc] initWithFrame:CGRectMake(210+50, 50, 50, 18)];
    loLabel.font = [UIFont systemFontOfSize:13];
    loLabel.textColor = RGBA(255, 249, 192, 1.0);
    loLabel.backgroundColor = [UIColor clearColor];
    loLabel.text = self.longitude;
    [self.view addSubview:loLabel];
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 49+21+4, 320, 568-49)];
    webView.delegate = self;
    webView.scrollView.bounces = NO;
    [self.view addSubview:webView];
    [webView loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@/?latitude=%@&longtitude=%@", self.util.heroku, self.latitude, self.longitude]]]];
    
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.frame = CGRectMake(0, 49+21+4+320-20, 320, 30);
    dateLabel.textAlignment = NSTextAlignmentCenter;
    dateLabel.backgroundColor = [UIColor clearColor];
    
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
	[inputDateFormatter setDateFormat:@"yyyy.MM.dd"];
	NSDate *inputDate = [inputDateFormatter dateFromString:self.recordDate];
    NSDate *now = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *diff = [calendar components:
                              (NSDayCalendarUnit|
                               NSHourCalendarUnit|
                               NSMinuteCalendarUnit|
                               NSSecondCalendarUnit)
                                         fromDate:inputDate
                                           toDate:now
                                          options:0];
    
    dateLabel.text = [NSString stringWithFormat:@"%d days since you visited there.", [diff day]];
    [self.view addSubview:dateLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
