//
//  weatherAPIManager.m
//  TigersApp
//
//  Created by timecapsule02 on 2015/06/24.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import "weatherAPIManager.h"

@implementation weatherAPIManager{
   
}

static weatherAPIManager *sharedUtil = nil;

#pragma mark - singleton method
+ (weatherAPIManager*)sharedManager
{
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [[self alloc] init];
        }
    }
    return sharedUtil;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [super allocWithZone:zone];
            return sharedUtil;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone
{
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

-(NSArray*)weatherAPI:(int)adjustNum{
    //NSString *orign = @"http://api.openweathermap.org/data/2.5/weather?lat=%d&lon=%d";
    NSString *orign = @"http://api.openweathermap.org/data/2.5/forecast/daily?lat=%d&lon=%d&mode=json&cnt=14&units=metric";
    NSString *url = [NSString stringWithFormat:orign,35,140];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *json = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSArray *array = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:nil];
   // NSLog(@"array:%@",array);
   NSArray *weatherArray =[[array valueForKeyPath:@"list"]objectAtIndex:adjustNum];
    
    return weatherArray;
}


@end

