//
//  SettingViewController.m
//  Marinos
//
//  Created by eagle014 on 2014/03/03.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import "SettingViewController.h"
#import "CustomTextView.h"
#import "CustomTabBarController.h"
#import "Util.h"
#import "Toast+UIView.h"
#import "SoundUtility.h"

@interface SettingViewController ()

@property (nonatomic, strong) UIView *bodyView;
@property (nonatomic, strong) SoundUtility *onSound;
@property (nonatomic, strong) SoundUtility *offSound;
//@property (nonatomic, strong) AVAudioPlayer *offSound;
@property (nonatomic, strong) UISwitch *mSwitch;

@end

@implementation SettingViewController
#pragma mark - iOS7対応
- (UIStatusBarStyle)preferredStatusBarStyle {
    //文字を白くする
    return UIStatusBarStyleLightContent;
}

- (void)transitionAnimation{
    [(CustomTabBarController *)self.tabBarController showHideTabBar:YES];
    CATransition *transition = [CATransition animation];
    [transition setDuration:0.5f];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [transition setType:kCATransitionReveal];
    [transition setSubtype:kCATransitionFromBottom];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}
#pragma mark - goBackPreviousView
- (void)goBackPreviousView:(UIButton *)sender{
    //SoundUtility *su = [[SoundUtility alloc] initWithFileName:SOUND_SETTING_OFF ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
    //[su audioStart];
    [self.offSound audioStart];
    //[self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:NO];
    [self performSelector:@selector(transitionAnimation) withObject:nil afterDelay:0.5f];
    // アニメーション作成
    /*
    CATransition *transition = [CATransition animation];
    [transition setDuration:0.5f];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [transition setType:kCATransitionReveal];
    [transition setSubtype:kCATransitionFromBottom];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
     */
}

#pragma mark - setUpParts
- (void)setUpHeaderParts{
    CGFloat topBarHeight = [[Util sharedManager] getAdjustedHeaderPartsHeight];
    // 投稿画面のヘッダー画像
    NSString *iOS_adjust;
    CGFloat topbarAdjust;
    CGFloat bottomAdjust;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            iOS_adjust = @"new_topbar-none_iOS6_44pxl.png";
            topbarAdjust = 13.0f;
            bottomAdjust = 20.0f;
            break;
        case NSOrderedSame:
            iOS_adjust =@"new_topbar-none_iOS7_64pxl.png";
            topbarAdjust = 6.0f;
            bottomAdjust = 0.0f;
            break;
        case NSOrderedDescending:
            iOS_adjust =@"new_topbar-none_iOS7_64pxl.png";
            topbarAdjust = 6.0f;
            bottomAdjust = 0.0f;
            break;
        default:
            break;
    }
    UIImage *topBarImg = [UIImage imageNamed:iOS_adjust];
    UIImageView *topBarImgView = [[UIImageView alloc] initWithImage:topBarImg];
    [topBarImgView setFrame:CGRectMake(0.0f, 0.0f, WIN_SIZE.width, topBarHeight)];
    [self.view addSubview:topBarImgView];
    
    CGFloat labelWidth = 240.0f;
    CGFloat labelHeight = 36.0f;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((WIN_SIZE.width - labelWidth) / 2, topBarHeight - labelHeight, labelWidth, labelHeight)];
    titleLabel.font = [UIFont fontWithName:MY_FONT size:TOP_TITLE_FONT_SIZE];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [titleLabel setText:@"ユーザー情報"];
    [topBarImgView addSubview:titleLabel];
    
    CGFloat btnSize = 25.0f;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(5.0f, topBarHeight - btnSize - topbarAdjust, btnSize, btnSize);
    [backBtn setImage:[UIImage imageNamed:@"cast_scene_btn_close.png"] forState:UIControlStateNormal];
    [backBtn setImage:[UIImage imageNamed:@"cast_scene_btn_close_tap.png"] forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(goBackPreviousView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    self.bodyView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, topBarHeight, WIN_SIZE.width, (WIN_SIZE.height - topBarHeight))];
    [self.bodyView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.bodyView];
    
}

- (void)setUpBodyParts{
    CGFloat bodySizeHeight = self.bodyView.frame.size.height;
    CGFloat sideAdjust = 10.0f;
    CGFloat heightAdjust = 5.0f;
    //5%
    CGSize smallLabelSize = CGSizeMake(300.0f, bodySizeHeight * 0.05f);
    CGFloat smallLabelFontSize = 15.0f;
    //12.5%
    CGSize largeLabelSize = CGSizeMake(300.0f, bodySizeHeight * 0.075f);
    CGFloat largeLabelFontSize = 20.0f;
    
    //75%
    CGSize textViewSize = CGSizeMake(300.0f, bodySizeHeight * 0.55f);
    
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            smallLabelFontSize = 12.5f;
            break;
        case NSOrderedSame:
            smallLabelFontSize = 15.0f;
            break;
        case NSOrderedDescending:
            smallLabelFontSize = 15.0f;
            break;
        default:
            break;
    }
    
    UILabel *verNumberLable = [[UILabel alloc] initWithFrame:CGRectMake(sideAdjust, heightAdjust + 20.0f, smallLabelSize.width, smallLabelSize.height)];
    NSString *verNumberLableStr = @"PUSH通知を受け取る";
    NSAttributedString *attrStr = [NSAttributedString attributedStringWithString:verNumberLableStr];
    [verNumberLable setAttributedText:attrStr];
    [verNumberLable setTextColor:[UIColor blackColor]];
    [verNumberLable setFont:[UIFont fontWithName:MY_FONT size:smallLabelFontSize]];
    [self.bodyView addSubview:verNumberLable];
    
    self.mSwitch = [[UISwitch alloc] init];
    [self.mSwitch setFrame:CGRectMake(260.0f, heightAdjust + 20.0f, 40.0f, 20.0f)];
    NSLog(@"booooooool::::::::::::%d", [[[NSUserDefaults standardUserDefaults] objectForKey:KEY_RECEIVE_PUSH_FLAG] boolValue]);
    [self.mSwitch setOn:[[[NSUserDefaults standardUserDefaults] objectForKey:KEY_RECEIVE_PUSH_FLAG] boolValue]];
    [self.mSwitch addTarget:self action:@selector(changeSwitchValue:) forControlEvents:UIControlEventValueChanged];
    [self.bodyView addSubview:self.mSwitch];
    
    UILabel *versionNumber = [[UILabel alloc] initWithFrame:CGRectMake(sideAdjust, verNumberLable.frame.size.height + heightAdjust * 2, largeLabelSize.width, largeLabelSize.height)];
    [versionNumber setTextColor:[UIColor colorWithRed:0.004f green:0.251f blue:0.608f alpha:1.0f]];
    [versionNumber setBackgroundColor:[UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1.0f]];
    NSString *currentVerStr = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *verInfoText = [@" " stringByAppendingString:[APP_NAME stringByAppendingString:currentVerStr]];
    NSAttributedString *a_verInfoText = [NSAttributedString attributedStringWithString:verInfoText];
    [versionNumber setAttributedText:a_verInfoText];
    [versionNumber setFont:[UIFont fontWithName:MY_FONT size:largeLabelFontSize * 0.75f]];
    //[self.bodyView addSubview:versionNumber];
    
    UILabel *policyLable = [[UILabel alloc] initWithFrame:CGRectMake(sideAdjust, verNumberLable.frame.size.height +versionNumber.frame.size.height + heightAdjust * 3, smallLabelSize.width, smallLabelSize.height)];
    NSString *policyLabelStr = @"利用規約";
    NSAttributedString *a_policyLabelStr = [NSAttributedString attributedStringWithString:policyLabelStr];
    [policyLable setAttributedText:a_policyLabelStr];
    [policyLable setTextColor:[UIColor blackColor]];
    [policyLable setFont:[UIFont fontWithName:MY_FONT size:smallLabelFontSize]];
    [self.bodyView addSubview:policyLable];

    CustomTextView *policyTextView = [[CustomTextView alloc] initWithFrame:CGRectMake(sideAdjust, verNumberLable.frame.size.height +versionNumber.frame.size.height + policyLable.frame.size.height + heightAdjust * 4, textViewSize.width, textViewSize.height)];
    NSString *str = [[NSBundle mainBundle] pathForResource:@"acceptableUsePolicy" ofType:@"plist"];
    NSArray *array = [NSArray arrayWithContentsOfFile:str];
    NSDictionary *dic = [array firstObject];
    NSString *textStr = [dic objectForKey:@"acceptableUsePolicy"];
    [policyTextView setText:textStr];
    [policyTextView setBackgroundColor:[UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1.0f]];
    [policyTextView setTextColor:[UIColor blackColor]];
    [self.bodyView addSubview:policyTextView];
    
    UILabel *uuidLable = [[UILabel alloc] initWithFrame:CGRectMake(sideAdjust, verNumberLable.frame.size.height + versionNumber.frame.size.height + policyLable.frame.size.height + policyTextView.frame.size.height + heightAdjust * 5, smallLabelSize.width, smallLabelSize.height)];
    NSString *uuidLabelStr = @"あなたのID";
    NSAttributedString *a_uuidLabelStr = [NSAttributedString attributedStringWithString:uuidLabelStr];
    [uuidLable setAttributedText:a_uuidLabelStr];
    [uuidLable setTextColor:[UIColor blackColor]];
    [uuidLable setFont:[UIFont fontWithName:MY_FONT size:smallLabelFontSize]];
    [self.bodyView addSubview:uuidLable];
    /*
    UITextView *uuidText = [[UITextView alloc] initWithFrame:CGRectMake(sideAdjust, verNumberLable.frame.size.height + versionNumber.frame.size.height + policyLable.frame.size.height + policyTextView.frame.size.height + uuidLable.frame.size.height + heightAdjust * 6, largeLabelSize.width, largeLabelSize.height)];
    [uuidText setTextColor:[UIColor colorWithRed:0.004f green:0.251f blue:0.608f alpha:1.0f]];
    [uuidText setBackgroundColor:[UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1.0f]];
    //[versionNumber setTextColor:[UIColor blueColor]];
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_UNIVERSAL_UNIQUE_ID];
    NSString *uuidStr = [@" " stringByAppendingString:uuid];
    [uuidText setText:uuidStr];
    [uuidText setFont:[UIFont fontWithName:MY_FONT size:largeLabelFontSize]];
    [uuidText setEditable:NO];
    [uuidText setScrollEnabled:NO];
    [self.bodyView addSubview:uuidText];
    */
    OHAttributedLabel *uuidLinkLabel = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(sideAdjust, verNumberLable.frame.size.height + versionNumber.frame.size.height + policyLable.frame.size.height + policyTextView.frame.size.height + uuidLable.frame.size.height + heightAdjust * 6, largeLabelSize.width, largeLabelSize.height)];
    [uuidLinkLabel setDelegate:self];
    NSString *uuid = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_UNIVERSAL_UNIQUE_ID];
    NSString *uuidStr = [@" " stringByAppendingString:uuid];
    NSMutableAttributedString *a_uuidStr = [NSMutableAttributedString attributedStringWithString:uuidStr];
    //[uuidLinkLabel setAttributedText:a_uuidStr];
    [a_uuidStr setTextColor:[UIColor colorWithRed:0.004f green:0.251f blue:0.608f alpha:1.0f]];
    [a_uuidStr setFont:[UIFont fontWithName:MY_FONT size:largeLabelFontSize * 1.5f]];
    NSRange linkRange = [uuidStr rangeOfString:uuid];
    [a_uuidStr setLink:[NSURL URLWithString:@""] range:linkRange];
    [uuidLinkLabel setAttributedText:a_uuidStr];
    [uuidLinkLabel setBackgroundColor:[UIColor colorWithRed:0.847f green:0.847f blue:0.847f alpha:1.0f]];
    [self.bodyView addSubview:uuidLinkLabel];
}

- (void) changeSwitchValue:(UISwitch *)sender {
    if (self.mSwitch.on) {
        //on
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:KEY_RECEIVE_PUSH_FLAG];
    } else {
        //off
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:KEY_RECEIVE_PUSH_FLAG];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo{
    static BOOL result = NO;
    //:TODO　実装
    [self.view makeToast:@"こちらが貴方のIDです。\n大切に保管して下さい。" duration:1.0f position:@"center"];
    return result;
}
#pragma mark - LifeCycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.onSound = [[SoundUtility alloc] initWithFileName:SOUND_SETTING_ON ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
        self.offSound = [[SoundUtility alloc] initWithFileName:SOUND_SETTING_OFF ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpHeaderParts];
    [self setUpBodyParts];
    
    [self.onSound audioStart];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setHidesBottomBarWhenPushed:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
