//
//  JKCSEManager.m
//  Umiken
//
//  Created by timecapsule07 on 2015/03/17.
//  Copyright (c) 2015年 timecapsule.inc. All rights reserved.
//

#import "JKCSEManager.h"
#import <AVFoundation/AVFoundation.h>

@interface JKCSEManager()
{
    NSMutableArray *bgmPlayerArray;
    AVAudioPlayer *tapSE;
}

@end

@implementation JKCSEManager

+ (JKCSEManager*)sharedManager
{
    static JKCSEManager *sharedManager = nil;
    
    if (!sharedManager) {
        sharedManager = [[JKCSEManager alloc]init];
    }
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        soundArray = [[NSMutableArray alloc]init];
        bgmPlayerArray = [[NSMutableArray alloc]init];
        [[NSUserDefaults standardUserDefaults]registerDefaults:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:SE_VOLUME]];
        [[NSUserDefaults standardUserDefaults]registerDefaults:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:BGM_VOLUME]];
        _soundVolume = [[[NSUserDefaults standardUserDefaults]objectForKey:SE_VOLUME]boolValue];
        _bgmVolume = [[[NSUserDefaults standardUserDefaults]objectForKey:BGM_VOLUME]boolValue];
        
        //タップ時の音設定
        NSString *soundPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"tap.mp3"];
        NSURL *urlOfSound = [NSURL fileURLWithPath:soundPath];
        tapSE = [[AVAudioPlayer alloc] initWithContentsOfURL:urlOfSound error:nil];
        [tapSE prepareToPlay];
    }
    return self;
}

- (NSTimeInterval)playSound:(NSString *)soundName
{
    //NSLog(@"%@",soundName);
    NSString *soundPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:soundName];
    NSURL *urlOfSound = [NSURL fileURLWithPath:soundPath];
    NSError *err;
    AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:urlOfSound error:&err];
    [player setNumberOfLoops:0];
    player.volume = _soundVolume;
    player.delegate = (id)self;
    [soundArray addObject:player];
    [player prepareToPlay];
    [player play];
    return player.duration;
}

- (void)playTapSE{
    [tapSE play];
    [tapSE prepareToPlay];
}

- (void)playBGM:(NSString*)first,...
{
    for (AVAudioPlayer *player in bgmPlayerArray){
        [player stop];
    }
    [bgmPlayerArray removeAllObjects];
    
    va_list args;
    va_start(args, first);
    
    for (NSString *bgmName = first; bgmName != nil; bgmName = va_arg(args, NSString*)) {
        NSString *soundPath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:bgmName];
        NSURL *urlOfSound = [NSURL fileURLWithPath:soundPath];
        
        AVAudioPlayer *player = [[AVAudioPlayer alloc]initWithContentsOfURL:urlOfSound error:nil];
        player.numberOfLoops = -1;
        player.volume = _bgmVolume;
        player.delegate = (id)self;
        [bgmPlayerArray addObject:player];
        [player prepareToPlay];
        [player play];
    }
    
    va_end(args);
}

- (void)setSoundVolume:(float)soundVolume
{
    _soundVolume = soundVolume;
//    for (AVAudioPlayer *player in bgmPlayerArray){
//        player.volume = _soundVolume;
//    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSNumber numberWithBool:soundVolume] forKey:SE_VOLUME];
    [ud synchronize];
}

- (void)setBgmVolume:(float)bgmVolume
{
    _bgmVolume = bgmVolume;
    for (AVAudioPlayer *player in bgmPlayerArray){
        player.volume = _bgmVolume;
    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSNumber numberWithBool:bgmVolume] forKey:BGM_VOLUME];
    [ud synchronize];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [soundArray removeObject:player];
}

- (void)stopBGM
{
    for (AVAudioPlayer *player in bgmPlayerArray){
        [player stop];
    }
}

- (void)restartBGM
{
    for (AVAudioPlayer *player in bgmPlayerArray){
        [player play];
    }
}

@end
