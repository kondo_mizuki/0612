//
//  TopVC.h
//  TigersApp
//
//  Created by 菊地 拓也 on 2015/04/13.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ADG/ADGManagerViewController.h>
#import "GAITrackedViewController.h"
#import "WeatherCell.h"
#import "CustomTabBarController.h"
#import "AppDelegate.h"


@interface TopVC : GAITrackedViewController <UINavigationControllerDelegate,ADGManagerViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,NSXMLParserDelegate
>
{
    
}

//日付チェックメソッド
- (void)dateCheck;
@property (weak, nonatomic) IBOutlet UITextView *tideLevelTV;

@end
