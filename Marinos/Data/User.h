#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *prefecture;
@property(assign) NSInteger team;

+ (User *)sharedManager;

@end
