//
//  AppDelegate.m
//  Marinos
//
//

#import "AppDelegate.h"
#import "Util.h"
#import "DatabaseUtility.h"
#import "LocationUtility.h"
#import <GAI.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
/*
    NSLog(@"- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation");
    NSLog(@"absoluteString : %@",[url absoluteString]);
    NSLog(@"scheme         : %@",[url scheme]);
    NSLog(@"query          : %@",[url query]);
    NSString *absoluteString = [url absoluteString];
    if ([absoluteString isEqualToString:@"FMarinosCardCollection://"]) {
        NSLog(@"Open URL Schemes !!");
    }
*/
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
/*
    NSLog(@"- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url");
    NSLog(@"absoluteString : %@",[url absoluteString]);
    NSLog(@"scheme         : %@",[url scheme]);
    NSLog(@"query          : %@",[url query]);
    NSString *absoluteString = [url absoluteString];
    if ([absoluteString isEqualToString:@"FMarinosCardCollection://"]) {
        NSLog(@"Open URL Schemes !!");
    }

    // temporary..............
    if ([url.scheme isEqualToString:@"jp.RevQuest"]) {
        NSLog(@"%@ [%@|%@|%@|%@]", url, url.scheme, url.host, url.path, url.query);

        //ここに処理を書く
    }
    // .......................
*/
    return YES;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    // commit
    return YES;
}
 
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    NSError *error = nil;
    //バックヤードで音楽が鳴ってても、音楽を止めない方法
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryAmbient error:&error];
    if (error) {
        NSLog(@"AVAudioSessionError->%@", error);
    }
    
    //バージョン, UserDataなど初期値の設定
    //[self initData];

    // then initialize Google Analytics.
   // [self initializeGoogleAnalytics];
    
    // アプリ内に保存するディレクトリの設定
    // データ保存用のディレクトリを作成する
//    if ([[FileManager sharedManager] makeDirectoryForIsNotApplicableBackUp]) {
//        // ディレクトリに対して「do not backup」属性をセット
//        [[FileManager sharedManager] addSkipBackupAttribute];
//    }
//    
//    [[Util sharedManager] setMyServer:TIGERS_SERVER_MASTER];
//    [[Util sharedManager] prepareToPlaySounds];
    
    [[DatabaseUtility sharedManager] updateProcess];
    
    [[LocationUtility sharedManager] onPauseSignificantLocation];
    [[LocationUtility sharedManager] getLocation];
    
    //データベースチェック処理
    //[self searchDatabase];
    
    //デバイス初回起動時の処理
//    BOOL b = [[[NSUserDefaults standardUserDefaults] objectForKey:KEY_FIRST_PLAY_DEVICE] boolValue];
//    if (b == NO) {
//        NSUUID *unique_id = [NSUUID UUID];
//        if (unique_id != nil) {
//            NSString *uuidStr = [unique_id UUIDString];
//            [[NSUserDefaults standardUserDefaults] setObject:uuidStr forKey:KEY_UNIVERSAL_UNIQUE_ID_DEVICE];
//        }
//        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:KEY_FIRST_PLAY_DEVICE];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }

//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    InitViewController *ivc = [[InitViewController alloc] init];
//    [self.window setRootViewController:ivc];
//    [self.window makeKeyAndVisible];
    
    [Fabric with:@[CrashlyticsKit]];
    [Util sharedManager].heroku = HEROKU_MASTER;
    
    [self startTabView];
    return YES;
}

-(void) startTabView {
    CustomTabBarController *tabBarController = [[CustomTabBarController alloc] init];
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] window] setRootViewController:tabBarController];
    [[(AppDelegate *)[[UIApplication sharedApplication] delegate] window] makeKeyAndVisible];
}


//-(void)initHarpy{
//    // Set the App ID for your app
//    [[Harpy sharedInstance] setAppID:APP_ID];
//    
//    // (Optional) Set the App Name for your app
//    [[Harpy sharedInstance] setAppName:APP_NAME];
//    
//    /* (Optional) Set the Alert Type for your app
//     By default, the Singleton is initialized to HarpyAlertTypeOption */
//    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeForce];
//    
//    /* (Optional) If your application is not availabe in the U.S. App Store, you must specify the two-letter
//     country code for the region in which your applicaiton is available. */
//    [[Harpy sharedInstance] setCountryCode:@"JP"];
//    
//    // Perform check for new version of your app
//    [[Harpy sharedInstance] checkVersion];
//}

- (void)initializeGoogleAnalytics
{

    //UA-44275326-1
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    //[[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];

    //ToDo : GoogleAnalytics
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-63838698-1"];

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    [[LocationUtility sharedManager] onPauseLocation];
//    [[LocationUtility sharedManager] getSignificantLocation];
}

//アプリの画面が読み込まれたときに行われる処理。
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //[self.tvc dateCheck];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    /*
     Perform daily check for new version of your app
     Useful if user returns to you app from background after extended period of time
     Place in applicationDidBecomeActive:
     
     Also, performs version check on first launch.
     */
//    [[Harpy sharedInstance] checkVersionDaily];
    
    /*
     Perform weekly check for new version of your app
     Useful if you user returns to your app from background after extended period of time
     Place in applicationDidBecomeActive:
     
     Also, performs version check on first launch.
     */
//    [[Harpy sharedInstance] checkVersionWeekly];
    
    [[LocationUtility sharedManager] onPauseSignificantLocation];
    [[LocationUtility sharedManager] getLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[LocationUtility sharedManager] onPauseLocation];
    [[LocationUtility sharedManager] getSignificantLocation];
}

//- (void) searchDatabase {
//    NSArray *tableArray = [[Util sharedManager] showTables];
//    //テーブルが無かった場合のテーブル作成処理
//    if (![tableArray containsObject:@"new_historys_table"]) {
//        [[Util sharedManager] createNewHistorysTable];
//    }
//    
//    //dbのテーブル内にカラムがあるかどうか調べる。
//    //指定テーブルのカラム情報を取得する。
//    NSArray *cardColumnArray = [[Util sharedManager] getColumnFromTable:@"card"];
//    NSLog(@"追加前%@", cardColumnArray);
//    //カラムが無かった時の追加処理
//    if (![cardColumnArray containsObject:@"ad_title"]) {
//        [[Util sharedManager] alterColumnToTable:@"card" columnName:@"ad_title" withType:@"TEXT"];
//    }
//    if (![cardColumnArray containsObject:@"ad_text"]) {
//        [[Util sharedManager] alterColumnToTable:@"card" columnName:@"ad_text" withType:@"TEXT"];
//    }
//    NSLog(@"追加後 %@", [[Util sharedManager] getColumnFromTable:@"card"]);
//    
//    NSArray *placeColumnArray = [[Util sharedManager] getColumnFromTable:@"place"];
//    NSLog(@"追加前%@", placeColumnArray);
//    //カラムが無かった時の追加処理
//    if (![cardColumnArray containsObject:@"ad_text"]) {
//        [[Util sharedManager] alterColumnToTable:@"place" columnName:@"ad_text" withType:@"TEXT"];
//    }
//    NSLog(@"追加後%@", [[Util sharedManager] getColumnFromTable:@"place"]);
//}

- (void) initData {
    [Util setDefaultsValues];
    [Util setUserDataFromUd];
}
#warning Nifty通知時
// 配信端末情報を登録する
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    /*
    NCMBInstallation *currentInstallation = [NCMBInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    //    [currentInstallation setObject:[[UIDevice currentDevice] systemVersion] forKey:@"osVersion"];
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            //端末情報の登録が成功した場合の処理
            NSLog(@"Succeeded token get!");
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            NSString *device_id = currentInstallation.deviceToken;
            NSString *saveDeviceToken = [ud objectForKey:@"TOKEN"];
            if (![saveDeviceToken isEqualToString:device_id]) {
                [ud setObject:device_id forKey:@"TOKEN"];
                [ud synchronize];
                [[DatabaseUtility sharedManager] tablesCreater];
            }
        } else {
            //端末情報の登録が失敗した場合の処理
            if (error.code == 409){
                //失敗理由がdeviceTokenの重複だった場合は、登録された端末情報を取得する
                NCMBQuery *installationQuery = [NCMBInstallation query];
                [installationQuery whereKey:@"deviceToken" equalTo:currentInstallation.deviceToken];
                NCMBInstallation *install = (NCMBInstallation*)[installationQuery getFirstObject:&error];
                currentInstallation.objectId = install.objectId;
                if (!error){
                    //ローカルに保存される端末情報を更新するため、fetchを行う
                    [currentInstallation save:&error];
                    if (error){
                        NSLog(@"CurrentInstallation can't save data:%@", error);
                    } else {
                        NSLog(@"currentInstallation:%@, insatll:%@", [NCMBInstallation currentInstallation], install);
                        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                        NSString *device_id = currentInstallation.deviceToken;
                        [ud setObject:device_id forKey:@"TOKEN"];
                        [ud synchronize];
                        [[DatabaseUtility sharedManager] tablesCreater];
                        
                    }

                } else {
                    NSLog(@"Can't get first object from Installation Class.");
                }
            } else {
                NSLog(@"installation can't save:%@", error);
            }
        }
    }];
     */
}

// PUSH時に通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    NSLog(@"push!!");
    //フラグによって処理を変える
    BOOL pushFlag = [[[NSUserDefaults standardUserDefaults] objectForKey:KEY_RECEIVE_PUSH_FLAG] boolValue];
    if (pushFlag) {
        //アプリ内に通知する
        if ([userInfo.allKeys containsObject:@"com.nifty.RichUrl"]){
            if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive){
            }
        } else if ([userInfo.allKeys containsObject:@"com.nifty.PushId"]){
            if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            }
        }
    } else {
        //アプリ内には通知しない
    }
}

@end
