//
//  SEButton.m
//  TigersApp
//
//  Created by Atsushi Kaizaki on 2015/06/05.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import "SEButton.h"
#import "SoundUtility.h"

@implementation SEButton

SoundUtility *sound ;
-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
//        [self initialize];
        sound = [[SoundUtility alloc] initWithFileName:@"tap" ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
    }
    return self;
}


-(void)sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event {
    if( event.type == UIEventTypeTouches ) {
        UITouch* anyObj = event.allTouches.anyObject;
        if( anyObj.tapCount == 1 && anyObj.phase == UITouchPhaseEnded) {
//            [[JKCSEManager sharedManager]playTapSE];
            
            [sound audioStart];
        }
    }
    [super sendAction:action to:target forEvent:event];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
