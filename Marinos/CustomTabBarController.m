#import "CustomTabBarController.h"
#import "Util.h"
#import "JKCSEManager.h"

#import "SoundUtility.h"

@interface CustomTabBarController ()

@property (nonatomic, strong) SoundUtility *tabSound;
@end

@implementation CustomTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self prepareTabController];
       // [self showCustomView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabSound = [[SoundUtility alloc] initWithFileName:SOUND_TAB_BUTTON ofType:SOUND_EXTENSION_MP3 repeat:NO delegate:nil];
    self.delegate = self;
    
    [self trainView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

//------------------------------------------------------------------------------------//
#pragma mark - CustomTabBarShowHide
//------------------------------------------------------------------------------------//
- (void) showHideTabBar:(BOOL)flag {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2f];
    
    for (UIView *view in self.view.subviews)
    {
        CGRect _rect = view.frame;
        if([view isKindOfClass:[UITabBar class]])
        {
            if (flag) {
                _rect.origin.y = self.view.frame.size.height - self.tabBar.frame.size.height;
                [view setFrame:_rect];
            } else {
                _rect.origin.y = self.view.frame.size.height;
                [view setFrame:_rect];
            }
        } else {
            if (flag) {
                _rect.size.height = WIN_SIZE.height - self.tabBar.frame.size.height;
                [view setFrame:_rect];
            } else {
                _rect.size.height = WIN_SIZE.height;
                [view setFrame:_rect];
            }
        }
    }
    [UIView commitAnimations];
}

//------------------------------------------------------------------------------------//
#pragma mark - CustomTabBarShowHide
//------------------------------------------------------------------------------------//
- (void) showHideTabBar:(BOOL)flag withDuration:(NSTimeInterval)duration{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    
    for(UIView *view in self.view.subviews)
    {
        CGRect _rect = view.frame;
        if([view isKindOfClass:[UITabBar class]])
        {
            if (flag) {
                _rect.origin.y = WIN_SIZE.height - self.tabBar.frame.size.height;
                [view setFrame:_rect];
            } else {
                _rect.origin.y = WIN_SIZE.height;
                [view setFrame:_rect];
            }
        } else {
            if (flag) {
                _rect.size.height = WIN_SIZE.height - self.tabBar.frame.size.height;
                [view setFrame:_rect];
            } else {
                _rect.size.height = WIN_SIZE.height;
                [view setFrame:_rect];
            }
        }
    }
    [UIView commitAnimations];
}

//------------------------------------------------------------------------------------//
#pragma mark - tabBarControllerDelegate
//------------------------------------------------------------------------------------//
- (void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    [self.tabSound audioStart];
    
    [[JKCSEManager sharedManager]stopBGM];
    
    
    JKCSEManager *se = [JKCSEManager sharedManager];
    switch(tabBarController.selectedIndex){
        case 0:
            [se playBGM:@"gameBGM.mp3", nil];
            break;
        default:
            [se stopBGM];
            break;
    }
    
    
    UINavigationController *navi = (UINavigationController *)viewController;
    UIViewController *selectView = [navi.viewControllers objectAtIndex:0];

    //プロトコルを実装しているかチェック
    if ([selectView conformsToProtocol:@protocol(CustomTabBarControllerDelegate)]) {
        //デリゲートメソッドの実行
        [(UINavigationController <CustomTabBarControllerDelegate> *)selectView didSelect:self];
    }
}

- (void) prepareTabController {
    NSMutableArray *viewControllers = [NSMutableArray array];

    UIStoryboard *topStoryboard = [UIStoryboard storyboardWithName:@"TopStoryboard" bundle:nil];
    UIStoryboard *mapStoryboard = [UIStoryboard storyboardWithName:@"MapStoryboard" bundle:nil];
    UIStoryboard *cameraStoryboard = [UIStoryboard storyboardWithName:@"CameraStoryboard" bundle:nil];
    UIStoryboard *cardStoryBoard = [UIStoryboard storyboardWithName:@"CardStoryboard" bundle:nil];
    UIStoryboard *albumStoryBoard = [UIStoryboard storyboardWithName:@"AlbumStoryboard" bundle:nil];
    
    //それぞれNavigationControllerにセット
    UINavigationController *topNaviController = [topStoryboard instantiateViewControllerWithIdentifier:@"TopVC"];
    topNaviController.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil
                                                                 image:[[UIImage imageNamed:@"tab_home_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                         selectedImage:[[UIImage imageNamed:@"tab_home_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    topNaviController.tabBarItem.imageInsets = UIEdgeInsetsMake(7,0,-7,0);
    [viewControllers addObject:topNaviController];
    [topNaviController setNavigationBarHidden:YES];
    
    UINavigationController *mapNaviController = [mapStoryboard instantiateViewControllerWithIdentifier:@"MapVC"];
    mapNaviController.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil
                                                                 image:[[UIImage imageNamed:@"tab_map_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                         selectedImage:[[UIImage imageNamed:@"tab_map_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    mapNaviController.tabBarItem.imageInsets = UIEdgeInsetsMake(7,0,-7,0);
    [viewControllers addObject:mapNaviController];
    [mapNaviController setNavigationBarHidden:YES];
    
    UINavigationController *cameraNaviController = [cameraStoryboard instantiateViewControllerWithIdentifier:@"CameraVC"];
    cameraNaviController.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil
                                                                 image:[[UIImage imageNamed:@"tab_camera_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                         selectedImage:[[UIImage imageNamed:@"tab_camera_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    cameraNaviController.tabBarItem.imageInsets = UIEdgeInsetsMake(7,0,-7,0);
    [viewControllers addObject:cameraNaviController];
    [cameraNaviController setNavigationBarHidden:YES];

    UINavigationController *cardNaviController = [cardStoryBoard instantiateViewControllerWithIdentifier:@"CardVC"];
    cardNaviController.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil
                                                                    image:[[UIImage imageNamed:@"tab_card_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                            selectedImage:[[UIImage imageNamed:@"tab_card_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    cardNaviController.tabBarItem.imageInsets = UIEdgeInsetsMake(7,0,-7,0);
    [viewControllers addObject:cardNaviController];
    [cardNaviController setNavigationBarHidden:YES];

    UINavigationController *albumNaviController = [albumStoryBoard instantiateViewControllerWithIdentifier:@"AlbumVC"];
    albumNaviController.tabBarItem = [[UITabBarItem alloc] initWithTitle:nil
                                                                  image:[[UIImage imageNamed:@"tab_album_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                                          selectedImage:[[UIImage imageNamed:@"tab_album_on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    albumNaviController.tabBarItem.imageInsets = UIEdgeInsetsMake(7,0,-7,0);
    [viewControllers addObject:albumNaviController];
    [albumNaviController setNavigationBarHidden:YES];
    
    
    [self.tabBar setBackgroundColor:[UIColor blackColor]];
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"tab_bg"]];
    [self setViewControllers:viewControllers];
}

-(void)trainView{
    UIWebView *trainWebView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    trainWebView.center = CGPointMake(WIN_SIZE.width/2,WIN_SIZE.height-100);
    //trainWebView.delegate = self;
    trainWebView.scrollView.bounces = NO;
//    [self.trainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://timecapsule-eagle-staging.herokuapp.com/trainsearch?latitude=%@&longtitude=%@", [NSString stringWithFormat:@"%.3f", self.latitude], [NSString stringWithFormat:@"%.3f", self.longtitude]]]]];
    
     [trainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://timecapsule-eagle-staging.herokuapp.com/trainsearch?latitude=0&longtitude=0"]]]];
   // NSLog(@"lon:%f",self.longtitude);
    [self.view addSubview:trainWebView];
}

@end
