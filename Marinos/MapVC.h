#import <UIKit/UIKit.h>
#import "MapKit/MapKit.h"
#import "FromThereViewController.h"
#import "CustomScrollView.h"
#import "CustomTabBarController.h"

typedef enum{
    MPButtonTypeMap,
    MPButtonTypePhoto,
    MPMapTagMap,
    MPMapTagLabel,
    MPPhotoMapTagMap,
    MPPhotoMapTagWebView
}myViewTag;

@interface MapVC : GAITrackedViewController <MKMapViewDelegate, UIWebViewDelegate, CustomScrollViewDelegate, UIScrollViewDelegate, UIAlertViewDelegate, CustomTabBarControllerDelegate, UITabBarControllerDelegate>


@end
