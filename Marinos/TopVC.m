//
//  TopVC.m
//  TigersApp
//
//  Created by 菊地 拓也 on 2015/04/13.
//  Copyright (c) 2015年 eagle014. All rights reserved.
//

#import "TopVC.h"
#import "DatabaseUtility.h"
#import "Toast+UIView.h"
#import "ViewManager.h"
#import "LocationUtility.h"
#import "JKCSEManager.h"

@interface TopVC ()

@property (weak, nonatomic) IBOutlet UITableView *weatherTableView;
@end

@implementation TopVC{
    NSArray *tideArray;
     NSString *result;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //日付をチェック
    [self dateCheck];
   // [self loginProcess];
    
    //位置情報が使えないときはお知らせする
    if(![LocationUtility sharedManager].isLocationEnabled){
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"notAllowToGetGPS", @"アラート：位置情報取得不許可_メッセージ")];
        if(IS_OS_8_OR_LATER){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
        
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                // 何もしない
            }]];
        
            [alertController addAction:[UIAlertAction actionWithTitle:@"設定" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                // 設定画面へのURLスキーム
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication] openURL:url];
            }]];
            
            
            id rootViewController=[UIApplication sharedApplication].delegate.window.rootViewController;
            if([rootViewController isKindOfClass:[UINavigationController class]])
            {
                rootViewController=[((UINavigationController *)rootViewController).viewControllers objectAtIndex:0];
            }
            [rootViewController presentViewController:alertController animated:YES completion:nil];
        }else{
            // アラートを表示
//            UIAlertView* alert= [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
//                                                 cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
        }
    }
    //[self tideAPI];
}

//-(void)tideAPI{
//    NSString *orign = @"http://api.ktailab.net/weather/kaiho_tide.php?lat=%d&lon=%d&apikey=000000000000000000";
//    NSString *url = [NSString stringWithFormat:orign,35,140];
//    
//    NSXMLParser* parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
//    parser.delegate = self;
//    [parser parse];
//}
//
////デリゲートメソッド(解析開始時)
//-(void) parserDidStartDocument:(NSXMLParser *)parser{
//    
//    NSLog(@"解析開始");
//    
//    //解析の初期化処理
//    // isTarget = NO;
//}
//
////デリゲートメソッド(要素の開始タグを読み込んだ時)
//- (void) parser:(NSXMLParser *)parser
//didStartElement:(NSString *)elementName
//   namespaceURI:(NSString *)namespaceURI
//  qualifiedName:(NSString *)qName
//     attributes:(NSDictionary *)attributeDict{
//    
//    NSLog(@"要素の開始タグを読み込んだ:%@",elementName);
//    
//    //    if([elementName isEqualToString:@"count"]){
//    //
//    //        isTarget = YES;     //データを取得するターゲットである事を保持する
//    //    }
//    
//}
//
////デリゲートメソッド(タグ以外のテキストを読み込んだ時)
//- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
//    
//    NSLog(@"タグ以外のテキストを読み込んだ:%@", string);
//    
//    //    if(isTarget){           //データを取得するターゲットの場合
//    //
//    result = string;   //読み込んだテキストを取得結果として保持する
//    //    }
//}
//
////デリゲートメソッド(要素の終了タグを読み込んだ時)
//- (void) parser:(NSXMLParser *)parser
//  didEndElement:(NSString *)elementName
//   namespaceURI:(NSString *)namespaceURI
//  qualifiedName:(NSString *)qName{
//    
//    NSLog(@"要素の終了タグを読み込んだ:%@",elementName);
//    
//    //isTarget = NO;
//}
//
////デリゲートメソッド(解析終了時)
//-(void) parserDidEndDocument:(NSXMLParser *)parser{
//    
//    NSLog(@"解析終了");
//    
//    [self dispResult];
//}

//結果の表示
-(void) dispResult{
    
    //NSLog(@"解析結果 countの値:%@",result);
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor lightGrayColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    WeatherCell *cell = (WeatherCell*)[tableView dequeueReusableCellWithIdentifier:@"weatherCellID" forIndexPath:indexPath];
    cell.adjustNum = (int)indexPath.row;
    [cell setWeather];
   // NSLog(@"row:%d",(int)indexPath.row);
    return cell;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[JKCSEManager sharedManager]stopBGM];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"topVC";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dateCheck{
    //初期値設定
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@"1900" forKey:KEY_DATE_YEAR];
    [dic setObject:@"01" forKey:KEY_DATE_MONTH];
    [dic setObject:@"01" forKey:KEY_DATE_DAY];
    [ud registerDefaults:dic];
    [ud synchronize];
    
    // 今日の日付を作る
    NSDate *date = [NSDate date];
    // フォーマッタにGMT+-0のTimeZoneをセットして、ユーザー設定による2重起動がないようにする。
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [df setDateFormat:@"yyyy/MM/dd"];
    NSString *dateStr = [df stringFromDate:date];
    NSArray *dateAry = [dateStr componentsSeparatedByString:@"/"];
    NSLog(@"%@",dateAry);
    [ud setObject:[dateAry objectAtIndex:dateYear] forKey:KEY_DATE_YEAR];
    [ud setObject:[dateAry objectAtIndex:dateMonth] forKey:KEY_DATE_MONTH];
    [ud setObject:[dateAry objectAtIndex:dateDay] forKey:KEY_DATE_DAY];
    [ud synchronize];
    
    //日付を表示（デバッグ）
    NSString *year = [ud objectForKey:KEY_DATE_YEAR];
    NSString *month = [ud objectForKey:KEY_DATE_MONTH];
    NSString *day = [ud objectForKey:KEY_DATE_DAY];
    NSLog(@"今日の日付は%@年%@月%@日", year, month, day);
}

#pragma mark - loginProcess
- (void)showLoginAlert{
    // TODO:ポイント加算処理&ログイン処理
    int myPoint = [[NSUserDefaults standardUserDefaults] integerForKey:KEY_MY_CARD_POINT];
    int addPoint =10;
    myPoint = myPoint + addPoint;
    [[NSUserDefaults standardUserDefaults] setInteger:myPoint forKey:KEY_MY_CARD_POINT];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//        switch (result) {
//        case NSOrderedAscending:
//            //first
//            NSLog(@"first_login");
//            [self showLoginAlert:@"ログインボーナス獲得" message:[NSString stringWithFormat:@"おめでとう！\n初回ログインボーナスで%dポイントゲット！！！\n毎日ログインしてポイントを貯めて、タイガースカードをたくさんゲットしよう！", addPoint]];
//            break;
//        case NSOrderedSame:
//            //normal
//            NSLog(@"normal_login");
//            [self showLoginAlert:@"ログインボーナス獲得" message:[NSString stringWithFormat:@"%dポイントゲット！", addPoint]];
//            break;
//        case NSOrderedDescending:
//            NSLog(@"special_login");
//            [self showLoginAlert:@"ログインボーナス獲得" message:[NSString stringWithFormat:@"スペシャルログインボーナス%dポイントゲット！！\n水曜日と土曜日がスペシャルログインボーナスの日だよ！", addPoint]];
//            break;
//        default:
//            break;
//    }
}

- (void)showLoginAlert:(NSString *)title message:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f)
    {  //iOS7未満の場合
        ((UILabel *)[[alert subviews] objectAtIndex:1]).textAlignment = NSTextAlignmentLeft;
    }
    [alert show];
    
}

- (void)ADGManagerViewControllerReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerReceiveAd");
}

- (void)ADGManagerViewControllerFailedToReceiveAd:(ADGManagerViewController *)adgManagerViewController
{
    NSLog(@"%@", @"ADGManagerViewControllerFailedToReceiveAd");
}


@end
