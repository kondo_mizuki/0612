//
//  VictoryVCViewController.h
//  Marinos
//
//  Created by eagle014 on 2013/11/25.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@class VictoryVC;

@protocol victoryViewControllerDelegate <NSObject>

-(void)victoryViewDidFinished:(VictoryVC *)sender;

@end

@interface VictoryVC : GAITrackedViewController{
    
}

-(void)changeDeviceOrientation:(id)sender;
-(void)rotateBtn:(float)degree;
-(void)rotateView:(UIView *)targetView twBtn:(UIView *)tw fbBtn:(UIView *)fb degree:(float)degree;

@property (nonatomic, assign) int myPoint;
@property (nonatomic, retain) UILabel *myPointLabel;
@property (nonatomic, strong) CAEmitterLayer *emitterLayer;//パーティクル

@property(nonatomic,weak) id <victoryViewControllerDelegate> delegate;

//-(void)dateCheck;

@end
