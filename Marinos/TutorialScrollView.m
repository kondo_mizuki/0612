//
//  TutorialScrollView.m
//  Marinos
//
//  Created by 菊地 一貴 on 2013/10/03.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import "TutorialScrollView.h"

@implementation TutorialScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpParts];
    }
    return self;
    
}



-(void)setUpParts
{
    self.contentSize = CGSizeMake(320*4,400);//(320*4,388)
    self.pagingEnabled = YES;
    //self.showsHorizontalScrollIndicator = YES;
    self.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    
    //チュート画像を４枚貼る
    for (int i = 1; i<=4; i++)
    {
        UIImageView *iv= [[UIImageView alloc] initWithFrame:CGRectMake(10+(320*(i-1)), 12, 300, 388)];
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"tutorialcard%d",i]];
        iv.image = img;
        iv.userInteractionEnabled = YES;
        [self addSubview:iv];
//        UIImageView *signaldots = [[UIImageView alloc] initWithFrame:CGRectMake(150-13.5, 378, 27, 6)];
//        img = [UIImage imageNamed:[NSString stringWithFormat:@"signaldots%d",i]];
//        signaldots.image = img;
//        [iv addSubview:signaldots];
        //×ボタンつける
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(iv.frame.size.width-25,-5,30,30);
        [closeButton setImage:[UIImage imageNamed:@"close-btn.png"] forState:UIControlStateNormal];
        [closeButton setImage:[UIImage imageNamed:@"close-btn.png"] forState:UIControlStateHighlighted];
        [closeButton addTarget:self action:@selector(closeButtonPushed:) forControlEvents:UIControlEventTouchDown];
        [iv addSubview:closeButton];

        if (i ==4)
        {
            UIImageView *buttonImageView = [[UIImageView alloc]initWithFrame:closeButton.frame];
            buttonImageView.image = [UIImage imageNamed:@"close-btn.png"];
            [iv addSubview:buttonImageView];
            [self startAnimation:buttonImageView];
            
            UIButton *moreInfoButton;
            moreInfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
            moreInfoButton.frame = CGRectMake(0,iv.frame.size.height-55,300,55);
            //moreInfoButton.frame = CGRectMake(0, WIN_SIZE.height-55, 320, 55);
            [moreInfoButton setImage:[UIImage imageNamed:@"banner"] forState:UIControlStateNormal];
            [moreInfoButton setImage:[UIImage imageNamed:@"banner"] forState:UIControlStateHighlighted];
            [moreInfoButton addTarget:self action:@selector(moreInfoButtonPushed:) forControlEvents:UIControlEventTouchDown];
            [iv addSubview:moreInfoButton];
        }
    }
}



-(void)closeButtonPushed:(id)sender
{
    //デリゲートメソッド
    [_delegate2 tutorialCloseButtonPushed:sender];
}



-(void)startAnimation:(UIImageView*)buttonImageView
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    [UIView setAnimationDelegate:buttonImageView];
    // アニメーションを開始する時間
    [UIView setAnimationDelay:0.3];
    // アニメーションを実行する時間
    [UIView setAnimationDuration:0.5];
    // アニメーションを繰り返す回数
    [UIView setAnimationRepeatCount:HUGE_VALF];
    // アニメーション完了後元に戻る
    [UIView setAnimationRepeatAutoreverses:YES];
    // 大きさを縦横n倍にする
    CGAffineTransform scale = CGAffineTransformMakeScale(1.15, 1.15);
    [buttonImageView setTransform:scale];
    // アニメーション開始
    [UIView commitAnimations];
}



#pragma mark - もっとボタン押した
-(void)moreInfoButtonPushed:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://f-marinos.qisz-labo.com/"]];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
