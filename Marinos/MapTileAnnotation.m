//
//  MapTileAnnotation.m
//  TimeCapsule
//
//  Created by kyo on 2013/04/09.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "MapTileAnnotation.h"

@implementation MapTileAnnotation

- (id) initWithCoordinate:(CLLocationCoordinate2D)c {
    self.coordinate = c;
    return self;
}

@end
