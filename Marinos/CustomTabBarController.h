//
//  CustomTabBarController.h
//  Marinos
//
//  Created by eagle014 on 2014/05/28.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomTabBarController;
@protocol CustomTabBarControllerDelegate <NSObject>

- (void) didSelect:(CustomTabBarController *)tabBarController;

@end

@interface CustomTabBarController : UITabBarController <UITabBarControllerDelegate>

- (void) showHideTabBar:(BOOL)flag;
- (void) showHideTabBar:(BOOL)flag withDuration:(NSTimeInterval)duration;

- (void) prepareTabController;
@end
