//
//  HTTPViewController.h
//  Marinos
//
//  Created by eagle014 on 2014/02/15.
//  Copyright (c) 2014年 井上　貴文. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileDownloadObject.h"

@class HTTPViewController;

@protocol HTTPViewControllerDelegatre <NSObject>

- (void)HTTPDidFinished:(HTTPViewController *)sender;

@end

@interface HTTPViewController : UIViewController <FileDownloadObjectDelegate>

@property (nonatomic, strong) NSData *receivedData;
@property (nonatomic, strong) NSURLConnection *myConnect;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, weak) id <HTTPViewControllerDelegatre> delegate;

- (void)checkUserID:(NSString *)user_ID;
- (void)postXML:(NSString *)xmlStr;
- (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title;
//- (void)checkUserVersion:(NSString *)version;

@end
