#import <Foundation/Foundation.h>
#import "User.h"

@interface ViewManager : NSObject
{
    
}

+ (ViewManager *) sharedManager;

- (void) setCommonBg:(UIView*)view;
- (void) removeCommonBg;

- (void) setHeader:(UIView*)view;
- (void) removeHeader;
- (void) setHeaderWithRetutn:(UIView*)view target:(id)target action:(NSString*)selector;

- (void) setWebViewWithBackButton:(UIView*)view urlString:(NSString*) urlString;
- (void) closeButtonCallBack:(UIButton*)button;

@end
