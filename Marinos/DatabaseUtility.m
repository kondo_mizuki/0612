//
//  DatabaseUtility.m
//  BigbrotherMap
//
//  Created by kyo on 2013/07/18.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "DatabaseUtility.h"
#import "Util.h"
#import "FMDatabase.h"

@interface DatabaseUtility ()

@property(nonatomic, strong) FMDatabase *db;
@end

@implementation DatabaseUtility

static DatabaseUtility *sharedUtil = nil;

#pragma mark - singleton method
+ (DatabaseUtility*)sharedManager
{
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [[self alloc] init];
        }
    }
    return sharedUtil;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedUtil == nil) {
            sharedUtil = [super allocWithZone:zone];
            return sharedUtil;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone
{
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

#pragma mark - データベース操作のためのopen,closeメソッド
- (BOOL)openDatabase
{
    //DBファイルへのパスを取得
    //パスは~/Documents/配下に格納される。
    NSString *dbPath = nil;
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //取得データ数を確認
    if ([documentsPath count] >= 1) {
        //固定で0番目を取得でOK
        dbPath = [documentsPath objectAtIndex:0];
        //パスの最後にファイル名をアペンドし、DBファイルへのフルパスを生成。
        dbPath = [dbPath stringByAppendingPathComponent:DB_FILE];
    } else {
        //error
        NSLog(@"search Document path error. database file open error.");
        return false;
    }
    
    //DBファイルがDocument配下に存在するか判定
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dbPath]) {
        //存在しない
        //デフォルトのDBファイルをコピー(初回のみ)
        //ファイルはアプリケーションディレクトリ配下に格納されている。
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *orgPath = [bundle bundlePath];
        //初期ファイルのパス。(~/XXX.app/sample.db)
        orgPath = [orgPath stringByAppendingPathComponent:DB_FILE];
        
        //デフォルトのDBファイルをDocument配下へコピー
        if (![fileManager copyItemAtPath:orgPath toPath:dbPath error:nil]) {
            //error
            NSLog(@"db file copy error. : %@ to %@.", orgPath, dbPath);
            return false;
        }
    }
    
    //open database with FMDB.
    self.db = [FMDatabase databaseWithPath:dbPath];
    return [self.db open];
}

- (void)closeDatabase
{
    if (self.db) {
        [self.db close];
    }
}

#pragma mark - データベース操作
- (BOOL) tablesCreater
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"CREATE TABLE IF NOT EXISTS check_in(id INTEGER PRIMARY KEY AUTOINCREMENT, place_id INTEGER, latitude REAL, longitude REAL, time_table REAL)"];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
    } else {
        NSLog(@"Can't open Database");
    }
    return NO;
}

- (NSMutableArray*) showTables {
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM sqlite_master WHERE type='table'"];
        while ([rs next]) {
            [array addObject:[rs stringForColumn:@"name"]];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getPhotos
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *identification = [rs stringForColumn:@"id"];
            if (identification != nil) {
                [dic setObject:identification forKey:@"identification"];
            }
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longitude = [rs stringForColumn:@"longitude"];
            if (longitude != nil) {
                [dic setObject:longitude forKey:@"longitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (UIImage*)getPhotoFromIdentification:(NSString*)identification
{
    if ([self openDatabase]) {
        FMResultSet *rs = [self.db executeQuery:@"SELECT thumbnail FROM historys WHERE id=?", identification];
        UIImage *thumbnail = nil;
        while ([rs next]) {
            thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
        }
        [rs close];
        [self closeDatabase];
        return thumbnail;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (BOOL)saveHistory:(UIImage*)thumbnail url:(NSString*)url recordDate:(NSString*)recordDate latitude:(double)latitude longtitude:(double)longtitude title:(NSString*)title detail:(NSString*)detail
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        //トランザクション開始(exclusive)
        [self.db beginTransaction];
        
        //ステートメントの再利用フラグ
        //おそらくループ内で同一クエリの更新処理を行う場合バインドクエリの準備を何回
        //も実行してしまうのためこのフラグを設定する。
        //このフラグが設定されているとステートメントが再利用される。
        [self.db setShouldCacheStatements:YES];
        
        //insertクエリ実行(プリミティブ型は使えない)
        //    [_db executeUpdate:@"insert into example values (?, ?, ?, ?)",
        //                                1, 2, @"test", 4.1];
        // executeUpdateWithFormatメソッドで可能。
        
        NSData *imagedata = [[NSData alloc] initWithData:UIImageJPEGRepresentation(thumbnail, 0.2f)];
        
        NSDate *now = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *nowStr = [formatter stringFromDate:now];
        
        [self.db executeUpdate:@"INSERT INTO historys(thumbnail, url, record_date, latitude, longtitude, title, body, datetime, like_count, tweet_count) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", imagedata, url, recordDate, [NSNumber numberWithDouble:latitude], [NSNumber numberWithDouble:longtitude], title, detail, nowStr, [NSNumber numberWithInt:INITIAL_LIKE_COUNT], [NSNumber numberWithInt:INITIAL_TWEET_COUNT]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        //commit
        [self.db commit];
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (NSMutableArray*)getAlbumCells
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT title, thumbnail, record_date, url, latitude, longtitude FROM historys ORDER BY id DESC"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *title = [rs stringForColumn:@"title"];
            if (title != nil) {
                [dic setObject:title forKey:@"title"];
            }
            
            UIImage *thumbnail = [[UIImage alloc] initWithData:[rs dataForColumn:@"thumbnail"]];
            if (thumbnail != nil) {
                [dic setObject:thumbnail forKey:@"thumbnail"];
            }
            
            NSString *url = [rs stringForColumn:@"url"];
            if (url != nil) {
                [dic setObject:url forKey:@"url"];
            }
            
            NSString *record_date = [rs stringForColumn:@"record_date"];
            if (record_date != nil) {
                [dic setObject:record_date forKey:@"record_date"];
            }
            
            NSString *latitude = [rs stringForColumn:@"latitude"];
            if (latitude != nil) {
                [dic setObject:latitude forKey:@"latitude"];
            }
            
            NSString *longitude = [rs stringForColumn:@"longitude"];
            if (longitude != nil) {
                [dic setObject:longitude forKey:@"longitude"];
            }
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray*)getPlaceCells
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT place, address, detail, place_category_id, point FROM place"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *place = [rs stringForColumn:@"place"];
            if (place != nil) {
                [dic setObject:place forKey:@"place"];
            }
            
            NSString *address = [rs stringForColumn:@"address"];
            if (address != nil) {
                [dic setObject:address forKey:@"address"];
            }
            
            NSString *detail = [rs stringForColumn:@"detail"];
            if (detail != nil) {
                [dic setObject:detail forKey:@"detail"];
            }
            
            int place_category = [rs intForColumn:@"place_category_id"];
            [dic setObject:[NSNumber numberWithInt:place_category] forKey:@"place_category_id"];
            
            int point = [rs intForColumn:@"point"];
            [dic setObject:[NSNumber numberWithInt:point] forKey:@"point"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
/*
 - (NSMutableArray*)getLocationInCategory:(AnnotationCategory)category
 {
 if ([self openDatabase]) {
 NSMutableArray *array = [NSMutableArray array];
 
 NSString *locationCategory;
 switch (category) {
 case AnnotationCategorySights:
 {
 locationCategory = @"sights";
 break;
 }
 case AnnotationCategoryFood:
 {
 locationCategory = @"food";
 break;
 }
 case AnnotationCategoryShop:
 {
 locationCategory = @"shop";
 break;
 }
 case AnnotationCategoryHotel:
 {
 locationCategory = @"hotel";
 break;
 }
 
 default:
 break;
 }
 
 FMResultSet *rs = [self.db executeQuery:@"SELECT latitude, longtitude, radius FROM place WHERE category=?", locationCategory];
 
 while ([rs next]) {
 NSMutableDictionary *dic = [NSMutableDictionary dictionary];
 
 double latitude = [rs doubleForColumn:@"latitude"];
 [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
 
 double longtitude = [rs doubleForColumn:@"longtitude"];
 [dic setObject:[NSNumber numberWithDouble:longtitude] forKey:@"longtitude"];
 
 double radius = [rs doubleForColumn:@"radius"];
 [dic setObject:[NSNumber numberWithDouble:radius] forKey:@"radius"];
 
 [array addObject:dic];
 }
 [rs close];
 [self closeDatabase];
 return array;
 } else {
 NSLog(@"DBが開けなかった");
 }
 return nil;
 }
 */

//⬇すべてのデータを取ってくる。
- (NSMutableArray*)getAllLocation
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        
        //        FMResultSet *rs = [self.db executeQuery:@"SELECT id, place, address, place_category, latitude, longtitude, radius, point, check_in_count, sp_check_in_count FROM place"];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM card"];
        
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int identifier = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:identifier] forKey:@"identifier"];
            
            NSString *place = [rs stringForColumn:@"name"];
            if (place != nil) {
                [dic setObject:place forKey:@"name"];
            }
            
            NSString *address = [rs stringForColumn:@"address"];
            if (address != nil) {
                [dic setObject:address forKey:@"address"];
            }
            
            NSString *detail = [rs stringForColumn:@"detail"];
            if (detail != nil) {
                [dic setObject:detail forKey:@"detail"];
            }
            
            int place_category = [rs intForColumn:@"place_category_id"];
            [dic setObject:[NSNumber numberWithInt:place_category] forKey:@"place_category_id"];
            
            
            double latitude = [rs doubleForColumn:@"n"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longitude = [rs doubleForColumn:@"e"];
            [dic setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            
            double radius = [rs doubleForColumn:@"r"];
            [dic setObject:[NSNumber numberWithDouble:radius] forKey:@"radius"];
            
            int point = [rs intForColumn:@"point"];
            [dic setObject:[NSNumber numberWithInt:point] forKey:@"point"];
            
            int check_in_count = [rs intForColumn:@"check_in_count"];
            [dic setObject:[NSNumber numberWithInt:check_in_count] forKey:@"check_in_count"];
            
            int sp_check_in_count = [rs intForColumn:@"sp_check_in_count"];
            [dic setObject:[NSNumber numberWithInt:sp_check_in_count] forKey:@"sp_check_in_count"];
            
    
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableDictionary*)getLocation:(int)identifier
{
    if ([self openDatabase]) {
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM place WHERE id=?", [NSNumber numberWithInt:identifier]];
        
        while ([rs next]) {
            NSString *place = [rs stringForColumn:@"place"];
            if (place != nil) {
                [dic setObject:place forKey:@"place"];
            }
            
            NSString *address = [rs stringForColumn:@"address"];
            if (address != nil) {
                [dic setObject:address forKey:@"address"];
            }
            
            int place_category = [rs intForColumn:@"place_category_id"];
            [dic setObject:[NSNumber numberWithInt:place_category] forKey:@"place_category_id"];
            
            
            double latitude = [rs doubleForColumn:@"latitude"];
            [dic setObject:[NSNumber numberWithDouble:latitude] forKey:@"latitude"];
            
            double longitude = [rs doubleForColumn:@"longitude"];
            [dic setObject:[NSNumber numberWithDouble:longitude] forKey:@"longitude"];
            
            double radius = [rs doubleForColumn:@"radius"];
            [dic setObject:[NSNumber numberWithDouble:radius] forKey:@"radius"];
            
            int point = [rs intForColumn:@"point"];
            [dic setObject:[NSNumber numberWithInt:point] forKey:@"point"];
            
            NSString *last_check_in_date = [rs stringForColumn:@"last_check_in_date"];
            if (last_check_in_date != nil) {
                [dic setObject:last_check_in_date forKey:@"last_check_in_date"];
            }
            
            NSString *last_sp_check_in_date = [rs stringForColumn:@"last_sp_check_in_date"];
            if (last_sp_check_in_date != nil) {
                [dic setObject:last_sp_check_in_date forKey:@"last_sp_check_in_date"];
            }
            
            
            int check_in_count = [rs intForColumn:@"check_in_count"];
            [dic setObject:[NSNumber numberWithInt:check_in_count] forKey:@"check_in_count"];
            
            int sp_check_in_count = [rs intForColumn:@"sp_check_in_count"];
            [dic setObject:[NSNumber numberWithInt:sp_check_in_count] forKey:@"sp_check_in_count"];
        }
        [rs close];
        [self closeDatabase];
        
        return dic;
    } else {
        
        NSLog(@"DBが開けなかった");
    }
    
    return nil;
}

- (NSMutableArray*)getSPCheckIn:(int)identifier {
    
    if ([self openDatabase]) {
        
        /*
         NSMutableArray *array = [NSMutableArray array];
         FMResultSet *rs = [self.db executeQuery:@"SELECT card_id, rare_level_id FROM card WHERE invalid_flag <> 1;"];
         while ([rs next]) {
         // Get the column data for this record and put it into a custom Record object
         NSMutableDictionary *dic = [NSMutableDictionary dictionary];
         
         int card_id = [rs intForColumn:@"card_id"];
         [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
         
         int rare_level_id = [rs intForColumn:@"rare_level_id"];
         [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
         
         [array addObject:dic];
         }
         [rs close];
         [self closeDatabase];
         return array;
         
         */
        NSMutableArray *array = [NSMutableArray array];
        
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM sp_place WHERE place_id=?", [NSNumber numberWithInt:identifier]];
        
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int placeID = [rs intForColumn:@"place_id"];
            [dic setObject:[NSNumber numberWithInt:placeID] forKey:@"place_id"];
            
            NSString *date = [rs stringForColumn:@"sp_check_in_date"];
            if (date != nil) {
                [dic setObject:date forKey:@"sp_check_in_date"];
            }
            
            int cardID = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:cardID] forKey:@"card_id"];
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        
        return array;
    } else
    {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}



- (BOOL)updateLastCheckIn:(NSString*)now withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        //        [self.db executeUpdate:@"UPDATE place SET last_check_in_date=? WHERE id=?", now, [NSNumber numberWithInt:identifier]];
        //        [self.db executeUpdate:@"UPDATE place SET check_in_count=check_in_count+1 WHERE id=?",[NSNumber numberWithInt:identifier]];
        [self.db executeUpdate:@"UPDATE place SET last_check_in_date=?, check_in_count=check_in_count+1 WHERE id=?", now,[NSNumber numberWithInt:identifier]];
        
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)updateLastSPCheckIn:(NSString*)now withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        //        [self.db executeUpdate:@"UPDATE place SET last_sp_check_in_date=? WHERE id=?", now, [NSNumber numberWithInt:identifier]];
        //        [self.db executeUpdate:@"UPDATE place SET sp_check_in_count=sp_check_in_count+1 WHERE id=?",[NSNumber numberWithInt:identifier]];
        [self.db executeUpdate:@"UPDATE place SET last_sp_check_in_date=?, sp_check_in_count=sp_check_in_count+1 WHERE id=?", now,[NSNumber numberWithInt:identifier]];
        
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

/*うえと併せた
 - (BOOL)updateForCheckin:(int)cnt withID:(int)identifier
 {
 if ([self openDatabase]) {
 BOOL result = TRUE;
 
 [self.db executeUpdate:@"UPDATE place SET check_in_count=? WHERE id=?", cnt, [NSNumber numberWithInt:identifier]];
 
 //check
 if ([self.db hadError]) {
 result = FALSE;
 NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
 }
 
 return result;
 
 } else {
 NSLog(@"DBが開けなかった");
 }
 return NO;
 }
 
 - (BOOL)updateForSPCheckin:(int)cnt withID:(int)identifier
 {
 if ([self openDatabase]) {
 BOOL result = TRUE;
 
 [self.db executeUpdate:@"UPDATE place SET sp_check_in_count=? WHERE id=?", cnt, [NSNumber numberWithInt:identifier]];
 
 //check
 if ([self.db hadError]) {
 result = FALSE;
 NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
 }
 
 return result;
 
 } else {
 NSLog(@"DBが開けなかった");
 }
 return NO;
 }
 */



- (BOOL)updateLastNotification:(NSString*)now withID:(int)identifier
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE place SET last_notification_date=? WHERE id=?", now, [NSNumber numberWithInt:identifier]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}

- (BOOL)updateGetCard:(int)cardID
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE card SET possession=possession+1 WHERE card_id=?", [NSNumber numberWithInt:cardID]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}
/*
- (BOOL)updateGetCard:(int)cardID year:(int)year
{
    if ([self openDatabase]) {
        BOOL result = TRUE;
        
        [self.db executeUpdate:@"UPDATE card SET possession=possession+1 WHERE card_id=? AND card_year=?", [NSNumber numberWithInt:cardID],[NSNumber numberWithInt:year]];
        
        //check
        if ([self.db hadError]) {
            result = FALSE;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return NO;
}
*/


- (int)getAlbumCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM historys"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getCardCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}
/*
- (int)getCardCount:(int)year
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card WHERE card_year = ?",[NSNumber numberWithInt:year]];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}
*/
/*
- (NSMutableArray *)getCardData:(int)year
{
    if ([self openDatabase]) {
        NSMutableArray *ary = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, card_id, rare_level_id, char_category_id, card_year, file_name, card_text_sns, card_text_1, card_text_2, possession FROM card WHERE card_year=?;", [NSNumber numberWithInt:year]];
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            // Get the column data for this record and put it into a custom Record object
            int primary_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:primary_id] forKey:@"id"];
            
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int rare_level_id = [rs intForColumn:@"rare_level_id"];
            [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
            
            int char_category_id = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:char_category_id] forKey:@"char_category_id"];
            
            int card_year = [rs intForColumn:@"card_year"];
            [dic setObject:[NSNumber numberWithInt:card_year] forKey:@"card_year"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *card_text_sns = [rs stringForColumn:@"card_text_sns"];
            if (card_text_sns != nil) {
                [dic setObject:card_text_sns forKey:@"card_text_sns"];
            }
            
            NSString *card_text1 = [rs stringForColumn:@"card_text_1"];
            if (card_text1 != nil) {
                [dic setObject:card_text1 forKey:@"card_text_1"];
            }
            
            NSString *card_text2 = [rs stringForColumn:@"card_text_2"];
            if (card_text2 != nil) {
                [dic setObject:card_text2 forKey:@"card_text_2"];
            }
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
            
            [ary addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return ary;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
*/
/*
- (NSMutableArray *)getCardDataForAlbumPossession:(NSString *)album_yaer_name{
    if ([self openDatabase]) {
        NSMutableArray *ary = [NSMutableArray array];
        NSString *sql = @"SELECT id, card_id, card_year, char_category_id, possession, card_text_1 FROM card WHERE card_year=? ORDER BY card_id ASC";
        FMResultSet *rs = [self.db executeQuery:sql, [NSNumber numberWithInt:[album_yaer_name intValue]]];
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            // Get the column data for this record and put it into a custom Record object
            int primary_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:primary_id] forKey:@"id"];
            
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int card_year = [rs intForColumn:@"card_year"];
            [dic setObject:[NSNumber numberWithInt:card_year] forKey:@"card_year"];
            
            int card_category = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:card_category] forKey:@"card_category"];
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
            
            NSString *cardText = [rs stringForColumn:@"card_text_1"];
            [dic setObject:cardText forKey:@"card_text_1"];
            
            [ary addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return ary;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
*/

 - (NSMutableArray *)getCardData
{
    if ([self openDatabase]) {
        NSMutableArray *ary = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, card_id, rare_level_id, char_category_id, file_name, card_text_sns, card_text_1, card_text_2, possession FROM card;"];
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            // Get the column data for this record and put it into a custom Record object
            int primary_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:primary_id] forKey:@"id"];
            
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int rare_level_id = [rs intForColumn:@"rare_level_id"];
            [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
            
            int char_category_id = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:char_category_id] forKey:@"char_category_id"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *card_text_sns = [rs stringForColumn:@"card_text_sns"];
            if (card_text_sns != nil) {
                [dic setObject:card_text_sns forKey:@"card_text_sns"];
            }
            
            NSString *card_text1 = [rs stringForColumn:@"card_text_1"];
            if (card_text1 != nil) {
                [dic setObject:card_text1 forKey:@"card_text_1"];
            }
            
            NSString *card_text2 = [rs stringForColumn:@"card_text_2"];
            if (card_text2 != nil) {
                [dic setObject:card_text2 forKey:@"card_text_2"];
            }
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
            
            [ary addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return ary;
    } else {
        NSLog(@"DBが開けなかった");
    }

    return nil;
}

 - (NSMutableArray *)getCardDataForAlbumPossession
{
    if ([self openDatabase]) {
        NSMutableArray *ary = [NSMutableArray array];
        NSString *sql = @"SELECT id, card_id, char_category_id, possession, card_text_1 FROM card ORDER BY card_id ASC";
        FMResultSet *rs = [self.db executeQuery:sql];
        while ([rs next]) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            // Get the column data for this record and put it into a custom Record object
            int primary_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:primary_id] forKey:@"id"];
            
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int card_category = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:card_category] forKey:@"card_category"];
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
            
            NSString *cardText = [rs stringForColumn:@"card_text_1"];
            [dic setObject:cardText forKey:@"card_text_1"];
            
            [ary addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return ary;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
 
- (int)getGettedCardCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card WHERE possession > 0"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}
/*
- (int)getGettedCardCount:(int)year
{
    if ([self openDatabase]) {
 
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT COUNT(*) AS cnt FROM card WHERE possession > 0 AND card_year = ?",[NSNumber numberWithInt:year]];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}
*/

- (int)getGettedAllCardCount
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT SUM(possession) AS cnt FROM card"];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

/*
- (int)getGettedAllCardCount:(int)year
{
    if ([self openDatabase]) {
        
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT SUM(possession) AS cnt FROM card WHERE card_year = ?",[NSNumber numberWithInt:year]];
        while ([rs next]) {
            //            //ここでデータを展開
            cnt = [rs intForColumn:@"cnt"];
        }
        [rs close];
        [self closeDatabase];
        return cnt;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}
*/

- (int)getGettedCardPossession:(int)cardID
{
    if ([self openDatabase]) {
        
        int pos = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT possession FROM card WHERE card_id=?", [NSNumber numberWithInt:cardID]];
        while ([rs next]) {
            //            //ここでデータを展開
            pos = [rs intForColumn:@"possession"];
        }
        [rs close];
        [self closeDatabase];
        return pos;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}
/*
- (int)getGettedCardPossession:(int)cardID year:(int)year
{
    if ([self openDatabase]) {
        
        int pos = 0;
        FMResultSet *rs = [self.db executeQuery:@"SELECT possession FROM card WHERE card_id=? AND card_year=?", [NSNumber numberWithInt:cardID], [NSNumber numberWithInt:year]];
        while ([rs next]) {
            //            //ここでデータを展開
            pos = [rs intForColumn:@"possession"];
        }
        [rs close];
        [self closeDatabase];
        return pos;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return 0;
}

- (int)getAlbumPossession:(NSString *)album_year_name{
    static int pos = 0;
    if ([self openDatabase]){
        FMResultSet *rs = [self.db executeQuery:@"SELECT possession FROM card WHERE card_year= ?", [NSNumber numberWithInt:[album_year_name intValue]]];
        while ([rs next]) {
            //            //ここでデータを展開
            pos = [rs intForColumn:@"possession"];
        }
        [rs close];
        [self closeDatabase];
        return pos;
    }else{
        NSLog(@"DBが開けなかった");
    }
    return pos;
}
 */
- (NSMutableDictionary*)getSelectedCard:(int)cardID
{
    if ([self openDatabase]) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        FMResultSet *rs = [self.db executeQuery:@"SELECT card_id, rare_level_id, char_category_id, card_year, file_name, card_text_sns, card_text_1, card_text_2, possession FROM card WHERE card_id=?", [NSNumber numberWithInt:cardID]];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int rare_level_id = [rs intForColumn:@"rare_level_id"];
            [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
            
            int char_category_id = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:char_category_id] forKey:@"char_category_id"];
            
            int card_year = [rs intForColumn:@"card_year"];
            [dic setObject:[NSNumber numberWithInt:card_year] forKey:@"card_year"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *card_text_sns = [rs stringForColumn:@"card_text_sns"];
            if (card_text_sns != nil) {
                [dic setObject:card_text_sns forKey:@"card_text_sns"];
            }
            
            NSString *card_text1 = [rs stringForColumn:@"card_text_1"];
            if (card_text1 != nil) {
                [dic setObject:card_text1 forKey:@"card_text_1"];
            }
            
            NSString *card_text2 = [rs stringForColumn:@"card_text_2"];
            if (card_text2 != nil) {
                [dic setObject:card_text2 forKey:@"card_text_2"];
            }
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
        }
        [rs close];
        [self closeDatabase];
        return dic;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
/*
- (NSMutableDictionary*)getSelectedCard:(int)cardID year:(int)year
{
    if ([self openDatabase]) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        FMResultSet *rs = [self.db executeQuery:@"SELECT card_id, rare_level_id, char_category_id, card_year, file_name, card_text_sns, card_text_1, card_text_2, possession, ad_title, ad_text FROM card WHERE card_id=? AND card_year=?", [NSNumber numberWithInt:cardID],[NSNumber numberWithInt:year]];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int rare_level_id = [rs intForColumn:@"rare_level_id"];
            [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
            
            int char_category_id = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:char_category_id] forKey:@"char_category_id"];
            
            int card_year = [rs intForColumn:@"card_year"];
            [dic setObject:[NSNumber numberWithInt:card_year] forKey:@"card_year"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *card_text_sns = [rs stringForColumn:@"card_text_sns"];
            if (card_text_sns != nil) {
                [dic setObject:card_text_sns forKey:@"card_text_sns"];
            }
            
            NSString *card_text1 = [rs stringForColumn:@"card_text_1"];
            if (card_text1 != nil) {
                [dic setObject:card_text1 forKey:@"card_text_1"];
            }
            
            NSString *card_text2 = [rs stringForColumn:@"card_text_2"];
            if (card_text2 != nil) {
                [dic setObject:card_text2 forKey:@"card_text_2"];
            }
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
            
            NSString *ad_title = [rs stringForColumn:@"ad_title"];
            if (ad_title != nil) {
                [dic setObject:ad_title forKey:@"ad_title"];
            }
            
            NSString *ad_text = [rs stringForColumn:@"ad_text"];
            if (ad_text != nil) {
                [dic setObject:ad_text forKey:@"ad_text"];
            }
        }
        [rs close];
        [self closeDatabase];
        return dic;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}
*/

- (NSMutableDictionary *)getSelectCardPrimaryID:(int)primary_ID{
    if ([self openDatabase]) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        FMResultSet *rs = [self.db executeQuery:@"SELECT card_id, rare_level_id, char_category_id, card_year, file_name, card_text_sns, card_text_1, card_text_2, possession FROM card WHERE id=?", [NSNumber numberWithInt:primary_ID]];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int rare_level_id = [rs intForColumn:@"rare_level_id"];
            [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
            
            int char_category_id = [rs intForColumn:@"char_category_id"];
            [dic setObject:[NSNumber numberWithInt:char_category_id] forKey:@"char_category_id"];
            
            int card_year = [rs intForColumn:@"card_year"];
            [dic setObject:[NSNumber numberWithInt:card_year] forKey:@"card_year"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            if (file_name != nil) {
                [dic setObject:file_name forKey:@"file_name"];
            }
            
            NSString *card_text_sns = [rs stringForColumn:@"card_text_sns"];
            if (card_text_sns != nil) {
                [dic setObject:card_text_sns forKey:@"card_text_sns"];
            }
            
            NSString *card_text1 = [rs stringForColumn:@"card_text_1"];
            if (card_text1 != nil) {
                [dic setObject:card_text1 forKey:@"card_text_1"];
            }
            
            NSString *card_text2 = [rs stringForColumn:@"card_text_2"];
            if (card_text2 != nil) {
                [dic setObject:card_text2 forKey:@"card_text_2"];
            }
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
        }
        [rs close];
        [self closeDatabase];
        return dic;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray *)getAllCard
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, card_id, rare_level_id, possession ,card_year FROM card WHERE invalid_flag <> 1;"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int primary_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:primary_id] forKey:@"id"];
            
            int card_id = [rs intForColumn:@"card_id"];
            [dic setObject:[NSNumber numberWithInt:card_id] forKey:@"card_id"];
            
            int rare_level_id = [rs intForColumn:@"rare_level_id"];
            [dic setObject:[NSNumber numberWithInt:rare_level_id] forKey:@"rare_level_id"];
            
            int possession = [rs intForColumn:@"possession"];
            [dic setObject:[NSNumber numberWithInt:possession] forKey:@"possession"];
            
            int card_year = [rs intForColumn:@"card_year"];
            [dic setObject:[NSNumber numberWithInt:card_year] forKey:@"card_year"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray *)getAllFileNameOfCardTable
{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT id, file_name FROM card"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            int primary_id = [rs intForColumn:@"id"];
            [dic setObject:[NSNumber numberWithInt:primary_id] forKey:@"id"];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            [dic setObject:file_name forKey:@"file_name"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSMutableArray *) getAllFileNameAndMD5{
    if ([self openDatabase]) {
        NSMutableArray *array = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM card_image_withMD5_table"];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            
            NSString *file_name = [rs stringForColumn:@"file_name"];
            [dic setObject:file_name forKey:@"file_name"];
            
            NSString *MD5_str = [rs stringForColumn:@"MD5"];
            [dic setObject:MD5_str forKey:@"MD5"];
            
            [array addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return array;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (BOOL)checkMD5Data:(NSString *)file_name MD5:(NSString *)MD5_str{
    static BOOL result = NO;
    if ([self openDatabase]) {
        NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) AS cnt FROM card_image_withMD5_table WHERE file_name = '%@' AND MD5 = '%@';", file_name, MD5_str];//@"SELECT COUNT(*) AS cnt FROM ? WHERE id = '?'";//
        //NSString *sql = @"SELECT COUNT(*) AS cnt FROM card_image_withMD5_table WHERE file_name = ?, MD5 = ?";
        NSLog(@"SQL:%@", sql);
        //[self.db executeQuery:sql];
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:sql];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            cnt = [rs intForColumn:@"cnt"];
            NSLog(@"cnt:::::::::::%d",cnt);
            result = [[NSNumber numberWithInt:cnt] boolValue];
        }
        [rs close];
        //check
        if ([self.db hadError]) {
            result = NO;
            NSLog(@"MD5:::::Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        [self closeDatabase];
    } else {
        NSLog(@"DBが開けなかった");
    }
    return result;
}

#pragma mark - insertProcess
- (void)insertCardTable:(NSMutableArray *)cardDicAry{
    /*    "card_category_id" = 1;
     "card_id" = 54;
     "card_number" = 54;
     "card_rarelevel_id" = 1;
     "card_text_1" = "MF 8 \U4e2d\U753a\U516c\U7950";
     "card_text_2" = "2013 CC";
     "card_text_sns" = "\U4e2d\U753a\U516c\U7950 2013 C";
     "card_year" = 2013;
     "file_name" = "away_mf_nakamachi.jpg";
     "invalid_flag" = 0;
     */
    if ([self openDatabase]) {
        [self.db beginTransaction];
        NSDictionary *cardDic = [NSDictionary dictionary];
        BOOL isSucceeded = YES;
        for (cardDic in cardDicAry) {
            NSNumber *card_id = [cardDic objectForKey:@"card_id"];
            NSNumber *card_number = [cardDic objectForKey:@"card_number"];
            NSNumber *card_rarelevel_id = [cardDic objectForKey:@"card_rarelevel_id"];
            NSNumber *card_category_id = [cardDic objectForKey:@"card_category_id"];
            NSNumber *card_year = [cardDic objectForKey:@"card_year"];
            NSString *file_name = [cardDic objectForKey:@"file_name"];
            NSString *card_text_sns = [cardDic objectForKey:@"card_text_sns"];
            NSString *card_text_1 = [cardDic objectForKey:@"card_text_1"];
            NSString *card_text_2 = [cardDic objectForKey:@"card_text_2"];
            NSNumber *possession = [NSNumber numberWithInt:0];
            NSString *invalid_flag = [cardDic objectForKey:@"invalid_flag"];
            NSString *ad_title = [cardDic objectForKey:@"ad_title"];
            NSString *ad_text = [cardDic objectForKey:@"ad_text"];
            
            NSString *sql = @"INSERT INTO card VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, card_id, card_number, card_rarelevel_id, card_category_id, card_year, file_name, card_text_sns, card_text_1, card_text_2, possession, invalid_flag, ad_title, ad_text]) {
                isSucceeded = NO;
                break;
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        //card_id, rare_level_id, possession ,card_year FROM card WHERE invalid_flag <> 1;
        /*FMResultSet *rs = [self.db executeQuery:@"INSERT INTO card VALUES('?','?','?','?','?','?','?','?','?','?','?');", [NSNumber numberWithInt:card_id], [NSNumber numberWithInt:card_number], [NSNumber numberWithInt:card_rarelevel_id], [NSNumber numberWithInt:card_category_id], [NSNumber numberWithInt:card_year], file_name, card_text_sns, card_text_1, card_text_2, [NSNumber numberWithInt:possession], [NSNumber numberWithInt:invalid_flag]];
         
         */
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}

- (void)insertCardCategory:(NSMutableArray *)cardCategoryDicAry{
    /*
     "card_category_id" = 2;
     "card_category_name" = VictoryA;
     */
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *cardCategoryDic = [NSDictionary dictionary];
        for (cardCategoryDic in cardCategoryDicAry) {
            NSNumber *card_category_id = [cardCategoryDic objectForKey:@"card_category_id"];
            NSString *card_category_name = [cardCategoryDic objectForKey:@"card_category_name"];
            NSString *sql = @"INSERT INTO char_category VALUES(?,?);";
            NSLog(@"SQL:%@",sql);
            if (![self.db executeUpdate:sql, card_category_id, card_category_name]) {
                isSucceeded = NO;
                break;
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}

- (void)insertPlaceTable:(NSMutableArray *)placeDicAry{
    /*
     "place_id" = 3;
     "place_name" = "\U30e6\U30a2\U30c6\U30c3\U30af\U30b9\U30bf\U30b8\U30a2\U30e0\U4ed9\U53f0";
     address = "\U5bae\U57ce\U770c\U4ed9\U53f0\U5e02\U6cc9\U533a\U4e03\U5317\U7530\U5b57\U67f378";
     "place_detail" = "\U30d9\U30ac\U30eb\U30bf\U4ed9\U53f0\U306e\U30db\U30fc\U30e0\U30b9\U30bf\U30b8\U30a2\U30e0A.";
     "place_category_id" = 2;
     latitude = "38.3191";
     longitude = "140.8807";
     radius = 200;
     point = 10;
     以下向こうから来ないデータ
     check_in_count = 0;
     sp_check_in_count = 0;
     last_check_in_date = @"";
     last_sp_check_in_date = @"";
     last_notification_date = @"";
     多分以下いらない
     "end_datetime" = "2020-01-01 00:00:59";
     "end_unixtime" = 1577804459;
     "ins_datetime" = "0000-00-00 00:00:00";
     "ins_unixtime" = 1392092012;
     "sp_checkin_cnt" = 0;
     "start_datetime" = "2014-01-01 00:00:00";
     "start_unixtime" = 1388502000;
     timestamp = "2014-02-13 20:55:17";
     */
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *placeDic = [NSDictionary dictionary];
        for (placeDic in placeDicAry) {
            NSNumber *place_id = [placeDic objectForKey:@"place_id"];
            NSString *place_name = [placeDic objectForKey:@"place_name"];
            NSString *address = [placeDic objectForKey:@"address"];
            NSString *place_detail = [placeDic objectForKey:@"place_detail"];
            NSNumber *place_category_id = [placeDic objectForKey:@"place_category_id"];
            NSNumber *latitude = [placeDic objectForKey:@"latitude"];
            NSNumber *longitude = [placeDic objectForKey:@"longitude"];
            NSNumber *radius = [placeDic objectForKey:@"radius"];
            NSNumber *point = [placeDic objectForKey:@"point"];
            NSNumber *check_in_count = [NSNumber numberWithInt:0];
            NSNumber *sp_check_in_count = [placeDic objectForKey:@"sp_checkin_cnt"];
            NSString *last_check_in_date = @"";
            NSString *last_sp_check_in_date = @"";
            NSString *last_notification_date = @"";
            NSString *ad_text = [placeDic objectForKey:@"ad_text"];
            
            NSString *sql = @"INSERT INTO place VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, place_id, place_name, address, place_detail, place_category_id, latitude, longitude, radius, point, check_in_count, sp_check_in_count, last_check_in_date, last_sp_check_in_date, last_notification_date, ad_text]){
                isSucceeded = NO;
                break;
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}


- (void)insertPlaceCategory:(NSMutableArray *)placeCategoryDicAry{
    /*
     "place_category_id" = 2;
     "place_category_name" = "\U30a2\U30a6\U30a7\U30a4\U30b9\U30bf\U30b8\U30a2\U30e0";
     "reg_time" = "2014-02-11 12:39:52";
     */
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *placeCategoryDic = [NSDictionary dictionary];
        for (placeCategoryDic in placeCategoryDicAry) {
            //int place_category_id = [[placeCategoryDic objectForKey:@"place_category_id"] intValue];
            NSNumber *place_category_id = [placeCategoryDic objectForKey:@"place_category_id"];
            NSString *place_category_name = [placeCategoryDic objectForKey:@"place_category_name"];
            NSString *sql = @"INSERT INTO place_category VALUES(?,?);";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, place_category_id, place_category_name]) {
                isSucceeded = NO;
                break;
            }
            //[self.db executeUpdate:sql, place_category_id, place_category_name];
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
    
}

- (void)insertSpecialPlace:(NSMutableArray *)specialPlaceDicAry{
    /*
     "place_special_id" = 13;
     "place_id" = 5;
     // TODO:先方に形式確認
     "game_date" = "2014-02-18";
     "card_id" = 1;
     */
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *specialPlaceDic = [NSDictionary dictionary];
        for (specialPlaceDic in specialPlaceDicAry) {
            NSNumber *place_special_id = [specialPlaceDic objectForKey:@"place_special_id"];
            NSNumber *place_id = [specialPlaceDic objectForKey:@"place_id"];
            NSString *game_date = [specialPlaceDic objectForKey:@"game_date"];
            NSNumber *card_id = [specialPlaceDic objectForKey:@"card_id"];
            NSString *sql = @"INSERT INTO sp_place VALUES(?,?,?,?)";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, place_special_id, place_id, game_date, card_id]) {
                isSucceeded = NO;
                break;
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}

- (void)insertRareLevelTable:(NSMutableArray *)rareLevelDicAry {
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *rareLevelDic = [NSDictionary dictionary];
        for (rareLevelDic in rareLevelDicAry) {
            NSNumber *rare_level_id = [rareLevelDic objectForKey:@"card_rarelevel_id"];
            NSString *rare_level_name = [rareLevelDic objectForKey:@"card_rarelevel_name"];
            NSString *sql = @"INSERT INTO rare_level VALUES(?,?)";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, rare_level_id, rare_level_name]) {
                isSucceeded = NO;
                break;
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}



- (void)updateCardTable:(NSMutableArray *)cardDicAry{
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *cardDic = [NSDictionary dictionary];
        for (cardDic in cardDicAry) {
            NSNumber *card_id = [cardDic objectForKey:@"card_id"];
            NSNumber *card_number = [cardDic objectForKey:@"card_number"];
            NSNumber *card_rarelevel_id = [cardDic objectForKey:@"card_rarelevel_id"];
            NSNumber *card_category_id = [cardDic objectForKey:@"card_category_id"];
            NSNumber *card_year = [cardDic objectForKey:@"card_year"];
            NSString *file_name = [cardDic objectForKey:@"file_name"];
            NSString *card_text_sns = [cardDic objectForKey:@"card_text_sns"];
            NSString *card_text_1 = [cardDic objectForKey:@"card_text_1"];
            NSString *card_text_2 = [cardDic objectForKey:@"card_text_2"];
            NSString *ad_title = [cardDic objectForKey:@"ad_title"];
            NSString *ad_text = [cardDic objectForKey:@"ad_text"];
            NSString *invalid_flag = [cardDic objectForKey:@"invalid_flag"];
            NSString *sql = @"UPDATE card SET card_id = ?, rare_level_id = ?, char_category_id = ?, card_year = ?, file_name = ?, card_text_sns = ?, card_text_1 = ?, card_text_2 = ?, invalid_flag = ? , ad_title = ?, ad_text = ? WHERE id = ?";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, card_number, card_rarelevel_id, card_category_id, card_year, file_name, card_text_sns, card_text_1, card_text_2, invalid_flag, ad_title, ad_text, card_id]) {
                isSucceeded = NO;
                break;
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}

- (void)updatePlaceTable:(NSMutableArray *)placeDicAry{
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *placeDic = [NSDictionary dictionary];
        for (placeDic in placeDicAry) {
            NSNumber *place_id = [placeDic objectForKey:@"place_id"];
            NSString *place_name = [placeDic objectForKey:@"place_name"];
            NSString *address = [placeDic objectForKey:@"address"];
            NSString *place_detail = [placeDic objectForKey:@"place_detail"];
            NSNumber *place_category_id = [placeDic objectForKey:@"place_category_id"];
            NSNumber *latitude = [placeDic objectForKey:@"latitude"];
            NSNumber *longitude = [placeDic objectForKey:@"longitude"];
            NSNumber *radius = [placeDic objectForKey:@"radius"];
            NSNumber *point = [placeDic objectForKey:@"point"];
            NSNumber *check_in_count = [NSNumber numberWithInt:0];
            NSNumber *sp_check_in_count = [NSNumber numberWithInt:0];
            NSString *last_check_in_date = @"";
            NSString *last_sp_check_in_date = @"";
            NSString *last_notification_date = @"";
            NSString *ad_text = [placeDic objectForKey:@"ad_text"];
            NSString *sql = @"UPDATE place SET place = ?, address = ?, detail = ?, place_category_id = ?, latitude = ?, longitude = ?, radius = ?, point = ?, check_in_count = ?, sp_check_in_count = ?, last_check_in_date = ?, last_sp_check_in_date = ?, last_notification_date = ?, ad_text = ? WHERE id = ?";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, place_name, address, place_detail, place_category_id, latitude, longitude, radius, point, check_in_count, sp_check_in_count, last_check_in_date, last_sp_check_in_date, last_notification_date, ad_text, place_id]) {
                isSucceeded = NO;
                break;
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}
- (void)updateCardCategory:(NSMutableArray *)cardCategoryDicAry{
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *cardCategoryDic = [NSDictionary dictionary];
        for (cardCategoryDic in cardCategoryDicAry) {
            NSNumber *card_category_id = [cardCategoryDic objectForKey:@"card_category_id"];
            NSString *card_category_name = [cardCategoryDic objectForKey:@"card_category_name"];
            NSString *sql = @"UPDATE char_category SET char_category_name = ? WHERE id = ?";
            NSLog(@"SQL:%@",sql);
            if (![self.db executeUpdate:sql, card_category_name, card_category_id]) {
                isSucceeded = NO;
                break;
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
    
}
- (void)updatePlaceCategory:(NSMutableArray *)placeCategoryDicAry{
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *placeCategoryDic = [NSDictionary dictionary];
        for (placeCategoryDic in placeCategoryDicAry) {
            NSNumber *place_category_id = [placeCategoryDic objectForKey:@"place_category_id"];
            NSString *place_category_name = [placeCategoryDic objectForKey:@"place_category_name"];
            NSString *sql = @"UPDATE place_category SET place_category_name = ? WHERE id= ?";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, place_category_name, place_category_id]) {
                isSucceeded = NO;
                break;
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}
- (void)updateSpecialPlace:(NSMutableArray *)specialPlaceDicAry{
    
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *specialPlaceDic = [NSDictionary dictionary];
        for (specialPlaceDic in specialPlaceDicAry) {
            NSNumber *place_special_id = [specialPlaceDic objectForKey:@"place_special_id"];
            NSNumber *place_id = [specialPlaceDic objectForKey:@"place_id"];
            NSString *game_date = [specialPlaceDic objectForKey:@"game_date"];
            NSNumber *card_id = [specialPlaceDic objectForKey:@"card_id"];
            NSString *sql = @"UPDATE sp_place SET place_id = ?, sp_check_in_date = ?, card_id = ? WHERE id = ?";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, place_id, game_date, card_id, place_special_id]) {
                isSucceeded = NO;
                break;
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}

- (void) updateRareLevelTable:(NSMutableArray *)rareLevelDicAry {
    if ([self openDatabase]) {
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSDictionary *rareLevelDic = [NSDictionary dictionary];
        for (rareLevelDic in rareLevelDicAry) {
            NSNumber *rare_level_id = [rareLevelDic objectForKey:@"card_rarelevel_id"];
            NSString *rare_level_name = [rareLevelDic objectForKey:@"card_rarelevel_name"];
            NSString *sql = @"UPDATE rare_level SET rare_level_name = ? WHERE id = ?";
            NSLog(@"SQL:%@", sql);
            if (![self.db executeUpdate:sql, rare_level_name, rare_level_id]) {
                isSucceeded = NO;
                break;
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
            }
            //check
            if ([self.db hadError]) {
                NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
            }
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
}
- (BOOL)checkTableData:(NSString *)table_name tableID:(NSNumber *)primary_id{
    static BOOL result = NO;
    if ([self openDatabase]) {
        NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) AS cnt FROM %@ WHERE id = '%d';", table_name, [primary_id intValue]];//@"SELECT COUNT(*) AS cnt FROM ? WHERE id = '?'";//
        //NSString *sql = @"SELECT COUNT(*) AS cnt FROM ? WHERE id = ?";
        NSLog(@"SQL:%@", sql);
        //[self.db executeQuery:sql];
        int cnt = 0;
        FMResultSet *rs = [self.db executeQuery:sql];
        while ([rs next]) {
            // Get the column data for this record and put it into a custom Record object
            cnt = [rs intForColumn:@"cnt"];
            NSLog(@"%d",cnt);
            result = [[NSNumber numberWithInt:cnt] boolValue];
        }
        [rs close];
        //check
        if ([self.db hadError]) {
            result = NO;
            NSLog(@"check::::Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        [self closeDatabase];
    } else {
        NSLog(@"DBが開けなかった");
    }
    return result;
}
- (void)deleteTable:(NSString *)table_name serialID:(NSString *)serial_num{
    int serial_id = [serial_num intValue];
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE id = '%d';", table_name, serial_id];
    NSLog(@"SQL:%@", sql);
    if ([self openDatabase]) {
        [self.db executeUpdate:sql];
        //check
        if ([self.db hadError]) {
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
    
}
- (void)updateProcess
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    [defaults setObject:@"1.0.0" forKey:@"oldVersion"];
    [ud registerDefaults:defaults];
    
    // ホームディレクトリを取得
    NSString *path = [[NSBundle mainBundle] pathForResource:@"update" ofType:@"plist"];
    
    // 読み込みたいplistのパスを作成
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:path];
    
    //********************************************************************************
#pragma mark デバッグ用にチェックポイント１を会社に変更　本番はまとめてコメントアウト！！
    
    if ([self openDatabase]) {
        NSLog(@"デバッグ用にupdate!!");
        NSString *sql=@"UPDATE card SET n=35.416549,e=136.760198,r=50000 WHERE id=1;";
        [self.db executeUpdate:sql];
        
        //check
        if ([self.db hadError]) {
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        [self closeDatabase];
    } else {
        
        NSLog(@"DBが開けなかった");
    }
    
    //********************************************************************************

    
    for (NSMutableDictionary *dict in array) {
        
        // アプリバージョン情報
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        NSString *oldVersion = [ud stringForKey:@"oldVersion"];
        
        // plist内のバージョン情報
        NSString *update = [dict objectForKey:@"version"];
        NSLog(@"%@", update);
        
        if ([version compare:update options:NSNumericSearch] >= NSOrderedSame && [oldVersion compare:update options:NSNumericSearch] == NSOrderedAscending) {
            NSLog(@"%@バージョンアップ！", update);
            [ud setObject:update forKey:@"oldVersion"];
            [ud synchronize];
            
            if ([self openDatabase]) {
                for (NSString *sql in [dict objectForKey:@"sql"]) {
                    [self.db executeUpdate:sql];
                }
                //check
                if ([self.db hadError]) {
                    NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
                }
                [self closeDatabase];
            } else {
                
                NSLog(@"DBが開けなかった");
            }
        } else {
            
            NSLog(@"バージョンアップしない");
        }
    }
}

//-----------------------------------------------------------------//


- (NSArray*)getPlayerList {
 
    if ([self openDatabase]) {
        NSMutableArray *retArray = [NSMutableArray arrayWithCapacity:0];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM game_player WHERE retire_date IS NULL"];
        while ([rs next]) {
            //ここでデータを展開
            NSString *str = [rs stringForColumn:@"TITLE"];
            
            
            if(str.length==0)str=@"";
            NSLog(@"str%@",str);
            
            NSDictionary *dic = @{ @"PLAYER_ID" : [rs stringForColumn:@"player_id"],
                                   @"NAME" : [rs stringForColumn:@"name"],
                                   @"POSITION" : [rs stringForColumn:@"position"],
                                   @"TITLE" : str};
            

            [retArray addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return [retArray copy];
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSArray*)getRetirementPlayerList {
    
    if ([self openDatabase]) {
        NSMutableArray *retArray = [NSMutableArray arrayWithCapacity:0];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM game_player WHERE retire_date IS NOT NULL"];
        while ([rs next]) {
            //ここでデータを展開
            NSString *str = [rs stringForColumn:@"TITLE"];
            
            
            if(str.length==0)str=@"";
            NSLog(@"str%@",str);
            
            NSDictionary *dic = @{ @"PLAYER_ID" : [rs stringForColumn:@"player_id"],
                                   @"NAME" : [rs stringForColumn:@"name"],
                                   @"POSITION" : [rs stringForColumn:@"position"],
                                   @"TITLE" : str};
            
            [retArray addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return [retArray copy];
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}


#pragma mark アイテム装備
- (NSArray *)getMyItems {
    
    if ([self openDatabase]) {
        NSMutableArray *retArray = [NSMutableArray array];
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM game_myItem"];
        while ([rs next]) {
            //ここでデータを展開
            NSDictionary *dic = @{ @"ID" : [rs stringForColumn:@"id"],
                                   @"POSSESSION" : [rs stringForColumn:@"possession"]};
            
            [retArray addObject:dic];
        }
        [rs close];
        [self closeDatabase];
        return [retArray copy];
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}

- (NSDictionary *)getMyItemForId:(int)itemId{
    if ([self openDatabase]) {
        NSDictionary *dic;
        FMResultSet *rs = [self.db executeQuery:@"SELECT * FROM game_myItem where id = ?",@(itemId)];
        while ([rs next]) {
            //ここでデータを展開
            dic = @{ @"ID" : [rs stringForColumn:@"id"],
                                   @"POSSESSION" : [rs stringForColumn:@"possession"]};
            
            break;
        }
        [rs close];
        [self closeDatabase];
        return dic;
    } else {
        NSLog(@"DBが開けなかった");
    }
    return nil;
}


- (int)addMyItemForId:(int)itemId count:(int)cnt{
    
    NSDictionary *dic = [self getMyItemForId:itemId];
    
    if ([self openDatabase]) {
        int result = [dic[@"POSSESSION"] intValue] + cnt;
        [self.db executeUpdate:@"UPDATE game_myItem SET possession=? WHERE id=?", @(result),@(itemId)] ;
        
        
        //check
        if ([self.db hadError]) {
            result = -2;
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        
        return result;
        
    } else {
        NSLog(@"DBが開けなかった");
    }
    return -1;


}


- (void)setCareerData:(NSDictionary*)dic identifier:(NSString*)identifier {
    
    if ([self openDatabase]) {
        
        [self.db beginTransaction];
        BOOL isSucceeded = YES;
        NSNumber *count = [dic objectForKey:@"COUNT_KEY"];
        NSString *key = [dic objectForKey:@"CAREER_KEY"];
        
        if ([key isEqual:@"STUDENT"]) {
            NSString *sql = @"INSERT INTO game_career(identifier,count,student) VALUES(?,?,?)";
            if (![self.db executeUpdate:sql, identifier, count, [dic objectForKey:@"CONTENT"]]) {
                isSucceeded = NO;
            }
        } else if ([key isEqual:@"DRAFT"]) {
            NSString *sql = @"INSERT INTO game_career(identifier,count,draft) VALUES(?,?,?)";
            if (![self.db executeUpdate:sql, identifier, count, [dic objectForKey:@"CONTENT"]]) {
                isSucceeded = NO;
            }
        } else if ([key isEqual:@"PRO"]) {
            NSString *sql = @"INSERT INTO game_career(identifier,count,runnual_salary) VALUES(?,?,?)";
            if (![self.db executeUpdate:sql, identifier, count, [dic objectForKey:@"CONTENT"]]) {
                isSucceeded = NO;
            }
        }
        
        //check
        if ([self.db hadError]) {
            NSLog(@"Err %d: %@", [self.db lastErrorCode], [self.db lastErrorMessage]);
        }
        if (isSucceeded) {
            [self.db commit];
        } else {
            [self.db rollback];
        }
        [self closeDatabase];
    } else {
        NSLog(@"DBが開けなかった");
    }}

@end
