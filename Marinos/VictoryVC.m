//
//  VictoryVCViewController.m
//  Marinos
//
//  Created by eagle014 on 2013/11/25.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#define TAB_SPACE 60
#define PLAY_POINT 10
#define VICTORY_CARD_NUM 10

#import "VictoryVC.h"
#import "TabButton.h"
#import "Util.h"
#import "DatabaseUtility.h"

@interface VictoryVC (){
    UIButton *dissmissBtn;
    UIButton *cardGetBtn;
    UIButton  *twitterIcon;
    UIButton  *facebookIcon;
    
    UIImageView *cardBG;
    UIImageView *getLogoImageView;
    UIImageView *newLogoImageView;
    int cardLevel;
    int dayPoint;
    
    NSString *selectedCardIDString;
    NSString *selectedCardCharaName;
    BOOL isNewCard;

    CGFloat adjust;
}

@end

@implementation VictoryVC
@synthesize myPoint;
//------------------------------------------------------------------------------------//
#pragma mark - LifeCycle
//------------------------------------------------------------------------------------//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"Victory!!");
    [self setup];
    //load_dayPoint
    NSUserDefaults *udef = [NSUserDefaults standardUserDefaults];
    dayPoint = [udef integerForKey:@"KEY_DAYPOINT"];
    
    if (WIN_SIZE.height == 568) {
        adjust = 44;
    }else{
        adjust = 0;
    }
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }

}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//------------------------------------------------------------------------------------//
#pragma mark - rotate
//------------------------------------------------------------------------------------//
-(void)changeDeviceOrientation:(id)sender{
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationPortrait:
            //[self rotateBtn:0.0f];
            [self resetAffine];
            break;
        case UIDeviceOrientationLandscapeLeft:
            [self rotateBtn:90.0f];
            break;
            /*
             case UIDeviceOrientationPortraitUpsideDown:
             [self rotateBtn:180.0f];
             break;
             
             case UIDeviceOrientationLandscapeRight:
             [self rotateBtn:-90.0f];
             break;
             */
        default:
            break;
    }
}

-(void)rotateBtn:(float)degree{
    //[self rotateView:dissmissBtn degree:degree];
    [self rotateView:dissmissBtn twBtn:twitterIcon fbBtn:facebookIcon degree:degree];
}

-(void)resetAffine{
    dissmissBtn.transform = CGAffineTransformIdentity;
    twitterIcon.transform = CGAffineTransformIdentity;
    facebookIcon.transform = CGAffineTransformIdentity;
    getLogoImageView.transform = CGAffineTransformIdentity;
}
-(void)rotateView:(UIView *)targetView twBtn:(UIView *)tw fbBtn:(UIView *)fb degree:(float)degree{
    CGFloat angle = degree * M_PI / 180.0;
    CGAffineTransform t1 = CGAffineTransformMakeRotation(angle);
    CGAffineTransform t2 = CGAffineTransformMakeTranslation(-130, -190-adjust);
    targetView.transform = CGAffineTransformConcat(t1, t2);
    
    //targetView.transform = CGAffineTransformMakeRotation(angle);
    CGAffineTransform t3 = CGAffineTransformMakeTranslation(0, 25+adjust);
    tw.transform = CGAffineTransformConcat(t1, t3);
    fb.transform = CGAffineTransformConcat(t1, t3);
    
    CGAffineTransform t4 = CGAffineTransformMakeTranslation(-65, -70-adjust);

    getLogoImageView.transform = CGAffineTransformConcat(t1,t4);
    
    [newLogoImageView setHidden:YES];
    [_emitterLayer removeFromSuperlayer];
}

//------------------------------------------------------------------------------------//
#pragma mark - delegate
//------------------------------------------------------------------------------------//
-(void)goBackCardVC:(id)sender{
    //サウンド再生
    [[Util sharedManager] playSound:SOUND_GET_BUTTON];

    [self.delegate victoryViewDidFinished:self];
    
}

/*
//------------------------------------------------------------------------------------//
#pragma mark - 日付のチェック
//------------------------------------------------------------------------------------//
-(void)dateCheck{
    // 今日の日付を作る
    NSDate *date = [NSDate date];
    // フォーマッタにGMT+-0のTimeZoneをセットして、ユーザー設定による2重起動がないようにする。
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [df setDateFormat:@"yyyy/MM/dd"];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    // キーがなければアラート起動してキーを登録
    if (![ud boolForKey:[df stringFromDate:date]]) {
        // value:今日のアラート起動有無　key:今日の日付
        NSLog(@"アラート起動処理");
        [ud setObject:[NSNumber numberWithBool:YES] forKey:[df stringFromDate:date]];
        //.plistの書き込み（Library/Preferencesに保存される）
        [ud synchronize];
    }
}
 */

//------------------------------------------------------------------------------------//
#pragma mark - DataBaseError
//------------------------------------------------------------------------------------//
-(void)dbLoadError
{
    UIAlertView *alert =[[UIAlertView alloc] init];
    alert.title = @"原因不明のエラー";
    alert.message = @"カード情報の取得に失敗しました。\nエラーが繰り返し発生する場合、アプリの再起動および再インストールをお試しください。\n\n上記対応でも問題を解決できない場合、恐れいりますが開発会社にお問い合わせくださいませ。\n申し訳ありません。";
    //[alert setDelegate:self];
    [alert addButtonWithTitle:@"OK"];
    ((UILabel *)[[alert subviews] objectAtIndex:1]).textAlignment = NSTextAlignmentLeft;
    [alert show];
    //[alert setDelegate:nil];
}

//------------------------------------------------------------------------------------//
#pragma mark - GetButton Process
//------------------------------------------------------------------------------------//
-(void)getButtonPushed:(UIButton *)sender
{
    //まず非表示にする
    cardGetBtn.hidden = YES;
    
    //サウンド再生
    [[Util sharedManager] playSound:SOUND_VICTORY_BUTTON];
    
    //BOOL getNewCardFlag = YES;//一枚も持っていないカードをひくかどうか
    //int allCardCount = [[DatabaseUtility sharedManager] getGettedAllCardCount];
    NSMutableArray *allCardArray = [[DatabaseUtility sharedManager] getAllCard];
    if (allCardArray == nil || [allCardArray count] ==0)
    {
        //TODO:DB開けなかったらエラーだから、ポイントを引かないしエラー処理も書いておく。
        //エラーアラート
        [self dbLoadError];
        //tb.hidden = NO;
        return;
    }

    NSMutableArray *drawAry = [NSMutableArray array];//引く為のアレイ
    NSMutableArray *lotteryArray = [NSMutableArray arrayWithCapacity:0];//カテゴリが合致したカードのIDのみを格納するアレイ
    //ここでカードタイプ決定
    //cardLevel = CARD_LEVEL_NORMAL;
    cardLevel = CARD_LEVEL_VICTORY;
    
    //この先はカードDBがあるときのみ行う処理。
    //選択されたカテゴリのカードIDのみをアレイに格納(getAllCardメソッドで作成したmutablearrayから取り出す)
    for (int i = 0; i<[allCardArray count]; i++)
    {//カードDBから１つずつレコード（dic）を取り出して、選択されたカテゴリのカードのみ別アレイに格納。
        NSMutableDictionary *dic = allCardArray[i];
        int rare_level = [dic[@"rare_level_id"] intValue];
        //レアレベルが同じだった場合のみ行う処理
        if (rare_level == cardLevel)
        {
            int pos = [dic[@"possession"] intValue];
            if (pos == 0) {
                int cardID = [dic[@"card_id"] intValue];
                [drawAry addObject:[NSNumber numberWithInt:cardID]];
            }else if (pos > 0){
                int cardID = [dic[@"card_id"] intValue];
                [lotteryArray addObject:[NSNumber numberWithInt:cardID]];
            }
        }
    }
    //int roopOutCount = 0;
    int randomX = 1;//いろいろ使い回すランダムint
     /*
    do {
        roopOutCount ++;
        //NSLog(@"ループ%d回目",roopOutCount);
    
     
        if (roopOutCount >= VICTORY_CARD_NUM*3 )//または１０回以上ループしたら面倒なので、
        {//保有枚数ごとの低確率にひっかかったら、すでに保有しているカードを引くことになる。
            getNewCardFlag = NO;
            
        }
     

        if (allCardArray == nil || [allCardArray count] ==0)
        {
            //TODO:DB開けなかったらエラーだから、ポイントを引かないしエラー処理も書いておく。
            //エラーアラート
            [self dbLoadError];
            //tb.hidden = NO;
            return;
        }

        //この先はカードDBがあるときのみ行う処理。
        //選択されたカテゴリのカードIDのみをアレイに格納(getAllCardメソッドで作成したmutablearrayから取り出す)
        for (int i = 0; i<[allCardArray count]; i++)
        {
            //カードDBから１つずつレコード（dic）を取り出して、選択されたカテゴリのカードのみ別アレイに格納。
            NSMutableDictionary *dic = allCardArray[i];
            int rare_level = [dic[@"rare_level_id"] intValue];
            //レアレベルが同じだった場合のみ行う処理
            if (rare_level == cardLevel)
            {
                int possession = [dic[@"possession"] intValue];
                //ゲットするのは保有しているほうかしていないほうか
                if (getNewCardFlag == NO)
                {//新しいカードを引けないので保有枚数が１枚以上のカードのみ格納
                    if (possession > 0)
                    {
                        int cardID = [dic[@"card_id"] intValue];
                        [lotteryArray addObject:[NSNumber numberWithInt:cardID]];
                    }
                }
                else
                {//新しいカードを取得できる
                    if (possession == 0)
                    {
                        int cardID = [dic[@"card_id"] intValue];
                        [lotteryArray addObject:[NSNumber numberWithInt:cardID]];
                    }
                }
            }
        }
    }
    while ([lotteryArray count] <=0);
    */
    //ランダムでアレイの中身を取り出す（これが獲得したカードになる）
    int selectedCardID = 1;//とりあえず１を初期値。
    
    if (drawAry != nil && [drawAry count] > 0) {
        randomX = arc4random_uniform([drawAry count]);
        selectedCardID = [[drawAry objectAtIndex:randomX] intValue];
    }else{
        if (lotteryArray != nil && [lotteryArray count] >0)
        {
            randomX = arc4random_uniform([lotteryArray count]);
            selectedCardID = [lotteryArray[randomX] intValue];
        }
    }
    /*
    if (lotteryArray != nil && [lotteryArray count] >0)
    {
        randomX = arc4random_uniform([lotteryArray count]);
        selectedCardID = [lotteryArray[randomX] intValue];
    }
     */
    NSLog(@"カードIDは%d",selectedCardID);
#warning カードIDを強制的に書き換え！
    //selectedCardID = 6;
    //獲得したカード情報を、カードIDをもとにカードDBから取得（すでにアレイにある？）
    //カードの一番目なら確実に空じゃないだろう⬇
    int currentYear = 0;
    NSMutableDictionary *damyDic = allCardArray[0];
    currentYear = [damyDic[@"card_year"] intValue];

    NSMutableDictionary *selectedCardDic = [[DatabaseUtility sharedManager] getSelectedCard:selectedCardID];//なんかエラー出てもとりあえず1番返そうw
    int possession = [selectedCardDic[@"possession"] integerValue];
    isNewCard = NO;
    if (possession <=0)
    {//保有枚数が０枚以下なら
        isNewCard = YES;
    }
    NSString *file_name = selectedCardDic[@"file_name"];
    if (file_name == nil)
    {
        NSLog(@"ファイル名が空っぽって噂");
    }
    //SNS連携の文言に使う文字列を格納
    selectedCardIDString = [NSString stringWithFormat:@"No.%03d",selectedCardID];
    selectedCardCharaName = selectedCardDic[@"card_text_sns"];
    
    //獲得したカードのカードDB上の保有枚数を＋１更新する。
    //BOOL upDateGetCard = [[DatabaseUtility sharedManager] updateGetCard:selectedCardID];
    BOOL upDateGetCard = [[DatabaseUtility sharedManager] updateGetCard:selectedCardID year:currentYear];

    if (upDateGetCard == FALSE)
    {
        NSLog(@"保有枚数保存できませんでした！ガチャ");
    }
    else if (upDateGetCard == TRUE)
    {
        NSLog(@"保有枚数の保存に成功！");
        //保有に成功したらポイント減算とsave
        dayPoint--;
        //save_dayPoint
        NSUserDefaults *udef = [NSUserDefaults standardUserDefaults];
        [udef setInteger:dayPoint forKey:@"KEY_DAYPOINT"];
        [udef synchronize];
    }
    
    //ここからアニメーションの準備
    UIImageView *cardImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:file_name]];
    cardImage.frame = CGRectMake(0, 0, 320, 480);
    
    cardImage.backgroundColor = [UIColor whiteColor];
    cardImage.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    cardImage.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2 - 30);
    [self.view addSubview:cardImage];
    [self cardAppearAnime:cardImage btnType:sender cardID:selectedCardID];
}

#pragma mark - ゴールからカードが近づくアニメーション
-(void)cardAppearAnime:(UIImageView*)card btnType:(UIButton *)sender cardID:(int)card_id
{
    //サウンド再生
    [[Util sharedManager] playSound:SOUND_VICTORY_APPER];
    
    [UIView animateWithDuration:1.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         card.center = CGPointMake(card.center.x, card.center.y - 400);
                         //card.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                         CGFloat angle = 120 * M_PI / 180.0f;
                         CGAffineTransform t1 = CGAffineTransformMakeRotation(angle);
                         card.transform = CGAffineTransformConcat(t1, t1);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:1.5f
                                               delay:0.0f
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              CGFloat angle = 240 * M_PI / 180.0f;
                                              CGAffineTransform t1 = CGAffineTransformMakeRotation(angle);
                                              card.transform = CGAffineTransformConcat(t1, t1);
                                              card.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2 );//-30
                                          }
                          
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:1.5f
                                                                    delay:0.1f
                                                                  options:UIViewAnimationOptionCurveLinear
                                                               animations:^{
                                                                   CGFloat angle = -120 * M_PI / 180.0f;
                                                                   CGAffineTransform t1 = CGAffineTransformMakeRotation(angle);
                                                                   card.transform = CGAffineTransformConcat(t1, t1);

                                                                   card.transform = CGAffineTransformMakeScale(1.5f, 1.5f);
                                                                   card.alpha = 0.5f;
                                                               }
                                                               completion:^(BOOL finished) {
                                                                   
                                                                   [UIView animateWithDuration:1.5f
                                                                                         delay:0.1f
                                                                                       options:UIViewAnimationOptionCurveLinear
                                                                                    animations:^{
                                                                                        card.transform = CGAffineTransformMakeScale(1.5f, 1.5f);
                                                                                        card.alpha = 0.5f;
                                                                                    }
                                                                                    completion:^(BOOL finished) {
                                                                                        [self cardGetAnime:card btnType:sender cardID:card_id];}];
                                                               }];
                                          }];
                     }];
}



//------------------------------------------------------------------------------------//
#pragma mark - カード出現アニメーション
//------------------------------------------------------------------------------------//
-(void)cardGetAnime:(UIImageView*)card btnType:(UIButton *)sender cardID:(int)card_id
{
    
    //パーティクルを表示
    [self getParticle];
    
    cardBG.alpha = 1.0f;
    card.transform = CGAffineTransformMakeScale(0.7, 0.7);
    card.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2-58);
    card.alpha = 0.0f;
    //ロゴ差し込み
    getLogoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 296, 62)];
    getLogoImageView.center = CGPointMake(card.center.x+1000, card.center.y+150);
    getLogoImageView.image = [UIImage imageNamed:@"victory-text.png"];//good-text.png
    [self.view addSubview:getLogoImageView];
    
    newLogoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 296, 62)];
    newLogoImageView.center = CGPointMake(80, 30);
    newLogoImageView.image = [UIImage imageNamed:@"new-text.png"];
    newLogoImageView.alpha = 0.0;
    newLogoImageView.transform = CGAffineTransformMakeScale(1.3, 1.3);
    [self.view addSubview:newLogoImageView];
    
    [self setSocialButton];
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         card.alpha = 1.0f;
                         getLogoImageView.center = CGPointMake(WIN_SIZE.width/2, getLogoImageView.center.y);
                         if (isNewCard)
                         {
                             newLogoImageView.alpha = 1.0;
                             newLogoImageView.transform = CGAffineTransformIdentity;
                         }
                         [self moveSocialButton];
                     }
                     completion:^(BOOL finished) {
                         //サウンド再生
                         [[Util sharedManager] playSound:SOUND_VICTORY_FINISH];
                         [[Util sharedManager] playSound:SOUND_GET_TEXT];
                         UIImage *ss = [self screenCapture];
                         UIImageView *ssIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 64, 96)];
                         ssIV.center = CGPointMake(WIN_SIZE.width-65,WIN_SIZE.height-100);
                         if (WIN_SIZE.height <= 480)
                         {
                             ssIV.center = CGPointMake(WIN_SIZE.width-37,WIN_SIZE.height-100);
                         }
                         ssIV.image = ss;
                         ssIV.transform = CGAffineTransformMakeRotation(M_PI * 17 / 180.0);
                         
                         [cardBG addSubview:ssIV];
                         //ゲットアニメーションが終わったらボタン登場
                         //すぐ⬇
                         [self performSelector:@selector(showDissmissBtn:) withObject:[NSNumber numberWithInt:card_id] afterDelay:1.0];
                        
                     }];
}

-(void)showDissmissBtn:(id)sender{
     dissmissBtn.hidden = NO;
#warning message - if this logic is disenable then comment out!!
    /*
    if ([sender intValue] == 39) {
        //通知センターにデバイス回転のオブザーバーとして登録する
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(changeDeviceOrientation:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }*/

}
//------------------------------------------------------------------------------------//
#pragma mark - ソーシャルボタンセット&ムーブ
//------------------------------------------------------------------------------------//
- (void)setSocialButton
{
    twitterIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    twitterIcon.frame = CGRectMake(0, 0, 45, 45);
    twitterIcon.center = CGPointMake(-2060, WIN_SIZE.height-100);
    [twitterIcon setImage:[UIImage imageNamed:@"twitter-btn.png"] forState:UIControlStateNormal];
    [twitterIcon setImage:[UIImage imageNamed:@"twitter-btn.png"] forState:UIControlStateHighlighted];
    twitterIcon.tag = TWITTER_BTN_TAG;
    [twitterIcon addTarget:self action:@selector(SLComposeViewControllerButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:twitterIcon];
    
    facebookIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookIcon.frame = CGRectMake(0, 0, 45, 45);
    facebookIcon.center = CGPointMake(-2000, WIN_SIZE.height-100);
    [facebookIcon setImage:[UIImage imageNamed:@"facebook-btn.png"] forState:UIControlStateNormal];
    [facebookIcon setImage:[UIImage imageNamed:@"facebook-btn.png"] forState:UIControlStateHighlighted];
    facebookIcon.tag = FACEBOOK_BTN_TAG;
    [facebookIcon addTarget:self action:@selector(SLComposeViewControllerButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:facebookIcon];
    
    dissmissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dissmissBtn.frame = CGRectMake(0, 0, 127.0f, 52.0f);
    dissmissBtn.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height - 50);
    [dissmissBtn setImage:[UIImage imageNamed:@"victory-cardchallenge-backbtn.png"] forState:UIControlStateNormal];
    [dissmissBtn setImage:[UIImage imageNamed:@"victory-cardchallenge-tapbackbtn.png"] forState:UIControlStateHighlighted];
    dissmissBtn.hidden = YES;
    [dissmissBtn addTarget:self action:@selector(goBackCardVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:dissmissBtn];

}

- (void)moveSocialButton
{
    twitterIcon.center = CGPointMake(cardBG.frame.size.width-90, twitterIcon.center.y);
    facebookIcon.center = CGPointMake(cardBG.frame.size.width-40, facebookIcon.center.y);
}



//------------------------------------------------------------------------------------//
#pragma mark - ソーシャル機能
//------------------------------------------------------------------------------------//
- (void)SLComposeViewControllerButtonPushed:(id)sender
{
    //ボタン非表示
    dissmissBtn.hidden = YES;
    // May return nil if a tracker has not yet been initialized with
    // a property ID.
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    NSString* serviceType;
    NSString *SNSType;
    
    //NSLog(@"senders tag = %d",[sender tag]);
    if ([(UIButton*)sender tag] == TWITTER_BTN_TAG)
    {
        serviceType = SLServiceTypeTwitter;   // Twitter
        SNSType = GOOGLE_ANALYTICS_EVENT_ACTION_TW;
        
    }
    if ([(UIButton*)sender tag] == FACEBOOK_BTN_TAG)
    {
        serviceType = SLServiceTypeFacebook;  // Facebook
        SNSType = GOOGLE_ANALYTICS_EVENT_ACTION_FB;
    }
    //NSLog(@"サービスタイプ%@",serviceType);
    // TODO:GoogleAnalytics
    // This event will also be sent with &cd=Home%20Screen.
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_BUTTON
                                                          action:SNSType
                                                           label:GOOGLE_ANALYTICS_EVENT_LABEL_NIL
                                                           value:nil] build]];
    
     
    
    SLComposeViewController *composeViewController = [SLComposeViewController
                                                      composeViewControllerForServiceType:serviceType];
    // テキストの初期値
    NSString *message = [NSString stringWithFormat:@"横浜F・マリノスコレクションカード『%@ %@』ゲット！#fmarinos ",selectedCardIDString,selectedCardCharaName];
    
    
    [composeViewController setInitialText:message];
    
    UIImage *img = [self screenCapture];
    [composeViewController addImage:img];
    
    // URLを追加(自分のアプリのストアURLに変更)
    [composeViewController addURL:[NSURL URLWithString:@"http://itunes.apple.com/app/yokohama.f.marinos-collectioncards/id698571630"]];
    //cocos2d対応
    //[[[CCDirector sharedDirector]parentViewController]  presentViewController:composeViewController animated:YES completion:nil];
    //UIKit対応
    //[self presentViewController:composeViewController animated:YES completion:nil];
    [self presentViewController:composeViewController animated:NO completion:nil];
    
    //⬇これでDone or Cancelを判定
    //コールバック関数（怪しい固まる原因??）iOS6以前では固まるので判定しておく
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
    {  //iOS7未満の場合
        [composeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultDone:
                    NSLog(@"Done!!");
                    // TODO:GoogleAnalytics
                    // This event will also be sent with &cd=Home%20Screen.
                    /*
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_SNSPOST
                                                                          action:SNSType
                                                                           label:@"Done"
                                                                           value:nil] build]];
                     */
                    
                    break;
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Cancel!!");
                    // TODO:GoogleAnalytics
                    // This event will also be sent with &cd=Home%20Screen.
                    /*
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:GOOGLE_ANALYTICS_EVENT_CATEGORY_SNSPOST
                                                                          action:SNSType
                                                                           label:@"Cancel"
                                                                           value:nil] build]];
                     */
                    break;
            }
        }];
    }
    
    [self performSelector:@selector(showDissmissBtn:) withObject:nil afterDelay:1.0f];
    
}

//------------------------------------------------------------------------------------//
#pragma mark - 画面をキャプチャしてUIImageを返す
//------------------------------------------------------------------------------------//
-(UIImage *)screenCapture
{
    // キャプチャ対象をWindowにします。
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    // キャプチャ画像を描画する対象を生成します。
    UIGraphicsBeginImageContextWithOptions(window.bounds.size, NO, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Windowの現在の表示内容を１つずつ描画して行きます。
    for (UIWindow *aWindow in [[UIApplication sharedApplication] windows])
    {
        [aWindow.layer renderInContext:context];
    }
    
    // 描画した内容をUIImageとして受け取ります。
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    // 描画を終了します。
    UIGraphicsEndImageContext();
    return capturedImage;
}

//------------------------------------------------------------------------------------//
#pragma mark - パーティクルセットアップ
//------------------------------------------------------------------------------------//
-(void)getParticle
{
    self.emitterLayer = [CAEmitterLayer layer];
    self.emitterLayer.beginTime = 1.0;
    
    self.emitterLayer.renderMode = kCAEmitterLayerAdditive;
    [self.view.layer addSublayer:self.emitterLayer];
    //-----------
    // パーティクル画像
    UIImage *particleImage = [UIImage imageNamed:@"particle.png"];
    
    /*
     birthRate:１秒間に生成するパーティクルの数。
     lifetime:パーティクルが発生してから消えるまでの時間。単位は秒。
     color:パーティクルの色。
     velocity:パーティクルの秒速。
     emissionRange:パーティクルを発生する角度の範囲。単位はラジアン。
     
     */
    // 花火自体の発生源
    CAEmitterCell *baseCell = [CAEmitterCell emitterCell];
    baseCell.emissionLongitude = -M_PI / 2;
    baseCell.emissionLatitude = 0;
    baseCell.emissionRange = M_PI / 5;
    baseCell.lifetime = 1.0;
    baseCell.scale = 0.6;
    baseCell.birthRate = 10.0f;//ノーマル１　レア２だったから使ってみた。
    baseCell.velocity = 400;
    baseCell.velocityRange = 50;
    baseCell.yAcceleration = 300;
    baseCell.color = CGColorCreateCopy([UIColor colorWithRed:0.5
                                                       green:0.5
                                                        blue:0.5
                                                       alpha:0.5].CGColor);
    baseCell.redRange   = 0.5;
    baseCell.greenRange = 0.5;
    baseCell.blueRange  = 0.5;
    baseCell.alphaRange = 0.5;
    baseCell.name = @"fireworks";
    
    // 破裂後に飛散するパーティクルの発生源
    CAEmitterCell *sparkCell = [CAEmitterCell emitterCell];
    sparkCell.contents = (__bridge id)particleImage.CGImage;
    sparkCell.emissionRange = 2 * M_PI;
    sparkCell.birthRate = 600;
    sparkCell.scale = 0.4;
    sparkCell.velocity = 300;
    sparkCell.lifetime = 0.4;
    sparkCell.yAcceleration = 80;
    sparkCell.beginTime = 0.01;//risingCell.lifetime
    sparkCell.duration = 0.1;
    sparkCell.alphaSpeed = -0.2;
    sparkCell.scaleSpeed = -0.6;
    
    // baseCellからrisingCellとsparkCellを発生させる
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f)
    {  //iOS以上の場合
        
        // 上昇中のパーティクルの発生源
        CAEmitterCell *risingCell = [CAEmitterCell emitterCell];
        risingCell.contents = (__bridge id)particleImage.CGImage;
        risingCell.emissionLongitude = (4 * M_PI) / 2;
        risingCell.emissionRange = M_PI / 7;
        risingCell.scale = 0.4;
        risingCell.velocity = 100;
        risingCell.birthRate = 50;
        risingCell.lifetime = 1.5;
        risingCell.yAcceleration = 350;
        risingCell.alphaSpeed = -0.7;
        risingCell.scaleSpeed = -0.1;
        risingCell.scaleRange = 0.1;
        risingCell.beginTime = 0.01;
        risingCell.duration = 0.7;
        
        baseCell.emitterCells = [NSArray arrayWithObjects:risingCell, nil];
        risingCell.emitterCells = [NSArray arrayWithObjects:sparkCell, nil];
    }
    // baseCellはemitterLayerから発生させる
    else
    {
        baseCell.emitterCells = [NSArray arrayWithObjects:sparkCell, nil];
    }
    self.emitterLayer.emitterCells = [NSArray arrayWithObjects:baseCell, nil];
    
    //-----------
    CGSize size = self.view.bounds.size;
    self.emitterLayer.emitterPosition = CGPointMake(size.width / 2, size.height * 4 / 5);
}

//------------------------------------------------------------------------------------//
#pragma mark - Initialize
//------------------------------------------------------------------------------------//
-(void)setup
{
    UIImageView *background = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"victory-cardchallenge-scene.png"]];
    if (WIN_SIZE.height==568)
    {
        background.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
        background.image = [UIImage imageNamed:@"victory-cardchallenge-scene-568h.png"];
    }
    background.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    [self.view addSubview:background];
    
    UIImageView *titleImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"victory-cardchallenge-titlelogo.png"]];
    titleImage.center = CGPointMake(WIN_SIZE.width/2, titleImage.frame.size.height/2);
    titleImage.userInteractionEnabled = YES;
    [self.view addSubview:titleImage];
    
    UIImageView *logoImg= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"victory-cardchallenge-logo.png"]];
    logoImg.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2 + 50);
    [self.view addSubview:logoImg];
    
    UIImageView *cautionImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"victory-cardchallenge-utext.png"]];
    cautionImg.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height-15.0f);
    [self.view addSubview:cautionImg];
    
    cardGetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cardGetBtn.frame = CGRectMake(0.0f, 0.0f, 215.0f, 52.0f);
    cardGetBtn.center = CGPointMake(WIN_SIZE.width*0.5f, WIN_SIZE.height - cautionImg.frame.size.height*2.5);
    //victory-cardchallenge-cardbtn
    [cardGetBtn setImage:[UIImage imageNamed:@"victory-cardchallenge-cardbtn.png"] forState:UIControlStateNormal];
    [cardGetBtn setImage:[UIImage imageNamed:@"victory-cardchallenge-tapcardbtn.png"] forState:UIControlStateHighlighted];
    [cardGetBtn addTarget:self action:@selector(getButtonPushed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cardGetBtn];
    
    /*
    _myPointLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 50)];
    // myPointLabel.ceter = {185, 279}
    _myPointLabel.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2 + 57 - TAB_SPACE);
    _myPointLabel.text = [NSString stringWithFormat:@"残り%dpt",myPoint];
    _myPointLabel.textColor = [UIColor whiteColor];
    _myPointLabel.backgroundColor = [UIColor clearColor];
    _myPointLabel.textAlignment = NSTextAlignmentCenter;
    _myPointLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:29];
    _myPointLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:_myPointLabel];
     */
    
    /*
    UILabel *playPointLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 50)];
    playPointLabel.center = CGPointMake(_myPointLabel.center.x, _myPointLabel.center.y + 48);
    playPointLabel.text = [NSString stringWithFormat:@"1回 %dpt",PLAY_POINT];
    playPointLabel.textColor = [UIColor whiteColor];
    playPointLabel.backgroundColor = [UIColor clearColor];
    playPointLabel.textAlignment = NSTextAlignmentCenter;
    playPointLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:30];
    [self.view addSubview:playPointLabel];
     */
    
    cardBG = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"cardget-bg.png"]];
    if (WIN_SIZE.height==568) {
        cardBG.frame = CGRectMake(0, 0, WIN_SIZE.width, WIN_SIZE.height);
        cardBG.image = [UIImage imageNamed:@"cardget-bg-568h.png"];
    }
    cardBG.center = CGPointMake(WIN_SIZE.width/2, WIN_SIZE.height/2);
    cardBG.alpha = 0.0f;
    [self.view addSubview:cardBG];
}

@end
