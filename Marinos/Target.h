//
//  Target.h
//  testTouchGame2
//
//  Created by Takahashi Daichi on 2013/10/31.
//  Copyright (c) 2013年 USER. All rights reserved.
//

#import <UIKit/UIKit.h>

//動きのインターバル
#define kMoveTimerInterval 0.6f

@interface Target : UIImageView
{
  //速度
  float moveSpeed;
  
  //動くためのタイマー
  NSTimer *moveTimer;
}

@property int typeNo;

+ (id) targetWithType;
- (id) initWithType;


- (void) doStop;
- (void) stopTimer;
- (void) startTimer;

@end
