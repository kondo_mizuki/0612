#import "ViewManager.h"
#import "Util.h"
#import "CustomWebViewController.h"

@interface ViewManager()

@property(nonatomic,retain) CustomWebViewController *cv;
@property(nonatomic, retain) UIImageView *bg;
@property (nonatomic, strong) UIImageView *headerBg;
@property (nonatomic, strong) UIImageView *headerLogo;

@end

@implementation ViewManager

static ViewManager* sharedObj = nil;

+ (ViewManager*)sharedManager {
    @synchronized(self) {
        if (sharedObj == nil) {
            sharedObj = [[self alloc] init];
        }
    }
    return sharedObj;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedObj == nil) {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone {
    return self;
}

- (void) setCommonBg:(UIView*)view {
    _bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_bg.png"]];
    [view addSubview:_bg];
}

- (void) removeCommonBg {
    if (_bg) {
        [_bg removeFromSuperview];
    }
}

- (void) setHeader:(UIView*)view {
    _headerBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_bg.png"]];
    _headerBg.frame=CGRectMake(0, 0, WIN_SIZE.width, 50);
    [view addSubview:_headerBg];

//    _headerLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_logo.png"]];
//    _headerLogo.center = CGPointMake([Util getCenterWidth], _headerBg.image.size.height/2);
//    [view addSubview:_headerLogo];
}

- (void) removeHeader {
    if (_headerBg) {
        [_headerBg removeFromSuperview];
    }
}

- (void) setHeaderWithRetutn:(UIView*)view target:(id)target action:(NSString*)selector {
    _headerBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_bg.png"]];
    [view addSubview:_headerBg];

    _headerLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_logo.png"]];
    _headerLogo.center = CGPointMake([Util getCenterWidth], _headerBg.image.size.height/2);
    [view addSubview:_headerLogo];

    UIImage *backImage = [UIImage imageNamed:@"ranking_arrow_left.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(0, 15, backImage.size.width, backImage.size.height);
    backButton.center = CGPointMake(15, _headerBg.image.size.height/2);
    backButton.backgroundColor = [UIColor clearColor];
    [backButton setBackgroundImage:backImage forState:UIControlStateNormal];
    [backButton setBackgroundImage:backImage forState:UIControlStateHighlighted];
    [backButton addTarget:target action:NSSelectorFromString(selector) forControlEvents:UIControlEventTouchDown];

    [view addSubview:backButton];
}

- (void) setWebViewWithBackButton:(UIView*)view urlString:(NSString*) urlString {
    _cv = [[CustomWebViewController alloc] initWithFrame:CGRectMake(0, [Util getWinSize].size.height, [Util getWinSize].size.width, [Util getWinSize].size.height)];
    [_cv setbackButton:self action:@"closeButtonCallBack:"];
    [_cv loadUrl:urlString view:view];
    _cv.backgroundColor = [UIColor redColor];
    [view addSubview:_cv];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         _cv.frame = CGRectMake(0, 0, [Util getWinSize].size.width, [Util getWinSize].size.height);
                     }
                     completion:^(BOOL finished){
                     }];
}
- (void) closeButtonCallBack:(UIButton*)button {
    [UIView animateWithDuration:0.5f
                     animations:^{
                         _cv.frame = CGRectMake(0, [Util getWinSize].size.height, [Util getWinSize].size.width, [Util getWinSize].size.height);
                     }
                     completion:^(BOOL finished){
                         [_cv removeFromSuperview];
                     }];
}

@end
