//
//  Target.m
//  testTouchGame2
//
//  Created by Takahashi Daichi on 2013/10/31.
//  Copyright (c) 2013年 USER. All rights reserved.
//

#import "Target.h"

@implementation Target

#pragma mark - タイマーの開始と停止

- (void) stopTimer
{
  if (moveTimer)
  {
    [moveTimer invalidate];
    moveTimer = nil;
  }
}
- (void) startTimer
{
  //[self stopTimer];
  moveTimer = [NSTimer scheduledTimerWithTimeInterval:kMoveTimerInterval
                                               target:self
                                             selector:@selector(move)
                                             userInfo:nil
                                              repeats:YES];
}

#pragma mark - タイマー処理
- (void) move
{
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        CGPoint point =[self makeRadndomPoint];
        if (point.x > self.center.x) {
            self.image=[UIImage imageNamed:@"sakana_right.png"];
        }else{
            self.image=[UIImage imageNamed:@"sakana_left.png"];
        }
        self.center=point;
    }completion:nil];
}

-(CGPoint)makeRadndomPoint{
    
    CGPoint rtPoint;
    rtPoint.x =30 + arc4random()%290;
    rtPoint.y =100 + arc4random()%300;

    return rtPoint;
}


#pragma mark - 開始と停止

- (void) doStartWithSpeed:(float)speed
{
   [self startTimer];
}

- (void) doStop
{
  [self stopTimer];
  self.hidden = YES;
}

#pragma mark - 簡易コンストラクタ

#pragma mark - initialized


+ (id) targetWithType
{
  
  Target * tar = [[self alloc]initWithType];
  
  return tar;
  
  
}

- (id) initWithType
{
  NSString *imgName = [NSString stringWithFormat:@"sakana_right.png"];
  if ((self = [super initWithImage:[UIImage imageNamed:imgName]]))
  {
   self.transform = CGAffineTransformMakeScale(0.5, 0.5);
      self.userInteractionEnabled = YES;
      self.center=[self makeRadndomPoint];

  }
  return self;
}

- (void)dealloc
{
  [self stopTimer];
  
}


@end
