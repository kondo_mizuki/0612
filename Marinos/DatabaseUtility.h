//
//  DatabaseUtility.h
//  BigbrotherMap
//
//  Created by kyo on 2013/07/18.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"

// ゲーム用DBで使うクラス
//#import "PlayerData.h"

@interface DatabaseUtility : NSObject

+ (DatabaseUtility*)sharedManager;
- (BOOL)tablesCreater;
- (NSMutableArray*)getAlbumCells;
- (NSMutableArray*)getPlaceCells;
- (NSMutableArray*)getAllLocation;
- (NSMutableDictionary*)getLocation:(int)identifier;
- (NSMutableArray*)getSPCheckIn:(int)identifier;
- (BOOL)updateLastCheckIn:(NSString*)now withID:(int)identifier;
- (BOOL)updateLastSPCheckIn:(NSString*)now withID:(int)identifier;
//- (BOOL)updateForCheckin:(int)cnt withID:(int)identifier;
//- (BOOL)updateForSPCheckin:(int)cnt withID:(int)identifier;
- (BOOL)updateLastNotification:(NSString*)now withID:(int)identifier;
//-----------------------------------------------------------------//
- (BOOL)updateGetCard:(int)cardID;
//- (BOOL)updateGetCard:(int)cardID year:(int)year;
//-----------------------------------------------------------------//
- (int)getAlbumCount;
//-----------------------------------------------------------------//
- (int)getCardCount;
//- (int)getCardCount:(int)year;
//-----------------------------------------------------------------//
- (int)getGettedCardCount;
//- (int)getGettedCardCount:(int)year;
//-----------------------------------------------------------------//
- (int)getGettedAllCardCount;
//- (int)getGettedAllCardCount:(int)year;
//-----------------------------------------------------------------//
- (int)getGettedCardPossession:(int)cardID;
//- (int)getGettedCardPossession:(int)cardID year:(int)year;
//-----------------------------------------------------------------//

/*
 * カードのIDからカードのレアレベルを取得する
 * 第一引数：カードID
 * 戻り値　：カードレアレベル
 */
- (int) getCardRareLevelByCardIdentifer:(int)cardIdentifier;

/*
 * カードのIDからカードのレアレベルを取得する
 * 第一引数：レアレベルID
 * 戻り値　：レアレベルテーブルのテキスト
 */
- (NSString *) getRareLevelTextByRareLevelIdentifier:(int)rareLevelIdentifier;

- (NSMutableDictionary*)getSelectedCard:(int)cardID;
//- (NSMutableDictionary*)getSelectedCard:(int)cardID year:(int)year;
- (NSMutableDictionary *)getSelectCardPrimaryID:(int)primary_ID;
//-----------------------------------------------------------------//
- (NSMutableArray *)getAllCard;
- (NSMutableArray *)getAllFileNameOfCardTable;
- (NSMutableArray *)getAllFileNameAndMD5;
- (BOOL)checkMD5Data:(NSString *)file_name MD5:(NSString *)MD5_str;
//-----------------------------------------------------------------//
- (void)insertCardTable:(NSMutableArray *)cardDicAry;
- (void)insertPlaceTable:(NSMutableArray *)placeDicAry;
- (void)insertCardCategory:(NSMutableArray *)cardCategoryDicAry;
- (void)insertPlaceCategory:(NSMutableArray *)placeCategoryDicAry;
- (void)insertSpecialPlace:(NSMutableArray *)specialPlaceDicAry;
- (void)insertRareLevelTable:(NSMutableArray *)rareLevelDicAry;
//-----------------------------------------------------------------//
- (void)updateCardTable:(NSMutableArray *)cardDicAry;
- (void)updatePlaceTable:(NSMutableArray *)placeDicAry;
- (void)updateCardCategory:(NSMutableArray *)cardCategoryDicAry;
- (void)updatePlaceCategory:(NSMutableArray *)placeCategoryDicAry;
- (void)updateSpecialPlace:(NSMutableArray *)specialPlaceDicAry;
- (void)updateRareLevelTable:(NSMutableArray *)rareLevelDicAry;
//-----------------------------------------------------------------//
- (BOOL)checkTableData:(NSString *)table_name tableID:(NSNumber *)primary_id;
//-----------------------------------------------------------------//
- (void)deleteTable:(NSString *)table_name serialID:(NSString *)serial_num;
//-----------------------------------------------------------------//
- (void)updateProcess;
//-----------------------------------------------------------------//
//- (NSMutableArray *)getCardData:(int)year;
//- (NSMutableArray *)getCardDataForAlbumPossession:(NSString *)album_yaer_name;
- (NSMutableArray *)getCardData;
- (NSMutableArray *)getCardDataForAlbumPossession;


//- (void)updateTest:(NSString *)text cardID:(int)card_ID;
//- (void)insertTest:(NSString *)text testInt:(int)test_int testDouble:(double)test_double;

// ゲーム用DB
//- (NSString*)savePlayerData:(PlayerData*)dat;
- (void)saveRetirementIdentifier:(NSString*)identifier;
//- (void)saveAbilityData:(PlayerData*)dat identifier:(NSString*)identifier;
//- (void)savePotentialData:(PlayerData*)dat identifier:(NSString*)identifier;
//- (void)saveSenileData:(PlayerData*)dat identifier:(NSString*)identifier;
//- (void)saveGameData:(PlayerData*)dat identifier:(NSString*)identifier;
- (void)saveCountData:(NSInteger)num identifier:(NSString*)identifier;

- (NSArray*)getPlayerList;
- (NSArray*)getRetirementPlayerList;
//- (PlayerData*)getPlayerData:(NSString*)identifier;
//- (PlayerData*)getGameDataAll:(NSString*)identifier;

- (void)setCareerData:(NSDictionary*)dic identifier:(NSString*)identifier;

//アイテム関係
- (NSArray *)getMyItems;
- (NSDictionary *)getMyItemForId:(int)itemId;
- (int)addMyItemForId:(int)itemId count:(int)cnt;//返却値:計算後のアイテム数

@end
