//
//  FileManager.h
//  Marinos
//
//  Created by eagle014 on 2014/04/28.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FileManager;
@protocol FileManagerDelegate <NSObject>
@optional
- (void) didFinishedInsertProcess:(NSString *)versionNum downloadDicAry:(NSMutableArray *)downloadDicArray;

@end
@interface FileManager : NSObject

@property (nonatomic, copy, readonly) NSString *filePath;
@property (nonatomic, copy, readonly) NSString *filePathIsNotApplecableBackUp;
@property (nonatomic, weak) id <FileManagerDelegate> delegate;

+ (FileManager *)sharedManager;

- (BOOL) saveDirectory:(NSString *)directory fileName:(NSString *)file_name withData:(NSData *)data;
- (NSData *) loadDirectory:(NSString *)directory fileName:(NSString *)file_name;
- (NSString *) getFilePathDirectoty:(NSString *)directory fileName:(NSString *)file_name;

/*
 * iCloudに保存出来る領域が変更になった為
 */
- (BOOL) makeDirectoryForIsNotApplicableBackUp;
- (BOOL) addSkipBackupAttribute;

/* db操作 */
- (void)insertDatabase:(NSDictionary *)jsonDic;

/* tmp */
- (NSString *)temporaryDirectory;
/* /tmp/fileName */
- (NSString *)temporaryDirectoryWithFileName:(NSString *)fileName;

/* /Documents */
- (NSString *)documentDirectory;
/* /Documents/fileName */
- (NSString *)documentDirectoryWithFileName:(NSString *)fileName;

/* pathのファイルが存在しているか */
- (BOOL)fileExistsAtPath:(NSString *)path;

/* pathのファイルがelapsedTimeを超えているか */
- (BOOL)isElapsedFileModificationDateWithPath:(NSString *)path elapsedTimeInterval:(NSTimeInterval)elapsedTime;

/* directoryPath内のextension(拡張子)と一致する全てのファイル名 */
- (NSArray *)fileNamesAtDirectoryPath:(NSString *)directoryPath extension:(NSString *)extension;

/* pathのファイルを削除 */
- (BOOL)removeFilePath:(NSString *)path;
@end
