#import <Foundation/Foundation.h>

@interface UtilWebView : UIWebView <UIWebViewDelegate>
{
    
}

#pragma mark - property
@property UIActivityIndicatorView *indicator;

- (UtilWebView*) loadUrl:(NSString *)targetUrl view:(UIView *)view;

@end
