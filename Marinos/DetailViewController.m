//
//  DetailViewController.m
//  TimeCapsule
//
//  Created by kyo on 2013/02/20.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "DetailViewController.h"
#import "Define.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "Util.h"



@interface DetailViewController ()

@end

@implementation DetailViewController

- (void) dismiss
{
    
    [self.navigationController popViewControllerAnimated:YES];
    /*
     switch (self.from) {
     case FromCheckInViewController:
     {
     [self dismissViewControllerAnimated:YES completion:nil];
     break;
     }
     case FromAlbumViewController:
     {
     [self.navigationController popViewControllerAnimated:YES];
     break;
     }
     case FromCameraViewController:
     {
     [self dismissViewControllerAnimated:YES completion:nil];
     break;
     }
     default:
     break;
     }
     */
}


-(void) showComposerSheet {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:[NSString stringWithFormat:@"%@", [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"]]];
    
    //メールの本文を設定
    NSString *emailBody = [NSString stringWithFormat:@"%@", [self.webView stringByEvaluatingJavaScriptFromString:@"document.URL"]];
    [picker setMessageBody:emailBody isHTML:NO];
    
    [self presentViewController:picker animated:YES completion:nil];
}


#pragma mark アラート表示
-(void) setAlert:(NSString *)aTitle body:(NSString *)aDescription {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:aTitle message:aDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}



#pragma mark - webViewのdelegateメソッド
- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    //キャッシュを全て消去
    //動作検証がしきれない。
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        self.url = [request.URL absoluteURL];
        NSLog(@"webViewのdelegateメソッド : %@", [request.URL path]);
        NSLog(@"webViewのdelegateメソッド : %@", [request.URL absoluteString]);
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView*)webView {
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - setUpParts
- (void)setUpParts {
    [self setUpDetailPageParts];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)setUpDetailPageParts {
    
    // WebViewで詳細閲覧画面を表示
    [self setUpDetailBodyParts];
    [self setUpDetailHeaderParts];
    
    // WebViewでURLの読み込み
//    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"iOSWebView", @"UserAgent", nil];
//    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    
 }

- (void)setUpDetailHeaderParts
{
    
        UIImageView *headerTitleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"normal_nav_bar.png"]];
        headerTitleImageView.frame = CGRectMake(0,0, 320,50);
        [self.view addSubview:headerTitleImageView];
    
    UIButton *closebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closebtn.frame = CGRectMake(7, 0, 23, 23);
    
    [closebtn setImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [closebtn setImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateHighlighted];

    
    //ここの処理は考えないとやばそう。特に条件がまずい。
    //    if (self.navigationController) {
    //        [closebtn addTarget:self action:@selector(backToHistoryView) forControlEvents:UIControlEventTouchUpInside];
    //    } else {
    [closebtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    //    }
    
    [self.view addSubview:closebtn];
    //[headerTitleImageView addSubview:closebtn];
}


- (void)backToHistoryView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpDetailBodyParts {
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,44,320, SCREEN_BOUNDS.size.height-44-49)];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}


#pragma mark -
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpParts];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
