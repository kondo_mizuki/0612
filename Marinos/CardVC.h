//
//  CardVC.h
//  Marinos
//
//  Created by KEi on 13/08/06.
//  Copyright (c) 2013年 Keishi Kuwabara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <ADG/ADGManagerViewController.h>

#import "CustomTabBarController.h"

#define MORE_BTN 1003
#define CARD_TAG 1004


typedef enum {
    cardPointError,
    cardPointFaild,
    cardGetError,
    cardGetFaild
}CVResponseType;

@interface CardVC : GAITrackedViewController<UIAlertViewDelegate, CustomTabBarControllerDelegate, ADGManagerViewControllerDelegate>
{
    ADGManagerViewController *ADGViewController;
    //ゲーム時間
    float timeCount;
    //時間表示ラベル
    UILabel *timeLabel;
    //タッチラベル
    UILabel *scoreLabel;
    
    //生成のタイマー
    NSTimer *spawnTimer;
}
@property (nonatomic, retain) ADGManagerViewController *adGManagerViewController;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;

- (IBAction)startBtnPush:(id)sender;

@end
