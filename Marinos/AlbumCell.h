#import <UIKit/UIKit.h>

@interface AlbumCell : UICollectionViewCell

typedef enum {
    NORMAL = 1,
    TARGET
} CargCategory;

- (void) displayCard:(int)cardID possession:(int)cardPossession albumName:(NSString *)album_name cardCategory:(int)card_category cardText:(NSString *)card_text;

@end
