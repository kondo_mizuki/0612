//
//  TutorialScrollView.h
//  Marinos
//
//  Created by 菊地 一貴 on 2013/10/03.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TutorialScrollViewDelegate
@optional
-(void)tutorialCloseButtonPushed:(id)sender;
@end

@interface TutorialScrollView : UIScrollView

@property (nonatomic,assign) id <TutorialScrollViewDelegate> delegate2;
//@property(nonatomic,assign) id delegate;

@end
