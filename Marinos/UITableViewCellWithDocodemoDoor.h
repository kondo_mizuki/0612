//
//  UITableViewCellWithDocodemoDoor.h
//  TimeCapsule
//
//  Created by kyo on 2013/05/17.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCellWithDocodemoDoor : UITableViewCell

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, strong) UILabel *surroundDelta;
@property (nonatomic, strong) UIImageView *batch;
@property (nonatomic, assign) int postedCount;

@end
