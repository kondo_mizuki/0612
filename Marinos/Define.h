//
//  Define.h
//  Marinos
//
//  Created by 菊地 一貴 on 13/08/07.
//  Copyright (c) 2013年 菊地 一貴. All rights reserved.
//

#ifndef Marinos_Define_h
#define Marinos_Define_h

#define WIN_SIZE [[UIScreen mainScreen]bounds].size

#define APP_ID @"698571630"
#define APP_NAME @"横浜F・マリノスコレクションカード"

// Define
// tag

#define SIGHTS_TAG 100
#define FOOD_TAG 101
#define HOTEL_TAG 102
#define SHOP_TAG 103
#define ACCOUNTING_TAG 104
#define DETAIL_TEXTFIELD_TAG 105
#define SENDPHOTO_BACKGROUND_TAG 106
#define SHOT_BTN_TAG 107
#define POST_BTN_TAG 1111
#define TITLE_TEXTFIELD_TAG 108
#define TWITTER_BTN_TAG 109
#define FACEBOOK_BTN_TAG 110
#define STAY_TIME_LABEL_TAG 111
#define ALBUM_COUNT_LABEL_TAG 112
#define GETTED_ANIKICARD_COUNT_LABEL_TAG 113
#define ANIKI_INFO_BACKGROUND_TAG 114
#define ANIKI_COMMENT_LABEL_TAG 115
#define COMPASS_TAG 116

#define POINT_LABEL_TAG 117
#define GETTED_CARD_COUNT_LABEL_TAG 118

#define ANIKI_COMMENT_DETAIL_TAG 119
//
#define BOTTOM_LABEL_TAG 120

#define MAP_COUNT_PIN_TAG 150
#define MAP_COUNT_LABEL_TAG 151

#define CARD_DONT_HAVE 901
#define CARD_HAVE 902

/*
#warning リリース時は100に変更する！！！
#define LOG_IN_BONUS_FIRST 100//default:100
#define LOG_IN_BONUS_SPETCIAL 20//default:20
#define LOG_IN_BONUS_NORMAL 10//default:10
*/
/*
#define TAB_TOP 1001
#define TAB_MAP 1002
#define TAB_CARD 1003
#define TAB_ALBUM 1004
#define TAB_ALBUM_DETAIL 1005
#define TAB_CAMERA 1010
 */
#define SOUND_TAB 1
#define SOUND_GET_BUTTON 2
#define SOUND_CARD_APPEAR 3
#define SOUND_CARD_ZOOM_NORMAL 4
#define SOUND_CARD_ZOOM_RARE 5
#define SOUND_GET_TEXT 6
#define SOUND_ALBUM 7
#define SOUND_KIIIN 8
#define SOUND_CHUIU 9
#define SOUND_PYORO 10
#define SOUND_PIRON 11
#define SOUND_VICTORY_APPER 12
#define SOUND_VICTORY_BUTTON 13
#define SOUND_VICTORY_FINISH 14

#define CARD_LEVEL_NORMAL 1
#define CARD_LEVEL_RARE 2
#define CARD_LEVEL_HOMEGAME 3
#define CARD_LEVEL_VICTORY 4 //優勝

#define CARD_CATEGORY_PLAYER 1
#define CARD_CATEGORY_VICTORY 2
#define CARD_CATEGORY_GOAL 3
#define CARD_CATEGORY_HOMEGAME 4
#define CARD_CATEGORY_OTHER 5

#define TABBUTTON_HEIGHT 50.0f
#define TOOLBAR_HEIGHT 39.0f
#define TOP_BAR_HEIGHT 44.0f
#define TOP_TITLE_FONT_SIZE 16.0f
// DB_FILE
#define DB_FILE @"tsurikichi.db"

// GoogleAnalytics用のDefine
#define GOOGLE_ANALYTICS_EVENT_CATEGORY_BUTTON @"ButtonTapped"
#define GOOGLE_ANALYTICS_EVENT_CATEGORY_TAB @"TabTapped"
#define GOOGLE_ANALYTICS_EVENT_CATEGORY_SNSPOST @"SNSPosted"
#define GOOGLE_ANALYTICS_EVENT_CATEGORY_WEBVIEW @"WebViewTapped"
#define GOOGLE_ANALYTICS_EVENT_CATEGORY_CHECKIN @"CheckIn"

#define GOOGLE_ANALYTICS_EVENT_ACTION_FB @"Facebook"
#define GOOGLE_ANALYTICS_EVENT_ACTION_TW @"Twitter"
#define GOOGLE_ANALYTICS_EVENT_ACTION_CARDGET @"CardGet"

#define GOOGLE_ANALYTICS_EVENT_LABEL_SUCCEED @"Succeed"
#define GOOGLE_ANALYTICS_EVENT_LABEL_FAILD @"Faild"
#define GOOGLE_ANALYTICS_EVENT_LABEL_FAILD_UPLOAD_ERROR @"Faild_UploadError"
#define GOOGLE_ANALYTICS_EVENT_LABEL_FAILD_NETWORK_ERROR @"Faild_NetworkError"
#define GOOGLE_ANALYTICS_EVENT_LABEL_NIL nil

//album
/*
#define ALBUM_2013 @"2013"
#define ALBUM_2014_OFF @"2014"
#define ALBUM_2014 @"2015"
#define ALBUM_2015 @"2015"
#define ALBUM_2017 @"2017" // for test
*/
//Directory
#define DOCUMENT_DIR @"Documents"
#define TEMPORARY_DIR @"tmp"
#define DIR_IS_NOT_APPLICABLE_BACK_UP @"notBackUpDirectory"

//PUSH通知
#define PUSH_UPDATE_INTERVAL 60*60
#define PUSH_UPDATE_METER 10.0

//Usersdefaults
#define KEY_USER_ID @"KEY_USER_ID"
#define KEY_UNIVERSAL_UNIQUE_ID @"KEY_UNIVERSAL_UNIQUE_ID"
#define KEY_UNIVERSAL_UNIQUE_ID_DEVICE @"KEY_UNIVERSAL_UNIQUE_ID_DEVICE"
#define KEY_SEND_FLAG @"KEY_SEND_FLAG"
#define KEY_FIRST_PLAY_DEVICE @"KEY_FIRST_PLAY_DEVICE"
#define KEY_MIGRATION_FLAG @"KEY_MIGRATION_FLAG"
#define KEY_RECEIVE_PUSH_FLAG @"KEY_RECEIVE_PUSH_FLAG"

#define KEY_MY_CARD_POINT @"KEY_MY_CARD_POINT"
#define KEY_DATE_YEAR @"KEY_DATE_YEAR"
#define KEY_DATE_MONTH @"KEY_DATE_MONTH"
#define KEY_DATE_DAY @"KEY_DATE_DAY"
#define KEY_DATE_HOUR @"KEY_DATE_HOUR"
#define KEY_DATE_MINUTE @"KEY_DATE_MINUTE"
#define KEY_DATE_SECOND @"KEY_DATE_SECOND"

//server
//#define TIGERS_SERVER_MASTER @"http://app.tigers.capsule-server.info"
//#define TIGERS_SERVER_DEV @"http://153.122.25.234"
//#define TIGERS_SERVER_IMAGE_CARD @"http://image.tigers.capsule-server.info/card/"
//#define CONNECT_CODE_FINISHED @"R0000"
//#define CONNECT_CODE_CREATE @"R0001"
//#define CONNECT_CODE_ERROR_EXCEPTION @"E0000"
//#define CONNECT_CODE_ERROR_FAILD @"E0001"
//#define LOGIN_CODE_FINISHED @"R0000"
//#define LOGIN_CODE_FIRST @"R0001"
//#define LOGIN_CODE_NORMAL @"R0002"
//#define LOGIN_CODE_SPECIAL @"R0003"
//#define LOGIN_CODE_ERROR @"E0000"
//#define VERSION_CODE_FINISHED @"R0000"
//#define VERSION_CODE_CREATE @"R0001"
//#define VERSION_CODE_ERROR @"E0002"
//#define CARD_POINT_CODE_SUCCESS @"R0000"
//#define CARD_POINT_CODE_ERROR @"E0000"
//#define CARD_ID_CODE_SUCCESS @"R0000"
//#define CARD_ID_CODE_ERROR @"E0003"
//#define CHECKIN_CODE_FINISHED @"R0000"
//#define CHECKIN_CODE_NORMAL @"R0001"
//#define CHECKIN_CODE_SPECIAL @"R0002"
//#define CHECKIN_CODE_ERROR @"E0004"
//#define IMAGE_CODE_SUCCESS @"R0000"
//#define IMAGE_CODE_ERROR @"E0005"
//#define PHOTO_CODE_SUCCESS @"R0001"
//#define PHOTO_CODE_ERROR @"E0006"
//#define PHOTO_DELETE_CODE_SUCCESS @"R0000"
//
//#define PHP_PROGRAM_CONNECT @"/connect/"
//#define PHP_PROGRAM_SYNC @"/sync/"
//#define PHP_PROGRAM_LOGIN @"/login/"
//#define PHP_PROGRAM_VERSION @"/ver/"
//#define PHP_PROGRAM_CARD_POINT @"/gacha_point/"
//#define PHP_PROGRAM_CARD_GET_ID @"/gacha_card/"
//#define PHP_PROGRAM_CHECKIN @"/checkin/"
//#define PHP_PROGRAM_IMAGE @"/image/"
//#define PHP_PROGRAM_PHOTO @"/photo/"
//#define PHP_PROGRAM_DETAIL @"/detail/"
//#define PHP_PROGRAM_TRAINSEARCH @"/trainsearch/"
//#define PHP_PROGRAM_TRAINPOINT @"/trainpoint/"
//#define PHP_PROGRAM_TIMELINE @"/timeline/"
//#define PHP_PROGRAM_TRAINRANGE @"/trainrange/"
//#define PHP_PROGRAM_VIEW @"/view/"
//#define PHP_PROGRAM_PHOTOSHOW @"/photo_show/"

#define MAX_FAILD_C0UNT 3
#define TIMEOUT_TIME 90.0

#define KEY_VERSION_ID @"KEY_VERSION_ID" 
#define KEY_CURRENT_VERSION @"KEY_CURRENT_VERSION"

#define MY_FONT @"ShinGoPr5-Regular"//ShinGoPr5-Light//ShinGoPr5-Regular

#define INITIAL_LIKE_COUNT 0
#define INITIAL_TWEET_COUNT 0

#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]

// UserDefault
#define UD_LOGIN @"is_login"
#define UD_USER_NAME @"user_name"
#define UD_USER_PREFECTURE @"user_prefecture"
#define UD_USER_TEAM @"user_team"

// URL
#define URL_YAMAHA_REGIST @"http://google.com/"
#define URL_REGIST_EXPLANATION @"http://yahoo.co.jp/"
#define URL_TOP @"http://superrobot.com/revquest/"

// Mail
#define MAIL_FORGOT_PASSWORD @"ae15213900@gmail.com"

// Login
#define LOGIN_ID_MAX @"                      ６〜２０文字半角英数"
#define LOGIN_PW_MAX @"                      ６〜２０文字半角英数"
#define REGIST_NAME_MAX @"                      ６〜２０文字半角英数"

//AWS仕様に戻す
#define HEROKU_MASTER @"http://timecapsule-eagle-staging.herokuapp.com"

typedef NS_ENUM(NSInteger, AnnotationCategory)
{
    AnnotationCategorySights = 0,
    AnnotationCategoryFood,
    AnnotationCategoryHotel,
    AnnotationCategoryShop
};

typedef NS_ENUM(NSInteger, LogInCategory)
{
    AlreadyGetLogInBonus = 0,
    FirstLogIn,
    NormalLogIn,
    SPLogIn
};

typedef enum {
    firstAlert,
    otherAlert
}AlertType;

typedef NS_ENUM (NSInteger, DateFactor){
    dateYear,
    dateMonth,
    dateDay,
    dateHour,
    dateMinute,
    dateSecond
};
#endif
