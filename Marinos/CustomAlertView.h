//
//  CustomAlertView.h
//  Marinos
//
//  Created by eagle014 on 2014/04/28.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIAlertView

- (id) initWithError:(NSError *)error;

@end
