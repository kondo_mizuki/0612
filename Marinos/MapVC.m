#import "MapVC.h"

#import "Util.h"
#import "CheckinViewController.h"
#import "CustomPointAnnotation.h"
#import "DatabaseUtility.h"
#import "LocationUtility.h"
#import "MapTileAnnotation.h"
#import "FrameLabel.h"
#import "Toast+UIView.h"
#import "SVProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "MTPopupWindow.h"
#import "AdButton.h"
#import "ViewManager.h"
#import "Reachability.h"
#import "Toast+UIView.h"

@interface MapVC (){
    UIButton *segMapBtn;
    UIButton *segPhotoBtn;
}

@property(nonatomic, strong)IBOutlet MKMapView *mapView;
@property(weak,nonatomic) IBOutlet UILabel *titleLabel;
@property(weak,nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIButton *chkinSegBtn;
@property (weak, nonatomic) IBOutlet UIButton *photoSegBtn;

@property(nonatomic, strong)UIButton *checkinBtn;
@property(nonatomic, strong)NSString *spotName;
@property(nonatomic, assign)double latitude;
@property(nonatomic, assign)double longitude;
@property(nonatomic, assign)float y568;
@property (nonatomic, strong) MKMapView *photoMapView;
@property (weak, nonatomic) IBOutlet UIWebView *mapTrainView;
@property (nonatomic, strong) UIImageView *myImageView;
@property (nonatomic, strong) NSTimer *trainViewTimer;
@property (nonatomic, assign) BOOL firstTap;
@property (nonatomic, assign) int currentAnnotaion;
@property (nonatomic, assign) int previousAnnotaion;
@property (nonatomic, assign) BOOL tabBarIsVisible;
@property (weak, nonatomic) IBOutlet UIImageView *textPanel;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UIImageView *noPhotoView;
@property (weak, nonatomic) IBOutlet UIButton *homeStadiumBtn;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationBtn;

@property (weak, nonatomic) IBOutlet UIImageView *commentView;
@end

static const NSInteger kMapAdButtonTag = 1000;

@implementation MapVC


//⬇一番最初のチェックインボーナス
-(void)firstTimeAlert
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    [defaults setObject:@"YES" forKey:@"FIRST_MAP_FLAG"];
    [ud registerDefaults:defaults];
    
    BOOL firstMap = [ud boolForKey:@"FIRST_MAP_FLAG"];
    if (firstMap == YES)
    {
        [ud setBool:NO forKey:@"FIRST_MAP_FLAG"];
        [ud synchronize];
        UIAlertView *alert;
        alert = [[UIAlertView alloc] init];
        alert.title = @"チェックインボーナスについて";
        alert.message = @"タイガースに関連する場所にチェックインすると、チェックインボーナスがもらえるよ！\n特別なボーナスがもらえる場合も！？";
        [alert addButtonWithTitle:@"OK"];
        
        if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f)
        {
            ((UILabel *)[[alert subviews] objectAtIndex:1]).textAlignment = NSTextAlignmentLeft;
        }
        [alert show];
    }
}

- (void) showAdText:(AdButton *)sender {
    [MTPopupWindow showWindowWithHTMLString:sender.adText insideView:self.view];
}

-(void)setAnnotation:(CLLocationCoordinate2D)co radius:(double)radius placeID:(int)placeID mapMove:(BOOL)mapMove animated:(BOOL)animated detail:(NSString*)detail name:(NSString*)name
{
    //⬇TCandDBディレクトリに存在するクラス、ピン情報を司る
    CustomPointAnnotation *annotation = [[CustomPointAnnotation alloc] init];
    annotation.coordinate = co;
    annotation.radius = radius;
    annotation.placeID = placeID;
    annotation.detail = detail;
    annotation.title = name;
    
    [self.mapView addAnnotation:annotation];
}

- (void)showPinInMapView
{
    NSArray* array = [[DatabaseUtility sharedManager] getAllLocation];
    for (NSDictionary* location in array) {
        [self setAnnotation:CLLocationCoordinate2DMake([[location objectForKey:@"latitude"] doubleValue], [[location objectForKey:@"longitude"] doubleValue]) radius:[[location objectForKey:@"radius"] doubleValue] placeID:[[location objectForKey:@"identifier"] intValue] mapMove:NO animated:NO detail:[location objectForKey:@"detail"] name:[location objectForKey:@"name"]];
    }
}

int PLACE_ID;
- (void)newMoveCheckIn:(id)sender{
    // ネット接続状態確認
    Reachability *currentReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [currentReachability currentReachabilityStatus];
    /*
    switch (netStatus)
    {
        case NotReachable:        {
            // 圏外の場合の処理
            break;
        }
        case ReachableViaWWAN:        {
            // 携帯回線に接続可能な場合の処理
            break;
        }
        case ReachableViaWiFi:        {
            // wifiに接続可能な場合の処理
            break;
        }
    }*/
    if(netStatus==NotReachable){
        [self.view makeToast:@"インターネット接続環境をご確認ください。圏外の状態ではチェックインできません。" duration:1.0f position:@"bottom"];
    }else{
        CustomPointAnnotation *annotation = (CustomPointAnnotation *)sender;
        PLACE_ID = [annotation placeID];
        [self performSegueWithIdentifier:@"checkinSegue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"checkinSegue"]){
        CheckinViewController *checkinViewController = segue.destinationViewController;
        checkinViewController.place_id = PLACE_ID;
        [checkinViewController setHidesBottomBarWhenPushed:YES];
    }
}

- (void)moveCheckIn:(id)sender; {/*
                                  ???: もしかして:使っていない
    CheckinViewController *checkinViewController = [[CheckinViewController alloc] init];
    UIButton *cargo = (UIButton*)sender;
    checkinViewController.place_id = [cargo tag];
    [checkinViewController setHidesBottomBarWhenPushed:YES];

    CATransition *transition = [CATransition animation];
    [transition setDuration:0.5f];
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [transition setType:kCATransitionMoveIn];
    [transition setSubtype:kCATransitionFromRight];
    [[[[self navigationController] view] layer] addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:checkinViewController animated:NO];*/
    

}

- (void)annotationViewTapped:(UITapGestureRecognizer *)sender{
    MKAnnotationView *myAnnotation = (MKAnnotationView *)sender.view;
    CustomPointAnnotation *annotation = (CustomPointAnnotation*)myAnnotation.annotation;
    
    if (!self.firstTap) {
        self.firstTap = YES;
        [[NSUserDefaults standardUserDefaults] setInteger:annotation.placeID forKey:@"KEY_CURRENT_ANNOTATION_PLACE_ID"];
    } else {
        self.currentAnnotaion = annotation.placeID;
        self.previousAnnotaion = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"KEY_CURRENT_ANNOTATION_PLACE_ID"];
        
        if (self.currentAnnotaion == self.previousAnnotaion) {
            if ([self canCheckInHere:self.mapView.userLocation.coordinate annotation:annotation]) {
                NSLog(@"半径までチェックできるか？");
                NSLog(@"%@", annotation.title);
                NSLog(@"%d",annotation.placeID);
                [self performSelector:@selector(newMoveCheckIn:) withObject:annotation afterDelay:0.0f];
                [[NSUserDefaults standardUserDefaults] setInteger:self.currentAnnotaion forKey:@"KEY_CURRENT_ANNOTATION_PLACE_ID"];
                self.firstTap = NO;
            }
        } else {
            self.firstTap = NO;
        }
    }
}
#pragma mark - tutorialDelegate
- (void) tutorialView:(CustomScrollView *)scrollView_sender blindView:(UIView *)blindView_sender{
    [scrollView_sender removeFromSuperview];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
}
#pragma mark - チュートリアルビュー表示
-(void)tutorialShow
{
    // UIScrollViewのインスタンス化
    CustomScrollView *scrollView = [[CustomScrollView alloc] init];
    [scrollView setDelegate:nil];
    [scrollView setCustomDelegate:self];
    [scrollView setPageName:@"checkin"];
    [scrollView setPageNumber:1];
    //[dialog showTutorialPage];
    scrollView.frame = self.view.bounds;
    [self.view addSubview:scrollView];
    [scrollView showTutorialPage];
}

//------------------------------------------------------------------------------------//
#pragma mark - 初回起動かどうか調べる
//------------------------------------------------------------------------------------//
-(void)serchFirstPlay{
    // NSUserDefaultsの取得
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // KEY_BOOLの内容を取得し、BOOL型変数へ格納
    BOOL isBool = [defaults boolForKey:@"KEY_BOOL_FIRST_PLAY_MAP_VIEW"];
    
    // isBoolがNOの場合、アラート表示
    if (!isBool) {
        //初回起動の場合の処理
//        [self tutorialShow];
        // KEY_BOOLにYESを設定
        [defaults setBool:YES forKey:@"KEY_BOOL_FIRST_PLAY_MAP_VIEW"];
        // 設定を保存
        [defaults synchronize];
        
    }else{
        
    }
    
}

//--------------------------------------------------------------------------------------//
#pragma mark - MapView Delegate
//--------------------------------------------------------------------------------------//
//アノテーションビューが作られたときのデリゲート。addAnotationするときに呼ばれる
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    // アノテーションビューを取得する
    for (MKAnnotationView *annotationView in views) {
        
        //isKindOfClassのインスタンスメソッドはどのクラスから参照されたか判定する。
        //普通の.annotationかも知れないし、カスタム.annotationかもしれないから、判定しておく。
        if ([annotationView.annotation isKindOfClass:[CustomPointAnnotation class]]) {
            //ここではマップの吹き出しのボタンを作成している。
            UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            CustomPointAnnotation *po = (CustomPointAnnotation*)annotationView.annotation;
            button.tag = po.placeID;
            [button addTarget:self action:(@selector(moveCheckIn:)) forControlEvents:UIControlEventTouchUpInside];
        }else if ([annotationView.annotation isKindOfClass:[MapTileAnnotation class]]){
            NSLog(@"MapTileAnnotation");
            MapTileAnnotation *tile = (MapTileAnnotation *)[annotationView annotation];
            NSLog(@"%@",tile.url);
            NSLog(@"%@",tile.identification);
            NSLog(@"%d",tile.count);
            NSLog(@"%f",tile.coordinate.latitude);
            NSLog(@"%f",tile.coordinate.longitude);
        }else if ([annotationView.annotation isKindOfClass:[MKPointAnnotation class]]){
            NSLog(@"MKPointAnnotation");
        }
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation> )annotation
{
    if (annotation == mapView.userLocation) {
        return nil;
    }
    
    if ([annotation isMemberOfClass:[CustomPointAnnotation class]]) {
        CustomPointAnnotation *custumAnnotation = (CustomPointAnnotation *)annotation;
        static NSString *Identifier = @"PinAnnotationIdentifier";
        MKPinAnnotationView *pinView;
        pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:Identifier];
        
        if (pinView == nil) {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:custumAnnotation reuseIdentifier:Identifier];
            UITapGestureRecognizer *tapGesture;
            NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
            NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
            switch (compare) {
                case NSOrderedAscending:
                    break;
                case NSOrderedSame:
                    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationViewTapped:)];
                    [pinView addGestureRecognizer:tapGesture];
                    break;
                case NSOrderedDescending:
                    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationViewTapped:)];
                    [pinView addGestureRecognizer:tapGesture];
                    break;
                default:
                    break;
            }
        }
        
//        pinView.centerOffset = CGPointMake(100, 100);

        pinView.annotation = annotation;
        
        pinView.canShowCallout = YES;
        pinView.image=[UIImage imageNamed:@"mapping"];
        return pinView;
    }else if ([annotation isMemberOfClass:[MapTileAnnotation class]]) {
        MapTileAnnotation *tile = (MapTileAnnotation*)annotation;
        static NSString *identifier = @"MapTileAnnotationIdentifier";
        MKAnnotationView *annotationView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if(annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:tile reuseIdentifier:identifier];
        }
        annotationView.image = [UIImage imageNamed:@"mapping"];
        /*
        if (tile.count == 1) {
            annotationView.image = [UIImage imageNamed:@"white.png"];
        } else if (1 < tile.count && tile.count <= 5 ) {
            annotationView.image = [UIImage imageNamed:@"water.png"];
        } else if (5 < tile.count && tile.count <= 10) {
            annotationView.image = [UIImage imageNamed:@"blue.png"];
        } else if (10 < tile.count && tile.count <= 20) {
            annotationView.image = [UIImage imageNamed:@"deepblue.png"];
        } else if (20 < tile.count && tile.count <= 30) {
            annotationView.image = [UIImage imageNamed:@"yellow.png"];
        } else if (30 < tile.count && tile.count <= 50) {
            annotationView.image = [UIImage imageNamed:@"orange.png"];
        } else if (50 < tile.count && tile.count <= 80) {
            annotationView.image = [UIImage imageNamed:@"red.png"];
        } else if (80 < tile.count) {
            annotationView.image = [UIImage imageNamed:@"purple.png"];
        }*/
        
        annotationView.annotation = annotation;
        UIImageView *countPin = (UIImageView *)[annotationView viewWithTag:MAP_COUNT_PIN_TAG];
        if (countPin) {
            [countPin removeFromSuperview];
        }
        return annotationView;
    } else {
        NSString* identifier = @"Pin";
        MKAnnotationView *annotationView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        return annotationView;
    }

}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[CustomPointAnnotation class]]) {
        
        CustomPointAnnotation *annotation = (CustomPointAnnotation*)view.annotation;
        NSLog(@"%@", annotation.title);
        UIView *anikiInfoBG = [self.view viewWithTag:ANIKI_INFO_BACKGROUND_TAG];
        
        [self setUpInfomationParts];
        
        NSArray *subView = [anikiInfoBG subviews];
        for (UIView *view in subView) {
            if ([view isKindOfClass:[AdButton class]]) {
                [view removeFromSuperview];
            }
        }
    }
    self.firstTap = NO;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (view.annotation == mapView.userLocation) {
        return;
    }
    
    if ([view.annotation isKindOfClass:[CustomPointAnnotation class]]) {
        
        NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
        NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
        UIButton *button;
        CustomPointAnnotation *customAnnotaion = (CustomPointAnnotation *)view.annotation;
        CGFloat btnSize;
        switch (compare) {
            case NSOrderedAscending:
                button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
                button.tag = customAnnotaion.placeID;
                [button addTarget:self action:(@selector(moveCheckIn:)) forControlEvents:UIControlEventTouchUpInside];
                if ([self canCheckInHere:mapView.userLocation.coordinate annotation:customAnnotaion]) {
                    view.rightCalloutAccessoryView = button;
                }
                break;
            case NSOrderedSame:
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setEnabled:NO];
                btnSize = 23.0f;
                [button setFrame:CGRectMake(0.0f, 0.0f, btnSize, btnSize)];
                [button setImage:[UIImage imageNamed:@"btn_chackin_on"] forState:UIControlStateNormal];
                if ([self canCheckInHere:mapView.userLocation.coordinate annotation:customAnnotaion]) {
                    // コールアウトの右側のアクセサリビューにボタンを追加する
                    view.rightCalloutAccessoryView = button;
                }
                break;
            case NSOrderedDescending:
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setEnabled:NO];
                btnSize = 23.0f;
                [button setFrame:CGRectMake(0.0f, 0.0f, btnSize, btnSize)];
                [button setImage:[UIImage imageNamed:@"btn_chackin_on"] forState:UIControlStateNormal];
                if ([self canCheckInHere:mapView.userLocation.coordinate annotation:customAnnotaion]) {
                    // コールアウトの右側のアクセサリビューにボタンを追加する
                    view.rightCalloutAccessoryView = button;
                }
                break;
            default:
                break;
        }
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@", customAnnotaion.title];
        self.detailLabel.text = [NSString stringWithFormat:@"%@", customAnnotaion.detail];

/*
        NSString *adText = customAnnotaion.ad_text;
        if ([adText length] > 0) {
            NSLog(@"広告あり");
            [anikiInfoBG setUserInteractionEnabled:YES];
            AdButton *btn = [[AdButton alloc] init];
            [btn setFrame:CGRectMake(240.0f, 4.0f, 60.0f, 60.0f)];
            [btn setImage:[UIImage imageNamed:@"ad_icon.png"] forState:UIControlStateNormal];
            [btn setAdText:customAnnotaion.ad_text];
            [btn setTag:kMapAdButtonTag];
            [btn addTarget:self action:@selector(showAdText:) forControlEvents:UIControlEventTouchUpInside];
            [anikiInfoBG addSubview:btn];
            
        } else {
            NSLog(@"広告無し");
            NSArray *subView = [anikiInfoBG subviews];
            for (UIView *view in subView) {
                if ([view isKindOfClass:[AdButton class]]) {
                    [view removeFromSuperview];
                }
            }
        }
*/
        
    }else if ([view.annotation isKindOfClass:[MapTileAnnotation class]]){
        MapTileAnnotation *annotation = (MapTileAnnotation *)view.annotation;
        if (view != [self.mapView viewForAnnotation:self.mapView.userLocation]) {
//            NSString *URL = [NSString stringWithFormat:@"%@%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_TRAINPOINT, [NSString stringWithFormat:@"?latitude=%@", [NSString stringWithFormat:@"%.3f", annotation.coordinate.latitude]], [NSString stringWithFormat:@"&longitude=%@", [NSString stringWithFormat:@"%.3f", annotation.coordinate.longitude]]];//- 0.000156,-0.0005
//            NSLog(@"URL:%@",URL);
            
//            view.rightCalloutAccessoryView = [uibutton]

           // [self.mapTrainView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
//            annotation.title=[NSString stringWithFormat:@"投稿数 : %d",annotation.count];
            
            UIImageView *countPin = (UIImageView*)[self.view viewWithTag:MAP_COUNT_PIN_TAG];
            if (countPin) {
                [countPin removeFromSuperview];
                countPin.frame = CGRectMake(-3, -29, 29, 42);
                [view addSubview:countPin];
                
                FrameLabel *countLabel = (FrameLabel*)[self.view viewWithTag:MAP_COUNT_LABEL_TAG];
                if (countLabel) {
                    countLabel.text = [NSString stringWithFormat:@"%d", annotation.count];
                }
                
            } else {
                UIImageView *countPin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"amedascnt.png"]];
                countPin.frame = CGRectMake(-3, -29, 29, 42);
                countPin.tag = MAP_COUNT_PIN_TAG;
                [view addSubview:countPin];
                
                FrameLabel *countLabel = [[FrameLabel alloc] init];
                countLabel.frame = CGRectMake(4.5, 3.5, 20, 20);
                countLabel.font = [UIFont systemFontOfSize:10];
                countLabel.textColor = [UIColor colorWithWhite:0.298 alpha:1.000];
                countLabel.backgroundColor = [UIColor clearColor];
                countLabel.tag = MAP_COUNT_LABEL_TAG;
                countLabel.text = [NSString stringWithFormat:@"%d", annotation.count];
                countLabel.textAlignment = NSTextAlignmentCenter;
                countLabel.OutlineColor = [UIColor whiteColor];
                countLabel.OutlineWidth = 1.0f;
                [countPin addSubview:countLabel];
            }
            
        }

    }
    
}
- (void) didSelect:(CustomTabBarController *)tabBarController {
    
}
- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated
{
    NSLog(@"didChangeUserTrackingMode:%ld", (long)mode);
    
}

#pragma mark - チェックインできるかどうかのメソッド
/*
 *第一引数：ユーザーの現在地
 *第二引数：アノテーション情報
 */
- (BOOL) canCheckInHere:(CLLocationCoordinate2D)hereCoordinate annotation:(CustomPointAnnotation *)annotaion{
    CLLocation *hereLocation = [[CLLocation alloc] initWithLatitude:hereCoordinate.latitude longitude:hereCoordinate.longitude];
    CLLocation *thereLocation = [[CLLocation alloc] initWithLatitude:annotaion.coordinate.latitude longitude:annotaion.coordinate.longitude];
    CLLocationDistance distance = [hereLocation distanceFromLocation:thereLocation];
    if (distance <= annotaion.radius) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - showMapView
- (void)showMapView{
    for (MKAnnotationView *annotation in [self mapView].annotations) {
        id <MKAnnotation> aPin = (id<MKAnnotation>) annotation;
        [[self mapView] removeAnnotation:aPin];
    }
    [self showPinInMapView];
    
    if (self.trainViewTimer) {
        [self.trainViewTimer invalidate];
        self.trainViewTimer = nil;
    }
    
    [[self mapView] setShowsUserLocation:YES];
    [[self mapView] setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    [[self mapView] setScrollEnabled:YES];
    [[self mapView] setZoomEnabled:YES];
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            break;
        case NSOrderedSame:
            [[self mapView] setRotateEnabled:YES];
            break;
        case NSOrderedDescending:
            [[self mapView] setRotateEnabled:YES];
            break;
        default:
            break;
    }
    
    [self moveMap];
    
    [[[self view] viewWithTag:MPPhotoMapTagWebView] setHidden:YES];
    _panelView.hidden = false;
    [[[self view] viewWithTag:ANIKI_INFO_BACKGROUND_TAG] setHidden:NO];
    [[[self view] viewWithTag:COMPASS_TAG] setHidden:YES];
    [self.myImageView setHidden:YES];
    UIImageView *countPin = (UIImageView*)[self.view viewWithTag:MAP_COUNT_PIN_TAG];
    if (countPin) {
        [countPin removeFromSuperview];
    }
    _noPhotoView.hidden = true;
    _homeStadiumBtn.hidden = false;
    _currentLocationBtn.hidden = false;
}

- (void)testAnnotation_Latitude:(double)lat longitude:(double)lon{
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = lat;
    location.longitude = lon;
    [annotation setCoordinate:location];
    [self.mapView addAnnotation:annotation];
}
#pragma mark - showPhotoMapView
- (void)showPhotoMapView{
    for (MKAnnotationView *annotation in [self mapView].annotations) {
        id <MKAnnotation> aPin = (id<MKAnnotation>) annotation;
        [[self mapView] removeAnnotation:aPin];
    }
    
    [[self mapView] setShowsUserLocation:NO];
    [[self mapView] setScrollEnabled:NO];
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            break;
        case NSOrderedSame:
            [[self mapView] setRotateEnabled:NO];
            break;
        case NSOrderedDescending:
            [[self mapView] setRotateEnabled:NO];
            break;
        default:
            break;
    }
    [[self mapView] setZoomEnabled:NO];
    
    /*LocationUtility *lu = [LocationUtility sharedManager];
    
    MKCoordinateRegion zoom = [[self mapView] region];
    CLLocationDegrees adjustDeg = 0.001;
    zoom.center.latitude = [[LocationUtility sharedManager] latitude] - adjustDeg;
    zoom.center.longitude = [[LocationUtility sharedManager] longitude];
    zoom.span.latitudeDelta = 0.01;
    zoom.span.longitudeDelta = 0.01;
    [[self mapView] setRegion:zoom animated:YES];*/
    [self moveMap];
    
//    NSString *URL = [NSString stringWithFormat:@"%@%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_TRAINSEARCH, [NSString stringWithFormat:@"?latitude=%@", [NSString stringWithFormat:@"%.3f", [LocationUtility sharedManager].latitude]], [NSString stringWithFormat:@"&longitude=%@", [NSString stringWithFormat:@"%.3f", [LocationUtility sharedManager].longitude]]];
//    [self.mapTrainView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
    
    if (!self.trainViewTimer) {
        [self setUpTimer];
    }

    [[[self view] viewWithTag:ANIKI_INFO_BACKGROUND_TAG] setHidden:YES];
    [[[self view] viewWithTag:MPPhotoMapTagWebView] setHidden:NO];
    [[[self view] viewWithTag:COMPASS_TAG] setHidden:NO];
    [self.myImageView setHidden:NO];
    _panelView.hidden = true;
    _homeStadiumBtn.hidden = true;
    _currentLocationBtn.hidden = true;
}
#pragma mark - pushSegSwitch
- (IBAction)pushSegBtns:(UIButton *)sender {
    NSInteger type = [sender tag];
    switch (type) {
        case MPButtonTypeMap:
            NSLog(@"map");
            [_chkinSegBtn setSelected:YES];
            [_photoSegBtn setSelected:NO];
            [self showMapView];
            break;
            
        case MPButtonTypePhoto:
            NSLog(@"photo");
            [_chkinSegBtn setSelected:NO];
            [_photoSegBtn setSelected:YES];
            [self showPhotoMapView];
            break;
            
        default:
            break;
    }
}

#pragma mark - set up parts
- (void)setUpHeaderTabParts
{
    CGFloat statusbarHeight = (CGFloat)[[Util sharedManager] getStatusbarHeight];
    CGFloat topBarHeight = [[Util sharedManager] getAdjustedHeaderPartsHeight];
    CGFloat topIconSize = 44.0f;
    CGFloat sideAdjust = 10.0f;
    CGFloat segIconWidth = 80.0f;
    NSString *iOS_adjust;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            iOS_adjust = @"new_topbar-none_iOS6_44pxl.png";
            break;
        case NSOrderedSame:
            iOS_adjust =@"new_topbar-none_iOS7_64pxl.png";
            break;
        case NSOrderedDescending:
            iOS_adjust =@"new_topbar-none_iOS7_64pxl.png";
            break;
        default:
            break;
    }
    
    CGFloat compassSize = 50.0f;
    UIImageView *compass = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"compass.png"]];
    compass.frame = CGRectMake(WIN_SIZE.width - compassSize, topBarHeight, compassSize, compassSize);
    compass.tag = COMPASS_TAG;
    [compass setHidden:YES];
    [self.view addSubview:compass];
    
    UIImageView *pin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"compass-pin.png"]];
    [pin setFrame:CGRectMake(0.0f, 0.0f, compassSize, compassSize)];
    [compass addSubview:pin];
}

- (void)setUpMapParts
{
    self.mapView.delegate = self;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow];
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    
    [self moveMap];
    [[self mapView] setTag:MPMapTagMap];
    
}

- (void)setUpInfomationParts
{
    self.titleLabel.text = @"チェックインポイント";
    self.detailLabel.text = @"阪神タイガースに関連する場所に行って、\nチェックインボーナスをもらおう!!";
}

#pragma mark - photoMap関連
#pragma mark - setUpPhotoMapView

#pragma mark - setUpImageParts
- (void)setUpImageParts{
    // TODO:実装
    //CGSize viewSize = CGSizeMake(320.0f, 105.0f);
//    self.myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, WIN_SIZE.height - viewSize.height - TABBUTTON_HEIGHT, viewSize.width, viewSize.height)];
    [self.myImageView setImage:[UIImage imageNamed:@"noPhoto.png"]];
    [self.view addSubview:self.myImageView];
}

#pragma mark - setUpWebTrainView
- (void)setUpWebTrainView{
    [self.mapTrainView setDelegate:self];
    [[self mapTrainView] setTag:MPPhotoMapTagWebView];
    _mapTrainView.scrollView.bounces = false;
}

- (void)setUpWebTrainView:(double)latitude longitude:(double)longitude{
    CGFloat sideAdjust = 20.0f;
    NSString *current_iOSver = [[UIDevice currentDevice] systemVersion];
    NSComparisonResult compare = [current_iOSver compare:@"7.0.0" options:NSNumericSearch];
    switch (compare) {
        case NSOrderedAscending:
            sideAdjust = 20.0f;
            break;
        case NSOrderedSame:
            sideAdjust = 0.0f;
            break;
        case NSOrderedDescending:
            sideAdjust = 0.0f;
            break;
        default:
            break;
    }
    
//    CGFloat mtHight = 105.0f;
//    self.mapTrainView = [[UIWebView alloc] initWithFrame:CGRectMake(0, WIN_SIZE.height - TABBUTTON_HEIGHT - mtHight - sideAdjust, WIN_SIZE.width, mtHight)];
    [self.mapTrainView setDelegate:self];
    /*[[self.mapTrainView scrollView] setBounces:NO];
    [[self.mapTrainView scrollView] setScrollEnabled:NO];*/
    [[self mapTrainView] setTag:MPPhotoMapTagWebView];
    [[self mapTrainView] setHidden:NO];
//    NSString *URL = [NSString stringWithFormat:@"%@%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_TRAINSEARCH, [NSString stringWithFormat:@"?latitude=%@", [NSString stringWithFormat:@"%.3f", [LocationUtility sharedManager].latitude]], [NSString stringWithFormat:@"&longitude=%@", [NSString stringWithFormat:@"%.3f", [LocationUtility sharedManager].longitude]]];
//    [self.mapTrainView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
//    [self.view addSubview:self.mapTrainView];
}

#pragma mark - trainViewTimer
//- (void)trainViewTimer:(NSTimer *)timer {
//    NSString *URL = [NSString stringWithFormat:@"%@%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_TRAINSEARCH, [NSString stringWithFormat:@"?latitude=%@", [NSString stringWithFormat:@"%.3f", [LocationUtility sharedManager].latitude]], [NSString stringWithFormat:@"&longitude=%@", [NSString stringWithFormat:@"%.3f", [LocationUtility sharedManager].longitude]]];
//    [self.mapTrainView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:URL]]];
//}


- (void)setUpParts
{
    [self setUpMapParts];
    [self setUpHeaderTabParts];
    [self setUpInfomationParts];
    [self setUpWebTrainView];
}

- (void)setUpTimer {
//    self.trainViewTimer = [NSTimer scheduledTimerWithTimeInterval:60.0f target:self selector:@selector(trainViewTimer:) userInfo:nil repeats:YES];
}

#pragma mark - webViewDelegate
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
//    
//    if ((navigationType == UIWebViewNavigationTypeLinkClicked) && ([[[request URL] path] isEqualToString:@"/detail"])) {
//        NSString *path = [[request URL] path];
//        NSLog(@"path:%@", path);
//        NSString *URL = [[request URL] absoluteString];
//        NSLog(@"URL:%@", URL);
//		NSString *query = [[request URL] query];
//		NSLog(@"query : %@", query);
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CameraStoryboard" bundle:nil];
//        FramePostFinishVC *postVC = [storyboard instantiateViewControllerWithIdentifier:@"FramePostFinishVC"];
//        [postVC setPhotPath:URL withIsFromPost:NO];
//        postVC.titleStr = @"周辺写真";
//        [self.navigationController pushViewController:postVC animated:YES];
//        
//        return NO;
//	}
//    
//    else if (navigationType == UIWebViewNavigationTypeReload) {
//        
//    }
//    return YES;
//}

// ページ読込開始時にインジケータをくるくるさせる
-(void)webViewDidStartLoad:(UIWebView*)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
}

// ページ読込完了時にインジケータを非表示にする
-(void)webViewDidFinishLoad:(UIWebView*)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//    [SVProgressHUD showSuccessWithStatus:@"Complete!"];
}

#pragma mark - iOS7対応
- (UIStatusBarStyle)preferredStatusBarStyle {
    //文字を白くする
    return UIStatusBarStyleLightContent;
}

#pragma mark - view life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // TODO:GoogleAnalytics
    self.screenName = @"MapVC";

    if (!self.trainViewTimer) {
        [self setUpTimer];
    }
    
    if (!self.tabBarIsVisible) {
        self.tabBarIsVisible = YES;
        [(CustomTabBarController *)self.tabBarController showHideTabBar:self.tabBarIsVisible withDuration:0.2f];
    }
    
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //タイマーの破棄
    if (self.trainViewTimer) {
        [self.trainViewTimer invalidate];
        self.trainViewTimer = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if([[UIScreen mainScreen]bounds].size.height == 568){
        //iPhone5の場合の処理
        _y568 = 44;
    }else{
        //それ以外の場合の処理
        _y568 = 0;
    }
    [self setUpParts];
    [self showPinInMapView];
    [self serchFirstPlay];
    
    self.tabBarIsVisible = YES;
    self.firstTap = NO;
    
    [self firstTimeAlert];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)firstViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    NSLog(@"First view return action invoked.");
}

//- (IBAction)pushedMyPhoto:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CameraStoryboard" bundle:nil];
//    MyPhotoVC *myPhotoVC = [storyboard instantiateViewControllerWithIdentifier:@"MyPhotoVC"];
//    [self.navigationController pushViewController:myPhotoVC animated:YES];
//}

- (IBAction)goHomeStadium:(id)sender {
    [self moveMap:34.721440 lon:135.361626 span:0.01];
}

- (IBAction)goCurrentLocation:(id)sender {
    [self moveMap];
}

- (void)moveMap{
    LocationUtility *lu = [LocationUtility sharedManager];
    if([LocationUtility sharedManager].isLocationEnabled)[self moveMap:lu.latitude lon:lu.longitude span:0.01];
    else [self moveMap:35 lon:137 span:25];
}

- (void)moveMap:(float)lat lon:(float)lon span:(float)span{
    MKCoordinateRegion zoom = self.mapView.region;
    zoom.center.latitude = lat;
    zoom.center.longitude = lon;
    zoom.span.latitudeDelta = span;
    zoom.span.longitudeDelta = span;
    [self.mapView setRegion:zoom animated:YES];
}

@end
