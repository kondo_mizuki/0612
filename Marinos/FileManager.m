//
//  FileManager.m
//  Marinos
//
//  Created by eagle014 on 2014/04/28.
//  Copyright (c) 2014年 eagle014. All rights reserved.
//

#import "FileManager.h"
#import "Util.h"
#import "DatabaseUtility.h"

#include <sys/xattr.h>

@interface FileManager ()

@property (nonatomic, copy) NSString *myDirectory;
@property (nonatomic, copy, readwrite) NSString *filePath;
@property (nonatomic, copy, readwrite) NSString *filePathIsNotApplecableBackUp;
@property (nonatomic, strong) NSFileHandle *myFileHandle;
@property (nonatomic, strong) NSMutableArray *myDownloadDicArray;

@end

@implementation FileManager

static FileManager *sharedInstance = nil;

#pragma mark - singleton method
+ (FileManager *)sharedManager
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;  // シングルトン状態を保持するため何もせず self を返す
}

- (BOOL) saveTemporaryDirectory:(NSString *)file_name withData:(NSData *)data{
    static BOOL ret = NO;
    
    return ret;
}

- (NSString *) getFilePathDirectoty:(NSString *)directory fileName:(NSString *)file_name{
    self.myDirectory = directory;
    NSString *file_path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![self.myDirectory isEqualToString:DOCUMENT_DIR]) {
        documentsDirectory = [documentsDirectory stringByReplacingOccurrencesOfString:DOCUMENT_DIR withString:self.myDirectory];
    }
    file_path = [documentsDirectory stringByAppendingPathComponent:file_name];
    return file_path;
}

- (NSString *)getFilePath:(NSString *)file_name{
    NSString *file_path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    /*
    if (![self.myDirectory isEqualToString:DOCUMENT_DIR]) {
        documentsDirectory = [documentsDirectory stringByReplacingOccurrencesOfString:DOCUMENT_DIR withString:self.myDirectory];
    }
     */
    //self.myDirectory = [paths objectAtIndex:0];
    // iCloudでのバックアップ対象から外すためfilePathを書き換え
    file_path = [[documentsDirectory stringByAppendingPathComponent:DIR_IS_NOT_APPLICABLE_BACK_UP] stringByAppendingPathComponent:file_name];
    //file_path = [[self.myDirectory stringByAppendingPathComponent:file_name] stringByStandardizingPath];
    return file_path;
}

- (BOOL) saveDirectory:(NSString *)directory fileName:(NSString *)file_name withData:(NSData *)data{
    static BOOL ret = NO;
    self.myDirectory = directory;
    self.filePath = [self getFilePath:file_name];
    @try {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:self.filePath]) { // yes
            // 空のファイルを作成する
            BOOL result = [fileManager createFileAtPath:self.filePath
                                               contents:[NSData data] attributes:nil];
            if (!result) {
                NSLog(@"ファイルの作成に失敗");
                ret = NO;
                return ret;
            }
        }
        self.myFileHandle = [NSFileHandle fileHandleForWritingAtPath:self.filePath];
		[self.myFileHandle writeData:data];
        [self.myFileHandle synchronizeFile];
        [self.myFileHandle closeFile];
        NSLog(@"%@ファイルの書き込みが完了しました．", self.filePath);
        ret = YES;
	}
	@catch (NSException * except) {
        NSLog(@"exception%@", except);
        ret = NO;
	}
    
    return ret;

}

//- (NSData *) loadDirectory:(NSString *)directory fileName:(NSString *)file_name{
//    NSData *retData = [NSData data];
//    self.myDirectory = directory;
//    self.filePath = [self getFilePath:file_name];
//    /*
//     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//     NSString *documentsDirectory = [paths objectAtIndex:0];
//     NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:file_name];
//     */
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    BOOL success = [fileManager fileExistsAtPath:self.filePath];
//    if (success) {
//        NSLog(@"load from local");
//        retData = [NSData dataWithContentsOfFile:self.filePath];
//    } else {
//        NSLog(@"data is missing");
//        [[FileDownloadObject sharedManager] loadImageFromServer:file_name];
//    }
//    
//    return retData;
//}


//----------------------------------------------------------------------------------
#pragma mark - insertDeleteProcess
//----------------------------------------------------------------------------------
- (void)routineWorkDelete:(NSDictionary *)workDic tableName:(NSString *)table_name{
    NSArray *deleteKeyAry = [workDic allKeys];
    NSNumber *de_num;
    for (de_num in deleteKeyAry) {
        NSString *num = [workDic objectForKey:de_num];
        NSLog(@"num:%@",num);
        [[DatabaseUtility sharedManager] deleteTable:table_name serialID:num];
    }
}
- (void)routineWork:(NSDictionary *)workDic workName:(NSString *)recode tableName:(NSString *)table_name{
    //recordDicの中に"card","card_category","photo","card_rarelevel","place","place_category","place_special"
    if ([workDic objectForKey:recode] != [NSNull null]) {
        //photoだけは別処理
        if ([recode isEqualToString:@"photo"]) {
            NSDictionary *photoDic = [workDic objectForKey:@"photo"];
            NSArray *photoIDNumAry = [photoDic allKeys];
            for (NSNumber *photoIDNum in photoIDNumAry) {
                NSDictionary *flagDic = [photoDic objectForKey:photoIDNum];
                NSNumber *boolean = [flagDic objectForKey:@"show_flag"];
                BOOL showFlag = [boolean boolValue];
//                NSString *keyURLStr = [NSString stringWithFormat:@"%@%@%@", [[Util sharedManager] myServer], PHP_PROGRAM_DETAIL, [NSString stringWithFormat:@"?photo_id=%d", [photoIDNum intValue]]];
//                [[Util sharedManager] saveNewHistoryTableForShowFlag:showFlag keyURLForDataBase:keyURLStr];
                
            }
        } else {
            NSDictionary *dic = [workDic objectForKey:recode];
            NSLog(@"dic:%@",dic);
            NSArray *ary = [dic allKeys];
            NSLog(@"ary:%@",ary);
            NSNumber *nameID;
            NSDictionary *updateDic = [NSDictionary dictionary];
            NSDictionary *insertDic = [NSDictionary dictionary];
            NSMutableArray *updateAry = [NSMutableArray array];
            NSMutableArray *insertAry = [NSMutableArray array];
            for (nameID in ary) {
                if ([[DatabaseUtility sharedManager] checkTableData:table_name tableID:nameID]) {
                    //update
                    updateDic = [dic objectForKey:nameID];
                    [updateAry addObject:updateDic];
                    //NSLog(@"update:%@", updateAry);
                } else {
                    //insert
                    insertDic = [dic objectForKey:nameID];
                    [insertAry addObject:insertDic];
                    //NSLog(@"insert:%@", insertAry);
                }
            }
            //NSArray *cardImageDownloadArray = [NSArray array];
            NSLog(@"tableName:::%@", table_name);
            if ([table_name isEqualToString:@"card"]) {
                if ([updateDic count] > 0) {
                    [[DatabaseUtility sharedManager] updateCardTable:updateAry];
                    //[self ImageDownloadProcess:updateAry];
                }
                if ([insertDic count] > 0) {
                    [[DatabaseUtility sharedManager] insertCardTable:insertAry];
                    //[self ImageDownloadProcess:insertAry];
                    //ImageDownload
                }
            } else if ([table_name isEqualToString:@"char_category"]){
                if ([updateDic count] > 0) {
                    [[DatabaseUtility sharedManager] updateCardCategory:updateAry];
                }
                if ([insertDic count] > 0) {
                    [[DatabaseUtility sharedManager] insertCardCategory:insertAry];
                }
            } else if ([table_name isEqualToString:@"place"]){
                if ([updateDic count] > 0) {
                    [[DatabaseUtility sharedManager] updatePlaceTable:updateAry];
                }
                if ([insertDic count] > 0) {
                    [[DatabaseUtility sharedManager] insertPlaceTable:insertAry];
                }
            } else if ([table_name isEqualToString:@"place_category"]){
                if ([updateDic count] > 0) {
                    [[DatabaseUtility sharedManager] updatePlaceCategory:updateAry];
                }
                if ([insertDic count] > 0) {
                    [[DatabaseUtility sharedManager] insertPlaceCategory:insertAry];
                }
                
            } else if ([table_name isEqualToString:@"sp_place"]){
                if ([updateDic count] > 0) {
                    [[DatabaseUtility sharedManager] updateSpecialPlace:updateAry];
                }
                if ([insertDic count] > 0) {
                    [[DatabaseUtility sharedManager] insertSpecialPlace:insertAry];
                }
            } else if ([table_name isEqualToString:@"rare_level"]) {
                if ([updateDic count] > 0) {
                    [[DatabaseUtility sharedManager] updateRareLevelTable:updateAry];
                }
                if ([insertAry count] > 0) {
                    [[DatabaseUtility sharedManager] insertRareLevelTable:insertAry];
                }
            }
        }
    }
}

- (void)insertDatabase:(NSDictionary *)jsonDic{
    //jsonDicの中に"code","record","version_ID","delete"
    NSString *tableName;
    //recordDicの中に"card","card_category","photo","card_rarelevel","place","place_category","place_special"
    if ([jsonDic objectForKey:@"recode"] != [NSNull null]) {
        NSDictionary *recodeDic = [jsonDic objectForKey:@"recode"];
        //NSLog(@"recoed:::%@", recodeDic);
        NSArray *invocationNameAry = [recodeDic allKeys];
        //NSLog(@"ary:%@", invocationNameAry);
        NSString *invocationName;
        for (invocationName in invocationNameAry) {
            NSLog(@"name:%@", invocationName);
            if ([invocationName isEqualToString:@"card_category"]){
                tableName = @"char_category";
            } else if ([invocationName isEqualToString:@"card"]){
                tableName = @"card";
            } else if ([invocationName isEqualToString:@"place"]){
                tableName = @"place";
            } else if ([invocationName isEqualToString:@"place_category"]){
                tableName = @"place_category";
            } else if ([invocationName isEqualToString:@"place_special"]){
                tableName = @"sp_place";
            } else if ([invocationName isEqualToString:@"photo"]) {
                tableName = @"photo";
            } else if ([invocationName isEqualToString:@"card_rarelevel"]) {
                tableName = @"rare_level";
            }
            //if ([invocationName isEqualToString:@"photo"]) { continue; };
            //[self routineWork:recodeDic tableName:invocationName];
            [self routineWork:recodeDic workName:invocationName tableName:tableName];
        }
        
        
        //この後はダウンロード処理の準備
        //カードだけは別処理
        if ([recodeDic objectForKey:@"card"] != [NSNull null]) {
            NSDictionary *dic = [recodeDic objectForKey:@"card"];
            NSLog(@"dic:%@",dic);
            NSArray *ary = [dic allKeys];
            NSLog(@"ary:%@",ary);
            NSNumber *nameID;
            NSDictionary *detailDic = [NSDictionary dictionary];
            self.myDownloadDicArray = [NSMutableArray array];
            NSString *fileName, *card_id;
            for (nameID in ary) {
                NSMutableDictionary *downloadDic = [NSMutableDictionary dictionary];
                detailDic = [dic objectForKey:nameID];
                
                fileName = [detailDic objectForKey:@"file_name"];
                [downloadDic setObject:fileName forKey:@"file_name"];
                
                card_id = [detailDic objectForKey:@"card_id"];
                [downloadDic setObject:card_id forKey:@"card_id"];
                
                [self.myDownloadDicArray addObject:downloadDic];
            }
            //ダウンロード処理
            //[self ImageDownloadProcess:self.myDownloadDicArray];
        }
    }
    
    //deleteProcess
    if ([jsonDic objectForKey:@"delete"] != [NSNull null]) {
        NSDictionary *deleteDic = [jsonDic objectForKey:@"delete"];
        NSArray *delete_category = [deleteDic allKeys];
        NSString *deleteStr;
        NSDictionary *deleteDetailDic = [NSDictionary dictionary];
        for (deleteStr in delete_category) {
            deleteDetailDic = [deleteDic objectForKey:deleteStr];
            if ([deleteStr isEqualToString:@"card"]) {
                tableName = @"card";
            } else if ([deleteStr isEqualToString:@"card_category"]){
                tableName = @"char_category";
            } else if ([deleteStr isEqualToString:@"photo"]){
                continue;
            } else if ([deleteStr isEqualToString:@"card_rarelevel"]){
                tableName = @"rare_level";
            } else if ([deleteStr isEqualToString:@"place"]){
                tableName = @"place";
            } else if ([deleteStr isEqualToString:@"place_category"]){
                tableName = @"place_category";
            } else if ([deleteStr isEqualToString:@"place_special"]){
                tableName = @"sp_place";
            } else if ([deleteStr isEqualToString:@"card_rarelevel"]) {
                tableName = @"rare_level";
            }
            [self routineWorkDelete:deleteDetailDic tableName:tableName];
        }
    }
    NSString *version_ID = [jsonDic objectForKey:@"version_id"];
    NSLog(@"version_ID:%@",version_ID);
    [self.delegate didFinishedInsertProcess:version_ID downloadDicAry:self.myDownloadDicArray];
}
//------------------------------------------------------------------------------------

//////////////////////////////////////////////////////////////////////////////////////
#pragma mark
#pragma mark - DocumentsDirectory
//////////////////////////////////////////////////////////////////////////////////////
/*
 * iCloudに保存出来る領域が変更になった為
 */
- (BOOL) makeDirectoryForIsNotApplicableBackUp {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *notBackUpDirectory = [self makeDirectoryPathForIsNotApplicableBackUp];
    
    BOOL exists = [fileManager fileExistsAtPath:notBackUpDirectory];
    if (!exists) {
        NSError *error;
        BOOL created = [fileManager createDirectoryAtPath:notBackUpDirectory withIntermediateDirectories:YES attributes:nil error:&error];
        if (!created) {
            NSLog(@"ディレクトリ作成失敗");
            return NO;
        }
    } else {
        return NO; // 作成済みの場合はNO
    }
    return YES;
}

- (NSString *) makeDirectoryPathForIsNotApplicableBackUp {
    NSString *pathString = [NSString string];
    NSString *documentsPath = [NSHomeDirectory() stringByAppendingPathComponent:DOCUMENT_DIR];
    pathString = [documentsPath stringByAppendingPathComponent:DIR_IS_NOT_APPLICABLE_BACK_UP];
    return pathString;
}

/*
 * iCloudに保存出来る領域が変更になった為
 */
- (BOOL) addSkipBackupAttribute {
    NSURL *Url =  [NSURL fileURLWithPath:[self makeDirectoryPathForIsNotApplicableBackUp]];
    const char *filePath = [[Url path] fileSystemRepresentation];
    
    const char *attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    if (result == 0) {
        NSLog(@":%@", [Url path]);
        return YES;
    } else {
        NSLog(@"属性マーク設定失敗");
        return NO;
    }
}


- (NSString *)temporaryDirectory
{
    return NSTemporaryDirectory();
}

- (NSString *)temporaryDirectoryWithFileName:(NSString *)fileName
{
    return [[self temporaryDirectory] stringByAppendingPathComponent:fileName];
}

- (NSString *)documentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(
                                                         NSDocumentDirectory,
                                                         NSUserDomainMask,
                                                         YES);
    return [paths objectAtIndex:0];
}

- (NSString *)documentDirectoryWithFileName:(NSString *)fileName
{
    return [[self documentDirectory] stringByAppendingPathComponent:fileName];
}

- (BOOL)fileExistsAtPath:(NSString *)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    /* ファイルが存在するか */
    if ([fileManager fileExistsAtPath:path]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isElapsedFileModificationDateWithPath:(NSString *)path elapsedTimeInterval:(NSTimeInterval)elapsedTime
{
    if ([self fileExistsAtPath:path]) {
        NSError *error = nil;
        NSDictionary *dicFileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
        if (error) {
            return NO;
        }
        /* 現在時間とファイルの最終更新日時との差を取得 */
        NSTimeInterval diff  = [[NSDate dateWithTimeIntervalSinceNow:0.0] timeIntervalSinceDate:dicFileAttributes.fileModificationDate];
        if(elapsedTime < diff){
            /* ファイルの最終更新日時からelapseTime以上経っている */
            return YES;
        } else {
            return NO;
        }
    }
    return NO;
}

- (NSArray *)fileNamesAtDirectoryPath:(NSString *)directoryPath extension:(NSString *)extension
{
    NSFileManager *fileManager=[[NSFileManager alloc] init];
    NSError *error = nil;
    /* 全てのファイル名 */
    NSArray *allFileName = [fileManager contentsOfDirectoryAtPath:directoryPath error:&error];
    if (error) return nil;
    NSMutableArray *hitFileNames = [[NSMutableArray alloc] init];
    for (NSString *fileName in allFileName) {
        /* 拡張子が一致するか */
        if ([[fileName pathExtension] isEqualToString:extension]) {
            [hitFileNames addObject:fileName];
        }
    }
    return hitFileNames;
}

- (BOOL)removeFilePath:(NSString *)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    return [fileManager removeItemAtPath:path error:NULL];
}

/*--------------------------------------------------------*/

/* /tmp内のpngファイルで、最終更新日から1時間以上経過したファイルを全て削除する */

/* /tmp内の全てのpngファイル名を取得 */
- (void)testMethod{
    NSArray *imgFileNames = [self fileNamesAtDirectoryPath:[self temporaryDirectory] extension:@"png"];
    for (NSString *fileName in imgFileNames) {
        NSString *filePath = [self temporaryDirectoryWithFileName:fileName];
        /* 最終更新日から1時間経過しているか */
        if ([self isElapsedFileModificationDateWithPath:filePath elapsedTimeInterval:60.0 * 60.0]) {
            /* 削除 */
            [self removeFilePath:filePath];
        }
    }

}

@end
