//
//  FrameLabel.m
//  TimeCapsule
//
//  Created by kyo on 2013/04/09.
//  Copyright (c) 2013年 kyo. All rights reserved.
//

#import "FrameLabel.h"

@implementation FrameLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect
{
    CGSize shadowOffset = self.shadowOffset;
    UIColor *txtColor = self.textColor;
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(contextRef, self.OutlineWidth);
    CGContextSetLineJoin(contextRef, kCGLineJoinRound);
    
    CGContextSetTextDrawingMode(contextRef, kCGTextStroke);
    self.textColor = self.OutlineColor;
    [super drawTextInRect:CGRectInset(rect, self.OutlineWidth, self.OutlineWidth)];
    
    CGContextSetTextDrawingMode(contextRef, kCGTextFill);
    self.textColor = txtColor;
    [super drawTextInRect:CGRectInset(rect, self.OutlineWidth, self.OutlineWidth)];
    
    self.shadowOffset = shadowOffset;
}

@end
