
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ABCustomUINavigationController
#define COCOAPODS_POD_AVAILABLE_ABCustomUINavigationController
#define COCOAPODS_VERSION_MAJOR_ABCustomUINavigationController 1
#define COCOAPODS_VERSION_MINOR_ABCustomUINavigationController 2
#define COCOAPODS_VERSION_PATCH_ABCustomUINavigationController 1

// Canvas
#define COCOAPODS_POD_AVAILABLE_Canvas
#define COCOAPODS_VERSION_MAJOR_Canvas 0
#define COCOAPODS_VERSION_MINOR_Canvas 1
#define COCOAPODS_VERSION_PATCH_Canvas 2

// DARecycledScrollView
#define COCOAPODS_POD_AVAILABLE_DARecycledScrollView
#define COCOAPODS_VERSION_MAJOR_DARecycledScrollView 1
#define COCOAPODS_VERSION_MINOR_DARecycledScrollView 0
#define COCOAPODS_VERSION_PATCH_DARecycledScrollView 2

// FMDB
#define COCOAPODS_POD_AVAILABLE_FMDB
#define COCOAPODS_VERSION_MAJOR_FMDB 2
#define COCOAPODS_VERSION_MINOR_FMDB 5
#define COCOAPODS_VERSION_PATCH_FMDB 0

// FMDB/common
#define COCOAPODS_POD_AVAILABLE_FMDB_common
#define COCOAPODS_VERSION_MAJOR_FMDB_common 2
#define COCOAPODS_VERSION_MINOR_FMDB_common 5
#define COCOAPODS_VERSION_PATCH_FMDB_common 0

// FMDB/standard
#define COCOAPODS_POD_AVAILABLE_FMDB_standard
#define COCOAPODS_VERSION_MAJOR_FMDB_standard 2
#define COCOAPODS_VERSION_MINOR_FMDB_standard 5
#define COCOAPODS_VERSION_PATCH_FMDB_standard 0

// GoogleAnalytics-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics_iOS_SDK 12
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics_iOS_SDK 0

// GoogleAnalytics-iOS-SDK/Core
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics_iOS_SDK_Core
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics_iOS_SDK_Core 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics_iOS_SDK_Core 12
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics_iOS_SDK_Core 0

// Harpy
#define COCOAPODS_POD_AVAILABLE_Harpy
#define COCOAPODS_VERSION_MAJOR_Harpy 3
#define COCOAPODS_VERSION_MINOR_Harpy 3
#define COCOAPODS_VERSION_PATCH_Harpy 6

// MJPopupViewController
#define COCOAPODS_POD_AVAILABLE_MJPopupViewController
#define COCOAPODS_VERSION_MAJOR_MJPopupViewController 0
#define COCOAPODS_VERSION_MINOR_MJPopupViewController 4
#define COCOAPODS_VERSION_PATCH_MJPopupViewController 0

// NCMB
#define COCOAPODS_POD_AVAILABLE_NCMB
#define COCOAPODS_VERSION_MAJOR_NCMB 2
#define COCOAPODS_VERSION_MINOR_NCMB 1
#define COCOAPODS_VERSION_PATCH_NCMB 0

// OHAttributedLabel
#define COCOAPODS_POD_AVAILABLE_OHAttributedLabel
#define COCOAPODS_VERSION_MAJOR_OHAttributedLabel 3
#define COCOAPODS_VERSION_MINOR_OHAttributedLabel 5
#define COCOAPODS_VERSION_PATCH_OHAttributedLabel 5

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 2
#define COCOAPODS_VERSION_PATCH_Reachability 0

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 1
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 3

